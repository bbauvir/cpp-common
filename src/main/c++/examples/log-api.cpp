/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Example code
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <common/log-api.h>
#include <common/TimeTools.h> // For time-related routines

// Local header files

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ex::log"

// Type definition

namespace user {
namespace appl {

// Function declaration

// Global variables

// Function definition

// cppcheck-suppress unusedFunction // Example code
bool UserDefinedSleep (ccs::types::uint32 arg) // exact next configurable number of nanosec
{
  log_debug("%s - Entering routine", __FUNCTION__);

  bool status = (0u != arg);

  if (status)
    {
      ccs::types::uint64 curr_time = ccs::HelperTools::GetCurrentTime();
      ccs::types::uint64 till_time = static_cast<ccs::types::uint64>(arg) * (1ul + curr_time / static_cast<ccs::types::uint64>(arg));

      log_info("Sleep from '%lu' till next '%u' period", curr_time, arg);

      ccs::types::uint64 wake_time = ccs::HelperTools::WaitUntil(till_time);

      log_info("Wakeup at '%lu'", wake_time); 
    }
  else
    {
      log_error("Invalid argument '%u'", arg);
      // Handle error
    }


  log_debug("%s - Leaving routine", __FUNCTION__);

  return status;
}

} // namespace appl
} // namespace user
