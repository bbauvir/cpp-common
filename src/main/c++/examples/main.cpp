/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Example code
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <common/tools.h>
#include <common/types.h>

// Local header files

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ex::log"

// Type definition

// Function declaration

extern "C" {

extern bool user_defined_sleep(uint32_t arg);

} // extern C

// Global variables

// Function definition

int main(int argc, char **argv)
{
  uint32_t period = 100000000ul;

  if (argc > 1)
    {
      uint32_t index;

      for (index = 1u; index < (uint32_t) argc; index++)
	{
          if ((strcmp(argv[index], "-p") == 0) || (strcmp(argv[index], "--period") == 0))
	    {
	      /* Get period in ns */
	      sscanf(argv[index + 1], "%u", &period);
	      index += 1;
            
	    }
	}
    }

  bool status = user_defined_sleep(period);

  return ((status)?0:1);
}
