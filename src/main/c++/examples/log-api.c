/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Example code
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <common/log-api.h>
#include <common/tools.h> // For time-related routines

// Local header files

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ex::log"

// Type definition

// Function declaration

// Global variables

// Function definition

bool user_defined_sleep (uint32_t arg) // exact next configurable number of nanosec
{
  log_debug("%s - Entering routine", __FUNCTION__);

  bool status = (0u != arg);

  if (status)
    {
      uint64_t curr_time = get_time();
      uint64_t till_time = (uint64_t)arg * (1ul + curr_time / (uint64_t)arg);

      log_info("Sleep from '%lu' till next '%u' period", curr_time, arg);

      uint64_t wake_time = wait_until(till_time, DEFAULT_WAIT_UNTIL_SLEEP_PERIOD);

      log_info("Wakeup at '%lu'", wake_time); 
    }
  else
    {
      log_error("Invalid argument '%u'", arg);
      // Handle error
    }


  log_debug("%s - Leaving routine", __FUNCTION__);

  return status;
}

