#ifndef LOCK_H
#define LOCK_H

/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Atomic lock class definition
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/* Global header files */

/* Local header files */

#include "Lock.h" /* Maintained for backward compatibility */
#include "AtomicLock.h" /* Maintained for backward compatibility */
#include "SemLock.h" /* Maintained for backward compatibility */

/* Constants */

/* Type definition */

/* Global variables */

/* Function declaration */

/* Function definition */

#endif /* LOCK_H */

