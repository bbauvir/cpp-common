/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVMonitorImpl.h
 * @brief Header file for PVMonitorImpl abstract class.
 * @date 01/07/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @details This header file contains the definition of the PVMonitorImpl class.
 */

#ifndef _PVMonitorImpl_h_
#define _PVMonitorImpl_h_

// Global header files

#include <functional> // std::function

// Local header files

#include "BasicTypes.h"

#include "AnyValue.h"

#include "PVMonitor.h"

// Constants

// Type definition

namespace ccs {

namespace base {

/**
 * @brief Abstract base class defining interface for PV monitor implementations.
 * @details The base class is provided to be able to support a bridge (PIMPL) pattern
 * for PV monitor clients independent of the actual transport mechanism.
 *
 * Transport implementations inherit from this base class and provide any necessary
 * means to instantiate a named channel implementation over the chosen communication
 * mechanism.
 *
 * The base class provides a mechanism to externally define a handler callback.
 *
 * @code
   @endcode
 */

typedef class PVMonitorImpl
{

  private:

    /**
     * @brief Attribute. 
     * @details Used to support the GetChannel() and SetChannel() default implementations.
     */

    ccs::types::string _name;

    /**
     * @brief Attribute. 
     * @details Monitor handler callback. The registered function is being called to provide
     * a PV value update. It is the responsibility of implementation classes to call the
     * handler by means of the CallDataHandler() method.
     */

    std::function<void(const ccs::types::AnyValue&)> _dcb;

    /**
     * @brief Attribute. 
     * @details Event handler callback. The registered function is being called to provide
     * a connect/disconnect event handling capability. It is the responsibility of
     * implementation classes to call the handler by means of the CallEventHandler() method.
     */

    std::function<void(const PVMonitor::Event&)> _ecb;

    /**
     * @brief Attribute. 
     * @details Used to support the IsConnected() default implementations.
     */

    bool _connected;

    PVMonitorImpl (const PVMonitorImpl& copy); // Undefined
    PVMonitorImpl& operator= (const PVMonitorImpl& copy); // Undefined

  protected:

  public:

    /**
     * @brief Constructor.
     */

    PVMonitorImpl (void);

    /**
     * @brief Destructor.
     */

    virtual ~PVMonitorImpl (void); 

    /**
     * @brief Initialiser method.
     * @details The interaction with the interface class is such that at instantiation time,
     * the implementation class is instantiated through the factory, variable name provided through
     * the SetChannet() interface and, if (true == SetChannel()), the Initialise() method is being
     * called for the implementation to perform any necessary start-up operation.
     * @return True if successful.
     */

    virtual bool Initialise (void);

    /**
     * @brief Tear-down method.
     * @details The interaction with the interface class is such that the Terminate() method is being
     * called before instance destruction. The particular implementation may or may not chose to 
     * specialise this method and rather opt to rely on the class destructor.
     * @return True if successful.
     */

    virtual bool Terminate (void);

    /**
     * @brief Accessor method.
     * @details The management of the connected interface can be done by specialising the IsConnected()
     * method or is implicit with the use of the CallEventHandler() one to notify the interface of
     * actual connection status.
     * @return True if currently connected.
     */

    virtual bool IsConnected (void) const;

    virtual const ccs::types::char8* GetChannel (void) const;
    virtual bool SetChannel (const ccs::types::char8 * const channel);

    /**
     * @brief Configure method.
     * @details The implementation class may provide additional settings offered to the application.
     * @return True if successful.
     */

    virtual bool SetParameter (const ccs::types::char8 * const name, const ccs::types::char8 * const value);
    virtual bool SetParameter (const ccs::types::char8 * const name, const ::ccs::types::AnyValue& value);

    bool RegisterEventHandler(const std::function<void(const PVMonitor::Event&)>& cb);
    bool RegisterDataHandler(const std::function<void(const ccs::types::AnyValue&)>& cb);

    void CallEventHandler (const PVMonitor::Event& event) const;
    void CallDataHandler (const ::ccs::types::AnyValue& value) const;

} ProcessVariableMonitorImpl;

// Global variables

// Function declaration

} // namespace base

} // namespace ccs

#endif // _PVMonitorImpl_h_

