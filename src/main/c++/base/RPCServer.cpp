/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow
#include <functional> // std::function

// Local header files

#include "BasicTypes.h" // Global type definition
#include "SysTools.h" // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include "log-api.h" // Syslog wrapper routines

#include "AnyType.h" // Introspectable data type ..
#include "AnyTypeHelper.h" // .. associated helper routines

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

#include "RPCTypes.h"

#include "RPCServerImpl.h"
#include "RPCServerImplFactory.h"

#include "RPCServer.h" // This class definition

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "rpc-if"

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

// Function definition

const ccs::types::char8* RPCServer::GetService (void) const
{

  const ccs::types::char8* service = static_cast<const ccs::types::char8*>(NULL);

  if (static_cast<RPCServerImpl*>(NULL) != __impl)
    {
      service = __impl->GetService();
    }

  return service;

}

RPCServer::RPCServer (const ccs::types::char8 * const service)
{ 
  // Use implementation plug-in factory with default
  __impl = RPCServerImplFactory::GetInstance()->Instantiate();

  bool status = (static_cast<RPCServerImpl*>(NULL) != __impl);

  if (status)
    {
      using namespace std::placeholders;
      status = __impl->RegisterHandler(std::bind(&RPCServer::HandleRequest, this, _1));
    }

  if (status)
    {
      status = __impl->SetService(service);
    }

  if (status)
    {
      status = __impl->Launch();
    }

  if (status)
    {
      log_info("RPCServer::RPCServer('%s') - Initialisation successful", service);
    }
  else
    {
      log_error("RPCServer::RPCServer('%s') - Initialisation error", service);
    }

  return; 

}

RPCServer::~RPCServer (void)
{ 

  if (static_cast<RPCServerImpl*>(NULL) != __impl)
    { 
      delete __impl; 
    }

  __impl= static_cast<RPCServerImpl*>(NULL);

  return;

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
