/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file RPCServerImplFactory.h
 * @brief Header file for RPCServerImplFactory singleton.
 * @date 18/09/2018
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @details This header file contains the definition of the RPCServerImplFactory singleton.
 */

#ifndef _RPCServerImplFactory_h_
#define _RPCServerImplFactory_h_

// Global header files

#include <functional> // std::function

// Local header files

#include "BasicTypes.h"

#include "TemplateFactory.h"

#include "RPCServerImpl.h"

// Constants

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

namespace RPCServerImplFactory { // Implementation plug-in factory

Factory<RPCServerImpl>* GetInstance (void);

// Function definition

} // namespace RPCServerImplFactory

} // namespace base

} // namespace ccs

#endif // _RPCServerImplFactory_h_

