/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

//#include <map> // std::map
#include <new> // std::nothrow
//#include <utility> // std::pair

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "LookUpTable.h"

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyType.h" // Introspectable type definition (base class) ..
#include "AnyTypeHelper.h" // .. associated helper routines

#include "CompoundType.h"
#include "ScalarType.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ccs::base"

// Type definition

namespace ccs {

namespace base {

// Global variables

namespace GlobalTypeDatabase {

AnyTypeDatabase* __p_tdb = NULL_PTR_CAST(AnyTypeDatabase*); // Just instantiate the globally scoped type database

// Function declaration

// Function definition
  
bool IsValid (const ::ccs::types::char8 * const name) { return GetInstance()->IsValid(name); }

bool Register (const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type)
{

  bool status = static_cast<bool>(type);

  if (status)
    {
      status = GetInstance()->Register(type->GetName(), type);
    }
  
  return status;

}

bool Register (const ::ccs::types::AnyType * const type) { return Register(::ccs::base::SharedReference<const ::ccs::types::AnyType>(type)); }

bool Register (const ::ccs::types::char8 * const buffer)
{

  ::ccs::base::SharedReference<::ccs::types::AnyType> type;

  bool status = (0u < ::ccs::HelperTools::Parse(type, buffer));

  if (status)
    {
      status = Register(type);
    }

  return status;

}

void TryAndRegister (const ::ccs::types::char8 * const buffer) 
{ 

  bool status = (false == ::ccs::HelperTools::IsUndefinedString(buffer));

  const ::ccs::types::char8 * p_buf = buffer;

  while (status)
    {
      log_debug("TryAndRegister - .. '%s'", p_buf);

      ::ccs::base::SharedReference<::ccs::types::AnyType> type;

      ::ccs::types::int32 begin = ::ccs::HelperTools::Find(p_buf, '{');

      status = (-1 != begin);

      ::ccs::types::uint32 count = 0u;

      if (status)
        {
          p_buf += begin;
          count = ::ccs::HelperTools::Parse(type, p_buf);
          status = (0u < count);
        }

      if (status)
        {
          (void)Register(type);
        }

      if (status)
        {
          p_buf += count;
        }
    }

  return;

}

const ::ccs::base::SharedReference<const ::ccs::types::AnyType> GetType (const ::ccs::types::char8 * const name)
{ 

  ::ccs::base::SharedReference<const ::ccs::types::AnyType> type;

  bool status = (true == IsValid(name));

  if (status)
    {
      type = GetInstance()->GetElement(name);
    }

  return type;

}

bool Remove (const ::ccs::types::char8 * const name) { return GetInstance()->Remove(name); }

} // namespace GlobalTypeDatabase

} // namespace base

} // namespace ccs
