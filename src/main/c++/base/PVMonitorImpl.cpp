/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow
#include <functional> // std::function

// Local header files

#include "BasicTypes.h" // Global type definition
#include "StringTools.h" // Misc. helper functions

#include "LookUpTable.h"

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include "log-api.h" // Syslog wrapper routines

#include "AnyType.h" // Introspectable data type ..
#include "AnyTypeHelper.h" // .. associated helper routines

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

#include "PVMonitorImpl.h" // This class definition
#include "PVMonitorImplFactory.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pv-if"

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

// Function definition

bool PVMonitorImpl::IsConnected (void) const { return _connected; }

bool PVMonitorImpl::Initialise (void) { return true; }
bool PVMonitorImpl::Terminate (void) { return true; }

const ccs::types::char8* PVMonitorImpl::GetChannel (void) const { return _name; }
bool PVMonitorImpl::SetChannel (const ccs::types::char8 * const channel) { (void)ccs::HelperTools::SafeStringCopy(_name, channel, ccs::types::MaxStringLength); return true; }

bool PVMonitorImpl::SetParameter (const ccs::types::char8 * const name, const ccs::types::char8 * const value) { (void)name; (void)value; return true; }
bool PVMonitorImpl::SetParameter (const ccs::types::char8 * const name, const ::ccs::types::AnyValue& value) { (void)name; (void)value; return true; }

bool PVMonitorImpl::RegisterEventHandler(const std::function<void(const PVMonitor::Event&)>& cb) { _ecb = cb; return true; }
bool PVMonitorImpl::RegisterDataHandler(const std::function<void(const ccs::types::AnyValue&)>& cb) { _dcb = cb; return true; }

void PVMonitorImpl::CallEventHandler (const PVMonitor::Event& event) const
{

  bool status = (NULL != _ecb);

  if (status)
    {
      try
        {
          log_debug("PVMonitorImpl::CallEventHandler - Calling registered handler .. ");
          _ecb(event);
          log_debug("PVMonitorImpl::CallEventHandler - .. returned");
        }
      catch (const std::exception& e)
        {
          log_notice("PVMonitorImpl::CallEventHandler - .. '%s' exception caught", e.what());
        }
      catch (...)
        {
          log_notice("PVMonitorImpl::CallEventHandler - .. unknown exception caught");
        }
    }

  // Update connected status
  *(const_cast<bool*>(&_connected)) = (PVMonitor::Event::Connect == event);

  return;

}

void PVMonitorImpl::CallDataHandler (const ::ccs::types::AnyValue& value) const
{

  bool status = (NULL != _dcb);

  if (status)
    {
      try
        {
          log_debug("PVMonitorImpl::CallDataHandler - Calling registered handler .. ");
          _dcb(value);
          log_debug("PVMonitorImpl::CallDataHandler - .. returned");
        }
      catch (const std::exception& e)
        {
          log_notice("PVMonitorImpl::CallDataHandler - .. '%s' exception caught", e.what());
        }
      catch (...)
        {
          log_notice("PVMonitorImpl::CallDataHandler - .. unknown exception caught");
        }
    }

  return;

}

PVMonitorImpl::PVMonitorImpl (void) : _dcb(NULL), _ecb(NULL), _connected(false)
{ 
  // Initialise attributes
  ccs::HelperTools::SafeStringCopy(_name, STRING_UNDEFINED, ccs::types::MaxStringLength); 

  return; 

}

PVMonitorImpl::~PVMonitorImpl (void) {}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
