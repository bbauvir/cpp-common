/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file RPCServerImpl.h
 * @brief Header file for RPCServerImpl abstract class.
 * @date 18/09/2018
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @details This header file contains the definition of the RPCServerImpl class.
 */

#ifndef _RPCServerImpl_h_
#define _RPCServerImpl_h_

// Global header files

#include <functional> // std::function

// Local header files

#include "BasicTypes.h"

#include "SharedReference.h"

#include "AnyValue.h" // Variable with introspectable data type ..

// Constants

// Type definition

namespace ccs {

namespace base {

/**
 * @brief Abstract base class defining interface for RPC server implementations.
 * @details The base class is provided to be able to support a bridge (PIMPL) pattern
 * for RPC servers independent of the actual transport mechanism.
 *
 * Transport implementations inherit from this base class and provide any necessary
 * means to instantiate a named service implementation over the chosen communication
 * mechanism.
 *
 * The base class provides a mechanism to externally define a handler callback.
 *
 * @code
   class PVAccessRPCServer : public ccs::base::RPCServerImpl, public epics::pvAccess::RPCService
   {

     private:

       // PVAccess-specific constructs
       std::shared_ptr<epics::pvAccess::RPCServer> __server;
       std::shared_ptr<epics::pvAccess::ServerContext> __context;

     public:

       PVAccessRPCServer (void) : ccs::base::RPCServerImpl() {};
       virtual ~PVAccessRPCServer (void) {};

       virtual bool Launch (void) {

         bool status = !ccs::HelperTools::IsUndefinedString(GetService());

         if (status)
           {

             try
               {

                 // Perform implementation-specific service start operations ..
                 __context = epics::pvAccess::startPVAServer(epics::pvAccess::PVACCESS_ALL_PROVIDERS, 0, true, true); 
                 __server = std::shared_ptr<epics::pvAccess::RPCServer>(new epics::pvAccess::RPCServer ()); 

                 __server->registerService(std::string(GetService()), std::shared_ptr<epics::pvAccess::RPCService>(this)); 
                 __server->runInNewThread(); 

               }
             catch (...)
               {
                 status = false;
               }

           }

         return status;

       };

       // PVAccess-specific methods
       std::shared_ptr<epics::pvData::PVStructure> request (std::shared_ptr<epics::pvData::PVStructure> const & __request)
         throw (epics::pvAccess::RPCRequestException);

   };

   @endcode
 *
 * @todo Mechanism to register and instantiate implementation classes.
 */

class RPCServerImpl
{

  private:

    /**
     * @brief Attribute. 
     * @details Used to support the GetService() and SetService() default implementations.
     */

    ccs::types::string __service;

    /**
     * @brief Attribute. 
     * @details Used to ensure the introspectable type is not deleted too early.
     */

    ccs::base::SharedReference<const ccs::types::CompoundType> __reply;

    /**
     * @brief Attribute. 
     * @details RPC handler callback. The registered function is being called to provide
     * a structured reply associated to the structured variable input. It is the
     * responsibility of implementation classes to call the handler by means of the
     * CallHandler() method.
     */

    std::function<ccs::types::AnyValue(const ccs::types::AnyValue&)> __cb;

    RPCServerImpl (const RPCServerImpl& copy); // Undefined
    RPCServerImpl& operator= (const RPCServerImpl& copy); // Undefined

  protected:

  public:

    /**
     * @brief Constructor.
     * @details RPC server is instantiated and service registered.
     */

    RPCServerImpl (void);

    /**
     * @brief Destructor.
     * @details Terminates RPC server, if necessary.
     */

    virtual ~RPCServerImpl (void); 

    /**
     * @brief Accessor. 
     * @details Virtual method with default implementation which just returns the
     * recorded service name.
     * @return Associated service name (nil-terminated character array).
     */

    virtual const ccs::types::char8* GetService (void) const;

    /**
     * @brief Accessor.
     * @details Virtual method with default implementation which just stores the
     * service name for future access. May be overloaded to provide implementation-specific
     * behaviour.
     * @param service Associated service name (nil-terminated character array).
     * @return True if successful, false otherwise.
     */

    virtual bool SetService (const ccs::types::char8 * const service);

    virtual bool Launch (void);
    virtual bool Terminate (void);

    /**
     * @brief Accessor.
     * @param cb Function callback providing service handling.
     * @return True in any condition.
     */

    bool RegisterHandler (const std::function<ccs::types::AnyValue(const ccs::types::AnyValue&)>& cb);

    /**
     * @brief Accessor.
     * @param request Request received as input for RPC.
     * @details Calls the RPC handler callback, if any has been registered. The
     * registered function is being called to provide a structured reply associated
     * to the structured variable input. It is the responsibility of implementation
     * classes to call this method, as necessary.
     *
     * This method does not throw exceptions. It also captures any exception thrown in the
     * handler and provides the exception in the returned structured variable. Unless the
     * called handler causes a segfault, the implementation class may safely call this
     * method, serialise the returned variable over the chosen transport.
     * @return Instance of ccs::types::AnyValue as reply.
     */

    ccs::types::AnyValue CallHandler (const ccs::types::AnyValue& request) const;

};

// Global variables

// Function declaration

// Function definition

} // namespace base

} // namespace ccs

#endif // _RPCServerImpl_h_

