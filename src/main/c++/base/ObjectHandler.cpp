/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "SysTools.h" // ccs::HelperTools::SafeStringCopy, etc.

#include "AnyObject.h"
#include "ObjectDatabase.h"
#include "ObjectFactory.h"

#include "log-api.h" // Logging helper functions

#include "ObjectHandler.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ccs::base"

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

// Function definition
  
bool ObjectHandler::LoadLibrary (const ::ccs::types::char8 * const name) const
{

  bool status = ::ccs::HelperTools::LoadSharedLibrary(name);

  return status;

}

bool ObjectHandler::IsTypeValid (const ::ccs::types::char8 * const name) const
{

  bool status = ::ccs::base::GlobalObjectFactory::IsValid(name);

  return status;

}

bool ObjectHandler::Instantiate (const ::ccs::types::char8 * const type, const ::ccs::types::char8 * const name)
{

  bool status = (IsTypeValid(type) && 
                 !::ccs::HelperTools::IsUndefinedString(name) && 
                 !::ccs::base::ObjectDatabase::IsValid(name));

  ::ccs::base::AnyObject* ref = NULL_PTR_CAST(::ccs::base::AnyObject*);

  if (status)
    {
      ref = ::ccs::base::GlobalObjectFactory::Instantiate(type);
      status = (NULL_PTR_CAST(::ccs::base::AnyObject*) != ref);

      if (status && ref->IsType()) // Check we have truly an instance of ccs::base::AnyObject ..
        {                          // .. before using its interface
          ref->SetInstanceName(name);
        }
    }

  if (status)
    {
      status = (::ccs::base::GlobalObjectDatabase::Register(name, ref) && ::ccs::base::ObjectDatabase::Register(name, ref));
    }

  return status;

}

bool ObjectHandler::Terminate (void)
{

  log_trace("ObjectHandler::Terminate - Entering method");

  bool status = true;

  ::ccs::base::AnyObject* ref = NULL_PTR_CAST(::ccs::base::AnyObject*);

  log_debug("ObjectHandler::Terminate - Looping through LODB of size '%u' ..", ::ccs::base::ObjectDatabase::GetSize());

  for (::ccs::types::uint32 index = 0u; index < ::ccs::base::ObjectDatabase::GetSize(); index += 1u)
    {
      log_debug("ObjectHandler::Terminate - .. '%u' ..", index);

      ref = ::ccs::base::ObjectDatabase::GetInstance(index);
      status = (NULL_PTR_CAST(::ccs::base::AnyObject*) != ref);

      if (status)
        {
          log_debug("ObjectHandler::Terminate - .. GODB::Remove('%s') ..", ref->GetInstanceName());
          status = ::ccs::base::GlobalObjectDatabase::Remove(ref->GetInstanceName());
        }

      if (status)
        {
          log_debug("ObjectHandler::Terminate - .. GOFY::Terminate('%s') ..", ref->GetInstanceName());
          (void)::ccs::base::GlobalObjectFactory::Terminate(ref);
        }
    }

  // Empty LookUpTable
  (void)::ccs::base::ObjectDatabase::Remove();

  log_trace("ObjectHandler::Terminate - Leaving method");

  return status;

}

bool ObjectHandler::Terminate (const ::ccs::types::char8 * const name)
{

  bool status = ::ccs::base::ObjectDatabase::IsValid(name);

  ::ccs::base::AnyObject* ref = NULL_PTR_CAST(::ccs::base::AnyObject*);

  if (status)
    {
      ref = ::ccs::base::ObjectDatabase::GetInstance(name);
      status = (NULL_PTR_CAST(::ccs::base::AnyObject*) != ref);
    }

  if (status)
    {
      status = (::ccs::base::GlobalObjectDatabase::Remove(name) && 
                ::ccs::base::ObjectDatabase::Remove(name));
    }

  if (status)
    {
      (void)::ccs::base::GlobalObjectFactory::Terminate(ref);
    }

  return status;

}

bool ObjectHandler::SetParameter (const ::ccs::types::char8 * const name, const ::ccs::types::char8 * const param, const ::ccs::types::char8 * const value) const
{

  bool status = IsInstanceOf<::ccs::base::CfgableObject>(name);

  ::ccs::base::CfgableObject* ref = NULL_PTR_CAST(::ccs::base::CfgableObject*);

  if (status)
    {
      ref = GetInstance<::ccs::base::CfgableObject>(name);
      status = (NULL_PTR_CAST(::ccs::base::CfgableObject*) != ref);
    }

  if (status)
    {
      status = ref->SetParameter(param, value);
    }

  return status;

}

bool ObjectHandler::ProcessMessage (const ::ccs::types::char8 * const name, const ::ccs::types::char8 * const msg) const
{

  bool status = IsInstanceOf<::ccs::base::MsgableObject>(name);

  ::ccs::base::MsgableObject* ref = NULL_PTR_CAST(::ccs::base::MsgableObject*);

  if (status)
    {
      ref = GetInstance<::ccs::base::MsgableObject>(name);
      status = (NULL_PTR_CAST(::ccs::base::MsgableObject*) != ref);
    }

  if (status)
    {
      status = ref->ProcessMessage(msg);
    }

  return status;

}

ObjectHandler::ObjectHandler (void) {}

ObjectHandler::~ObjectHandler (void) { Terminate(); return; }

} // namespace base

} // namespace ccs

extern "C" {

} // extern C

#undef LOG_ALTERN_SRC
