/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Counter class definition
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file Counter.h
 * @brief Header file for Counter class
 * @date 12/01/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @details This header file contains the definition of the Counter class.
 */

#ifndef _Counter_h_
#define _Counter_h_

// Global header files

// Local header files

#include "BasicTypes.h"

// Constants

// Type definition

namespace ccs {

namespace base {

/**
 * @brief The class provides atomic counting mechanism .. used for counting shared references.
 */

class Counter
{ 

  private:

    /**
     * @brief Counter used for atomic operations.
     */

    volatile ::ccs::types::uint32 __count;

  protected:

  public:

    /**
     * @brief Constructor.
     * @post counter = 0u
     */

    Counter (void);

    /**
     * @brief Copy constructor.
     */

    Counter (const Counter& counter);

    /**
     * @brief Destructor.
     */

    virtual ~Counter (void);

    /**
     * @brief Accessor. 
     */

    ::ccs::types::uint32 GetCount (void) const;
    ::ccs::types::uint32 Increment (void);
    ::ccs::types::uint32 Decrement (void);
       
    /**
     * @brief Copy assignment operator.
     */

    Counter& operator= (const Counter& counter);

};

// Global variables

// Function declaration

// Function definition

} // namespace base

} // namespace ccs

#endif // _Counter_h_

