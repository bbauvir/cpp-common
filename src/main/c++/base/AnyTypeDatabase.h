/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Generic type class definition
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file AnyTypeDatabase.h
 * @brief Header file for AnyTypeDatabase class.
 * @date 11/11/2018
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @details This header file contains the definition of the singleton AnyTypeDatabase interface.
 * The AnyTypeDatabase class definition is not exposed through this header file. The interface
 * to a singleton instance is provided. Using this interface in an application causes an implicit
 * instantiation of the AnyTypeDatabase class and registration of all built-in ScalarType. Types
 * are stored using smart pointers. This ensures persistence of AnyType definitions throughout the
 * lifetime of an application without requiring the application to manage instances and/or
 * references.
 */

#ifndef _AnyTypeDatabase_h_
#define _AnyTypeDatabase_h_

// Global header files

//#include <memory> // std::shared_ptr

// Local header files

#include "log-api.h" // Syslog wrapper routines

#include "LookUpTable.h"

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyType.h"
#include "ArrayType.h"
#include "CompoundType.h"
#include "ScalarType.h"

// Constants

// Type definition

namespace ccs {

namespace base {

typedef ::ccs::base::LookUpTable< ::ccs::base::SharedReference<const ::ccs::types::AnyType > > AnyTypeDatabase;

// Global variables

namespace GlobalTypeDatabase {

extern AnyTypeDatabase* __p_tdb;

// Function declaration

/**
 * @brief Test if named type is registered to the GlobalTypeDatabase.
 * @return True if type is registered.
 */

bool IsValid (const ::ccs::types::char8 * const name);

/**
 * @brief Register a type definition to the GlobalTypeDatabase.
 * @details The type must be defined with a unique name.
 * @param type The type definition to register.
 * @pre
 *   ccs::base::GlobalTypeDatabase::IsValid(type->GetName()) == false
 * @return True if successful.
 *
 * @code
   // Introspectable type definition
   ccs::types::CompoundType* type = (new (std::nothrow) ccs::types::CompoundType ("MyValueType_t/v1.0"))
                                      ->AddAttribute("valid", "boolean")
                                      ->AddAttribute("setpoint", "float64");

   bool status = (static_cast<ccs::types::CompoundType*>(NULL) != type);

   if (status)
     {
       // Register type for future reference
       status = ccs::base::GlobalTypeDatabase::Register(type);
     }

   if (status)
     {
       // Extend type definition .. e.g. with timestamp for archiving purposes
       ccs::types::char8 type [] = "{\"type\":\"MyOtherType_t/v1.0\","
                                    "\"attributes\":["
                                                    "{\"timestamp\":{\"type\":\"uint64\"}},"
                                                    "{\"value\":{\"type\":\"MyValueType_t/v1.0\"}}"
                                                   "]"
                                   "}";

       // Register type for future reference
       status = ccs::base::GlobalTypeDatabase::Register(type);
     }
   @endcode
 */

bool Register (const ::ccs::types::AnyType * const type);
bool Register (const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type);
bool Register (const ::ccs::types::char8 * const type);

/**
 * @brief Register type definitions to the GlobalTypeDatabase.
 * @details The buffer will be searched for type definitions and these will be parsed and
 * registered if found.
 * @param type The type definition to register.
 * @pre
 *   ccs::base::GlobalTypeDatabase::IsValid(type->GetName()) == false
 *
 * @code
     // Extend type definition .. e.g. with timestamp for archiving purposes
     ccs::types::char8 topic [] = 
       "<topic name =\"sdn::MyTopicName\" dataType=\"sdn::MyTopicType/v1.0\">"
         "<dataTypes>"
           "<dataType format=\"json\">"
             "{\"type\":\"sdn::MyTopicHeader\","
              "\"attributes\":["
                              "{\"index\":{\"type\":\"uint64\"}},"
                              "{\"timestamp\":{\"type\":\"uint64\"}}"
                             "]"
             "}"
           "</dataType>"
           "<dataType format=\"json\">"
             "{\"type\":\"sdn::MyTopicType/v1.0\","
              "\"attributes\":["
                              "{\"header\":{\"type\":\"sdn::MyTopicHeader\"}},"
                              "{\"value\":{\"type\":\"float64\"}}"
                             "]"
             "}"
           "</dataType>"
         "</dataTypes>"
         "<attributes>"
           "<attribute name=\"header.timestamp\" qualifier=\"timestamp\"/>"
           "<attribute name=\"value\" description=\"Some description\" unit=\"V\"/>"
         "</attributes>"      
       "</topic>";

     // Register type for future reference
     ccs::base::GlobalTypeDatabase::TryAndRegister(topic);
   @endcode
 */

void TryAndRegister (const ::ccs::types::char8 * const type);

/**
 * @brief Retrieve type definition from the GlobalTypeDatabase.
 * @param name The name of the type definition to retrieve.
 * @pre
 *   ccs::base::GlobalTypeDatabase::IsValid(name) == true
 * @return Valid shared pointer if successful.
 */

const ::ccs::base::SharedReference<const ::ccs::types::AnyType> GetType (const ::ccs::types::char8 * const name);

/**
 * @brief Remove a type definition from the GlobalTypeDatabase.
 * @param name The name of the type definition to remove.
 * @pre
 *   ccs::base::GlobalTypeDatabase::IsValid(name) == true
 * @return True if successful.
 */

bool Remove (const ::ccs::types::char8 * const name);

/**
 * @brief Retrieve type definition from the GlobalTypeDatabase.
 * @details The returned smart pointer is dynamically cast to the 
 * template Type defined by the application.
 * @param name The name of the type definition to retrieve.
 * @pre
 *   ccs::base::GlobalTypeDatabase::IsValid(name) == true
 * @return Valid shared pointer if successful.
 */

template <typename Type> inline const ::ccs::base::SharedReference<const Type> GetAsType (const ::ccs::types::char8 * const name);

// Function definition

/**
 * @return The GlobalTypeDatabase singleton instance.
 * @note Expose built-in type in order to get rid of the AnyTypeDatabase class wrapping
 * the lookup table without much added value.
 * @todo Re-consider hiding the implementation details in case the LookUpTable interface
 * changes in the future.
 */

static inline AnyTypeDatabase* GetInstance (void)
{

  log_trace("GlobalTypeDatabase::GetInstance - Entering method"); 

  if (__builtin_expect((NULL_PTR_CAST(AnyTypeDatabase*) == __p_tdb), 0)) // Unlikely
    {
      __p_tdb = new (std::nothrow) AnyTypeDatabase ();

      if (NULL_PTR_CAST(AnyTypeDatabase*) != __p_tdb)
        {
          // Register scalar types
          (void)Register(::ccs::types::Boolean);
          (void)Register(::ccs::types::Character8);
          (void)Register(::ccs::types::SignedInteger8);
          (void)Register(::ccs::types::UnsignedInteger8);
          (void)Register(::ccs::types::SignedInteger16);
          (void)Register(::ccs::types::UnsignedInteger16);
          (void)Register(::ccs::types::SignedInteger32);
          (void)Register(::ccs::types::UnsignedInteger32);
          (void)Register(::ccs::types::SignedInteger64);
          (void)Register(::ccs::types::UnsignedInteger64);
          (void)Register(::ccs::types::Float32);
          (void)Register(::ccs::types::Float64);
          (void)Register(::ccs::types::String);
        }
    }
  
  log_trace("GlobalTypeDatabase::GetInstance - Leaving method"); 

  return __p_tdb; 

}

template <typename Type> inline const ::ccs::base::SharedReference<const Type> GetAsType (const ::ccs::types::char8 * const name)
{

  ::ccs::base::SharedReference<const Type> type;

  bool status = (true == IsValid(name));

  if (status)
    {
      type = GetType(name);
      status = static_cast<bool>(type);
    }

  if (!status)
    {
      log_warning("GetAsType<CompoundType> - Invalid type '%s'", name);
    }

  return type;

}

} // namespace GlobalTypeDatabase

} // namespace base

} // namespace ccs

#endif // _AnyTypeDatabase_h_ 

