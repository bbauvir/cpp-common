/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow
#include <functional> // std::function

// Local header files

#include "BasicTypes.h" // Global type definition
#include "StringTools.h" // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include "log-api.h" // Syslog wrapper routines

#include "AnyType.h" // Introspectable data type ..
#include "AnyTypeHelper.h" // .. associated helper routines

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

#include "RPCServerImpl.h" // This class definition
#include "RPCTypes.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "rpc-if"

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

// Function definition

const ccs::types::char8* RPCServerImpl::GetService (void) const { return __service; }
bool RPCServerImpl::SetService (const ccs::types::char8 * const service) { ccs::HelperTools::SafeStringCopy(__service, service, STRING_MAX_LENGTH); return true; }

bool RPCServerImpl::RegisterHandler (const std::function<ccs::types::AnyValue(const ccs::types::AnyValue&)>& cb) { __cb = cb; return true; }
ccs::types::AnyValue RPCServerImpl::CallHandler (const ccs::types::AnyValue& request) const
{

  bool status = (NULL != __cb);

  ccs::types::AnyValue reply;

  ccs::types::string reason; ccs::HelperTools::SafeStringCopy(reason, "Unregistered service handler", ccs::types::MaxStringLength);

  if (status)
    {
      try
        {
          log_debug("RPCServerImpl::CallHandler - Calling registered handler .. ");
          reply = __cb(request);
          log_debug("RPCServerImpl::CallHandler - .. returned");
        }
      catch (const std::exception& e)
        {
          log_notice("RPCServerImpl::CallHandler - .. '%s' exception caught", e.what());
          ccs::HelperTools::SafeStringCopy(reason, e.what(), ccs::types::MaxStringLength);
          status = false;
        }
      catch (...)
        {
          log_notice("RPCServerImpl::CallHandler - .. unknown exception caught");
          ccs::HelperTools::SafeStringCopy(reason, "Unknown exception", ccs::types::MaxStringLength);
          status = false;
        }
    }

  if (!status)
    {
      // Provide error reply
      reply = ccs::HelperTools::InstantiateRPCReply(status, "failure", reason);
    }

  return reply;

}

bool RPCServerImpl::Launch (void) { return true; }
bool RPCServerImpl::Terminate (void) { return true; }

RPCServerImpl::RPCServerImpl (void) : __cb(NULL)
{ 
  // Initialise attributes
  ccs::HelperTools::SafeStringCopy(__service, STRING_UNDEFINED, STRING_MAX_LENGTH); 
  __reply = ccs::types::Reply_int; // Take a local copy .. ensure it does not go out of scope as long as this instance is running

  // Register types in GlobalTypeDatabase
  ccs::HelperTools::InitialiseRPCTypes(); 

  return; 

}

RPCServerImpl::~RPCServerImpl (void)
{ 

  Terminate();

  return;

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
