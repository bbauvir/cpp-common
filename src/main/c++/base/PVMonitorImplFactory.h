/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVMonitorImplFactory.h
 * @brief Header file for PVMonitorImplFactory singleton.
 * @date 01/07/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @details This header file contains the definition of the PVMonitorImplFactory singleton.
 */

#ifndef _PVMonitorImplFactory_h_
#define _PVMonitorImplFactory_h_

// Global header files

// Local header files

#include "BasicTypes.h"

#include "TemplateFactory.h"

#include "PVMonitorImpl.h"

// Constants

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

namespace PVMonitorImplFactory { // Implementation plug-in factory

Factory<PVMonitorImpl>* GetInstance (void);

// Function definition

} // namespace PVMonitorFactory

} // namespace base

} // namespace ccs

#endif // _PVMonitorImplFactory_h_

