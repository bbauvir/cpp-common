/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file RPCClientImplFactory.h
 * @brief Header file for RPCClientImplFactory singleton.
 * @date 18/09/2018
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @details This header file contains the definition of the RPCClientImplFactory singleton.
 */

#ifndef _RPCClientImplFactory_h_
#define _RPCClientImplFactory_h_

// Global header files

// Local header files

#include "BasicTypes.h"

#include "TemplateFactory.h"

#include "RPCClientImpl.h"

// Constants

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

namespace RPCClientImplFactory { // Implementation plug-in factory

Factory<RPCClientImpl>* GetInstance (void);

} // namespace RPCClientImplFactory

} // namespace base

} // namespace ccs

#endif // _RPCClientImplFactory_h_

