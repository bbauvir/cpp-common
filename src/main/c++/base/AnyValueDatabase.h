/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Generic type class definition
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file AnyValueDatabase.h
 * @brief Header file for AnyValueDatabase class.
 * @date 11/11/2018
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @details This header file contains the definition of the singleton AnyValueDatabase interface.
 * The AnyValueDatabase class definition is not exposed through this header file. The interface
 * to a singleton instance is provided. Using this interface in an application causes an implicit
 * instantiation of the AnyValueDatabase classe. The database acts as a global cache where AnyValue
 * instances may be stored, and accessed for read/write operations.
 */

#ifndef _AnyValueDatabase_h_
#define _AnyValueDatabase_h_

// Global header files

//#include <memory> // std::shared_ptr

// Local header files

#include "log-api.h" // Syslog wrapper routines

#include "LookUpTable.h"

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyValue.h"

// Constants

// Type definition

namespace ccs {

namespace base {

typedef ::ccs::base::LookUpTable< ::ccs::base::SharedReference< ::ccs::types::AnyValue > > AnyValueDatabase;

// Global variables

namespace GlobalVariableCache {

extern AnyValueDatabase* __p_vdb;

// Function declaration

/**
 * @brief Test if named variable is registered to the GlobalVariableCache.
 * @return True if variable is registered.
 */

bool IsValid (const ::ccs::types::char8 * const name);

/**
 * @brief Instantiate and register a variable to the GlobalVariableCache.
 */

bool Register (const ::ccs::types::char8 * const name, const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type);

/**
 * @brief Retrieve type definition from the GlobalVariableCache.
 * @param name The name of the variable definition to retrieve.
 * @pre
 *   ccs::types::GlobalVariableCache::IsValid(name) == true
 * @return Valid shared pointer if successful.
 */

const ::ccs::base::SharedReference<const ::ccs::types::AnyType> GetType (const ::ccs::types::char8 * const name);

/**
 * @brief Remove a variable from the GlobalVariableCache.
 */

bool Remove (const ::ccs::types::char8 * const name);

/**
 * @brief Templated accessors for the GlobalVariableCache.
 */

template <typename Type> inline Type GetValue (const ::ccs::types::char8 * const name);
template <typename Type> inline bool GetValue (const ::ccs::types::char8 * const name, Type& value);
template <typename Type> inline bool SetValue (const ::ccs::types::char8 * const name, Type& value);

// Function definition

/**
 * @return The GlobalVariableCache singleton instance.
 * @todo Re-consider hiding the implementation details in case the LookUpTable interface
 * changes in the future.
 */

static inline AnyValueDatabase* GetInstance (void)
{

  if (__builtin_expect((NULL_PTR_CAST(AnyValueDatabase*) == __p_vdb), 0)) // Unlikely
    {
      __p_vdb = new (std::nothrow) AnyValueDatabase ();
    }
  
  return __p_vdb; 

}

template <typename Type> inline Type GetValue (const ::ccs::types::char8 * const name)
{

  bool status = (true == IsValid(name));

  Type value; // WARNING - Uninitialised

  if (status)
    {
      (void)GetValue<Type>(name, value);
    }

  return value;

}

template <typename Type> inline bool GetValue (const ::ccs::types::char8 * const name, Type& value)
{

  bool status = (true == IsValid(name));

  if (status)
    {
      value = static_cast<Type>(*((GetInstance()->GetElement(name)).GetReference()));
    }

  return status;

}

template <typename Type> inline bool SetValue (const ::ccs::types::char8 * const name, Type& value)
{

  bool status = (true == IsValid(name));

  if (status)
    {
      *((GetInstance()->GetElement(name)).GetReference()) = value;
    }

  return status;

}

} // namespace GlobalVariableCache

} // namespace base

} // namespace ccs

#endif // _AnyValueDatabase_h_ 

