/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

// Local header files

#include "BasicTypes.h" // Global type definition

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include "log-api.h" // Syslog wrapper routines

#include "TemplateFactory.h"

#include "PVMonitorImpl.h"
#include "PVMonitorImplFactory.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pv-if"

// Type definition

namespace ccs {

namespace base {

// Global variables

namespace PVMonitorImplFactory { // Implementation factory

// Function declaration

// Function definition

Factory<PVMonitorImpl>* GetInstance (void) { return Factory<PVMonitorImpl>::GetInstance(); }

} // namespace PVMonitorImplFactory

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
