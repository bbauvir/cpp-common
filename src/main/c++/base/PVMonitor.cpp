/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow
#include <functional> // std::function

// Local header files

#include "BasicTypes.h" // Global type definition
#include "StringTools.h" // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include "log-api.h" // Syslog wrapper routines

#include "AnyType.h" // Introspectable data type ..
#include "AnyTypeHelper.h" // .. associated helper routines

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

#include "PVMonitorImpl.h"
#include "PVMonitorImplFactory.h"

#include "PVMonitor.h" // This class definition

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pv-if"

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

// Function definition

bool PVMonitor::Initialise (void)
{
  // Better be safe than sorry
  bool status = (NULL_PTR_CAST(PVMonitorImpl*) != _impl);

  if (status)
    {
      log_info("PVMonitor::Initialise('%s') - Register monitor callback ..", GetChannel());
      using namespace std::placeholders;
      status = _impl->RegisterDataHandler(std::bind(&PVMonitor::HandleMonitor, this, _1));
    }

  if (status)
    {
      log_info("PVMonitor::Initialise('%s') - Register event callback ..", GetChannel());
      using namespace std::placeholders;
      status = _impl->RegisterEventHandler(std::bind(&PVMonitor::HandleEvent, this, _1));
    }

  if (status)
    {
      log_info("PVMonitor::Initialise('%s') - Initialise implementation ..", GetChannel());
      status = _impl->Initialise();
    }
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("PVMonitor::Initialise('%s') - .. successful", GetChannel());
    }
  else
    {
      log_error("PVMonitor::Initialise('%s') - .. unsuccessful", GetChannel());
    }
#endif
  return status;

}

bool PVMonitor::IsConnected (void) const
{ 
  // Better be safe than sorry
  bool status = (NULL_PTR_CAST(PVMonitorImpl*) != _impl);

  if (status)
    {
      status = _impl->IsConnected(); 
    }

  return status;

}

bool PVMonitor::SetParameter (const ccs::types::char8 * const name, const ccs::types::char8 * const value)
{ 
  // Better be safe than sorry
  bool status = (NULL_PTR_CAST(PVMonitorImpl*) != _impl);

  if (status)
    {
      status = _impl->SetParameter(name, value); 
    }

  return status;

}

bool PVMonitor::SetParameter (const ccs::types::char8 * const name, const ::ccs::types::AnyValue& value)
{ 
  // Better be safe than sorry
  bool status = (NULL_PTR_CAST(PVMonitorImpl*) != _impl);

  if (status)
    {
      status = _impl->SetParameter(name, value); 
    }

  return status;

}

bool PVMonitor::SetChannel (const ccs::types::char8 * const channel)
{ 
  // Better be safe than sorry
  bool status = (NULL_PTR_CAST(PVMonitorImpl*) != _impl);

  if (status)
    {
      status = _impl->SetChannel(channel); 
    }

  return status;

}

const ccs::types::char8* PVMonitor::GetChannel (void) const
{ 
  // Better be safe than sorry
  bool status = (NULL_PTR_CAST(PVMonitorImpl*) != _impl);
  const ccs::types::char8* ret = NULL_PTR_CAST(const ccs::types::char8*);

  if (status)
    {
      ret = _impl->GetChannel(); 
    }

  return ret;

}

const PVMonitorImpl* PVMonitor::GetImplementation (void) const { return _impl; }

void PVMonitor::HandleEvent (const PVMonitor::Event& event)
{

  if (PVMonitor::Event::Connect == event)
    {
      log_notice("PVMonitor::HandleEvent - Connect to '%s'", GetChannel());
    }

  if (PVMonitor::Event::Timeout == event)
    {
      log_notice("PVMonitor::HandleEvent - Timeout for '%s'", GetChannel());
    }

  if (PVMonitor::Event::Disconnect == event)
    {
      log_notice("PVMonitor::HandleEvent - Disconnect from '%s'", GetChannel());
    }

  return;

}

PVMonitor::PVMonitor (void) : _impl(NULL_PTR_CAST(PVMonitorImpl*))
{ 
  // Use implementation plug-in factory with default
  _impl = PVMonitorImplFactory::GetInstance()->Instantiate();

  return; 

}

PVMonitor::PVMonitor (const ccs::types::char8 * const channel) : _impl(NULL_PTR_CAST(PVMonitorImpl*))
{ 
  // Use implementation plug-in factory with default
  _impl = PVMonitorImplFactory::GetInstance()->Instantiate();

  bool status = (NULL_PTR_CAST(PVMonitorImpl*) != _impl);

  if (status)
    {
      (void)SetChannel(channel);
      (void)Initialise();
    }

  return; 

}

PVMonitor::PVMonitor (const ccs::types::char8 * const channel, const ccs::types::char8 * const plugin) : _impl(NULL_PTR_CAST(PVMonitorImpl*))
{ 

  bool status = PVMonitorImplFactory::GetInstance()->IsValid(plugin);

  if (status)
    {  // Use implementation plug-in factory with provided attribute
      _impl = PVMonitorImplFactory::GetInstance()->Instantiate(plugin);
       status = (NULL_PTR_CAST(PVMonitorImpl*) != _impl);
    }

  if (status)
    {
      (void)SetChannel(channel);
      (void)Initialise();
    }

  return; 

}

PVMonitor::~PVMonitor (void)
{ 

  if (NULL_PTR_CAST(PVMonitorImpl*) != _impl)
    { 
      _impl->Terminate();
      delete _impl; 
    }

  _impl = NULL_PTR_CAST(PVMonitorImpl*);

  return;

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
