/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file SharedReference.h
 * @brief Header file for SharedReference class.
 * @date 01/06/20189
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @details This header file contains the definition of the SharedReference class.
 */

#ifndef _SharedReference_h_
#define _SharedReference_h_

// Global header files

#include <new> // std::nothrow

// Local header files

#include "BasicTypes.h" // Global type definition

#include "log-api.h"

#include "Counter.h"

// Constants

// Type definition

namespace ccs {

namespace base {

/**
 * @brief Implementation class providing support for RTTI and reference counting.
 */

template <typename Type> class SharedReference
{

  private:

    /**
     * @brief Attribute.
     * @details Shared counter.
     */

    Counter* _counter;

    /**
     * @brief Attribute.
     * @details Reference counted pointer.
     */

    Type* _pointer;

  protected:

  public:

    /**
     * @brief Constructor. NOOP.
     */

    SharedReference (void) : _counter(NULL_PTR_CAST(Counter*)), _pointer(NULL_PTR_CAST(Type*)) {};
    SharedReference (Type* ref) : _counter(NULL_PTR_CAST(Counter*)), _pointer(ref)
    {

      bool status = IsValid();

      if (status)
        {
          _counter = new (std::nothrow) Counter;
          status = (NULL_PTR_CAST(Counter*) != _counter);
        }

      if (status)
        {
          _counter->Increment();
        }

      return;

    };

    SharedReference (Type* ref, Counter* counter) : _counter(counter), _pointer(ref) // In order to cast away constness !!
    {

      bool status = IsValid();

      if (status)
        {
          status = (NULL_PTR_CAST(Counter*) != _counter);
        }

      if (status)
        {
          _counter->Increment();
        }

      return;

    };

    /**
     * @brief Copy constructor.
     */

    SharedReference (const SharedReference<Type>& ref) : _counter(ref.GetCounter()), _pointer(ref.GetReference())
    {

      bool status = IsValid();

      if (status)
        {
          status = (NULL_PTR_CAST(Counter*) != _counter);
        }

      if (status)
        {
          _counter->Increment();
        }

      return;

    }

    template <typename Origin> SharedReference (const SharedReference<Origin>& ref) : _counter(ref.GetCounter()), _pointer(dynamic_cast<Type*>(ref.GetReference()))
    {

      bool status = IsValid();

      if (status)
        {
          status = (NULL_PTR_CAST(Counter*) != _counter);
        }

      if (status)
        {
          _counter->Increment();
        }

      return;

    }

    /**
     * @brief Destructor.
     */

    virtual ~SharedReference (void)
    {

      Discard();

      return;

    }

    /**
     * @brief Accessor.
     * @return True if the reference is valid.
     */

    bool IsValid (void) const { return (NULL_PTR_CAST(Type*) != _pointer); };

    /**
     * @brief Accessor.
     * @return Encapsulated reference.
     */

    Counter* GetCounter (void) const { return _counter; };
    Type* GetReference (void) const { return _pointer; };

    ccs::types::uint32 GetCount (void) const
    {

      ccs::types::uint32 count = 0u;

      if (NULL_PTR_CAST(Counter*) != _counter)
        {
          count = _counter->GetCount();
        }

      return count;

    };

    void Discard (void)
    {

      bool status = IsValid();

      if (status)
        {
          status = (NULL_PTR_CAST(Counter*) != _counter);
        }

      if (status)
        {
          _counter->Decrement();
        }

      if (status && (0u == _counter->GetCount()))
        {
          delete _counter;
          delete _pointer;
        }

      _counter = NULL_PTR_CAST(Counter*);
      _pointer = NULL_PTR_CAST(Type*);

      return;

    };

    /**
     * @brief Copy assignment operator.
     */

    SharedReference<Type>& operator= (const SharedReference<Type>& ref)
    {

      bool status = (this != &ref);

      if (status)
        {
          Discard();
        }

      if (status)
        {
          _counter = ref.GetCounter();
          _pointer = ref.GetReference();
          status = IsValid();
        }

      if (status)
        {
          status = (NULL_PTR_CAST(Counter*) != _counter);
        }

      if (status)
        {
          _counter->Increment();
        }

      return *this;

    };

    template <typename Origin> SharedReference<Type>& operator= (const SharedReference<Origin>& ref) // With RTTI
    {

      Discard();

      _counter = ref.GetCounter();
      _pointer = dynamic_cast<Type*>(ref.GetReference());

      bool status = IsValid();

      if (status)
        {
          status = (NULL_PTR_CAST(Counter*) != _counter);
        }

      if (status)
        {
          _counter->Increment();
        }

      return *this;

    }

    /**
     * @brief Comparison operator.
     */

    bool operator== (const SharedReference<Type>& ref) const
    {

      bool status = (this->GetReference() == ref.GetReference());

      return status;

    };

    operator bool() const { return IsValid(); };

    /**
     * @brief Member access operator.
     */

    Type* operator-> (void) const { return _pointer; };

    /**
     * @brief Indirection operator.
     */

    Type& operator* (void) const { return *_pointer; };

};

// Global variables

// Function declaration

// Function definition

} // namespace base

} // namespace ccs

#endif // _SharedReference_h_

