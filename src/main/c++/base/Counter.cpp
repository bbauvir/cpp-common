/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

// Local header files

#include "BasicTypes.h"

#include "Counter.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ccs::base"

// Type definition

// Global variables

// Function declaration

// Function definition

namespace ccs {

namespace base {

::ccs::types::uint32 Counter::GetCount (void) const { return __count; }
::ccs::types::uint32 Counter::Increment (void) { return __sync_add_and_fetch(&__count, 1u); }

::ccs::types::uint32 Counter::Decrement (void) 
{ 

  ::ccs::types::uint32 count = GetCount();

  bool status = (0u != count);

  if (status)
    {
      count = __sync_sub_and_fetch(&__count, 1u);
    }

  return count; 

}

Counter& Counter::operator= (const Counter& counter)
{ 

  bool status = (this != &counter); 

  if (status) 
    { 
      __count = counter.GetCount(); 
    } 

  return *this; 

}

Counter::Counter (void) : __count(0u) {}
Counter::Counter (const Counter& counter) : __count(counter.GetCount()) {}

Counter::~Counter (void) {}

} // namespace base

} // namespace ccs
