/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file RPCClient.h
 * @brief Header file for RPCClient interface class.
 * @date 18/09/2018
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @details This header file contains the definition of the RPCClient interface class.
 */

#ifndef _RPCClient_h_
#define _RPCClient_h_

// Global header files

// Local header files

#include "BasicTypes.h"

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

// Constants

// Type definition

namespace ccs {

namespace base {

class RPCClientImpl; // Forward class declaration

/**
 * @brief Interface class providing support for RPC client.
 * @details ToDo.
 *
 * @note The design is based on a bridge pattern to avoid exposing technology-specific
 * internals through the interface class.
 */

class RPCClient
{

  private:

    /**
     * @brief Attribute. 
     * @details Bridge (PIMPL) pattern.
     */

    RPCClientImpl* __impl;

  protected:

  public:

    /**
     * @brief Constructor. NOOP.
     */

    RPCClient (void);

    /**
     * @brief Constructor.
     * @details RPC client is instantiated and connection to the RPC service is
     * issued.
     * @param service RPC service name (nil-terminated character array).
     */

    RPCClient (const ccs::types::char8 * const service);

    /**
     * @brief Destructor.
     */

    virtual ~RPCClient (void); 

    /**
     * @brief Accessor.
     * @return True if the service is currently connected.
     */

    bool IsConnected (void) const;

    /**
     * @brief Accessor.
     * @return Associated RPC service name (nil-terminated character array).
     */

    const ccs::types::char8* GetService (void) const;

    /**
     * @brief Accessor.
     * @param service Associated RPC service name (nil-terminated character array).
     * @return True if successful.
     */

    bool SetService (const ccs::types::char8 * const service);

    /**
     * @brief Launch method.
     * @details The RPC client gets instantiated and connection to the RPC service is
     * issued and verified.
     * @return True if successful.
     */

    bool Launch (void);

    /**
     * @brief SendRequest method.
     * @details The request is sent to the RPC server and reply is returned.
     * @return Reply from the RPC server.
     */

    ccs::types::AnyValue SendRequest (const ccs::types::AnyValue& request) const;

};

// Global variables

// Function declaration

// Function definition

} // namespace base

} // namespace ccs

#endif // _RPCClient_h_

