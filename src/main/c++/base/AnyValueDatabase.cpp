/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "LookUpTable.h"

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyValue.h" // Introspectable type definition (base class) ..
#include "AnyValueHelper.h" // .. associated helper routines

#include "AnyValueDatabase.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ccs::base"

// Type definition

namespace ccs {

namespace base {

// Global variables

namespace GlobalVariableCache {

AnyValueDatabase* __p_vdb; // Just instantiate the globally scoped variable cache

// Function declaration

// Function definition
  
bool IsValid (const ::ccs::types::char8 * const name) { return GetInstance()->IsValid(name); }

bool Register (const ::ccs::types::char8 * const name, const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type)
{

  bool status = static_cast<bool>(type);

  if (status)
    {
      status = (false == IsValid(name));
    }

  ::ccs::base::SharedReference<::ccs::types::AnyValue> value;

  if (status)
    {
      // Instantiate AnyValue
      value = ::ccs::base::SharedReference<::ccs::types::AnyValue>(new (std::nothrow) ::ccs::types::AnyValue (type));
    }

  if (status)
    {
      status = GetInstance()->Register(name, value);
    }
  
  return status;

}

const ::ccs::base::SharedReference<const ::ccs::types::AnyType> GetType (const ::ccs::types::char8 * const name)
{ 

  ::ccs::base::SharedReference<const ::ccs::types::AnyType> type;

  bool status = (true == IsValid(name));

  if (status)
    {
      type = GetInstance()->GetElement(name)->GetType();
    }

  return type;

}

bool Remove (const ::ccs::types::char8 * const name)
{ 

  bool status = (true == IsValid(name));

  if (status)
    {
      // Let smart pointers do their magic
      status = GetInstance()->Remove(name);
    }

  return status; 

}

} // namespace GlobalTypeDatabase

} // namespace types

} // namespace ccs
