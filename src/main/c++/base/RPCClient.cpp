/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

// Local header files

#include "BasicTypes.h" // Global type definition
#include "StringTools.h" // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include "log-api.h" // Syslog wrapper routines

#include "AnyType.h" // Introspectable data type ..
#include "AnyTypeHelper.h" // .. associated helper routines

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

#include "RPCTypes.h" // .. associated types

#include "RPCClientImpl.h"
#include "RPCClientImplFactory.h"

#include "RPCClient.h" // This class definition

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "rpc-if"

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

// Function definition

bool RPCClient::Launch (void) { return __impl->Launch(); }

bool RPCClient::IsConnected (void) const { return __impl->IsConnected(); }

const ccs::types::char8* RPCClient::GetService (void) const { return __impl->GetService(); }
bool RPCClient::SetService (const ccs::types::char8 * const service) { return __impl->SetService(service); }

ccs::types::AnyValue RPCClient::SendRequest (const ccs::types::AnyValue& request) const
{ 

  bool status = (static_cast<RPCClientImpl*>(NULL) != __impl);

  ccs::types::AnyValue reply;

  ccs::types::string reason; ccs::HelperTools::SafeStringCopy(reason, "Uninitialised implementation", ccs::types::MaxStringLength);

  if (status)
    {
      try
        {
          log_debug("RPCClient::SendRequest - Calling implementation method .. ");
          reply = __impl->SendRequest(request);
          log_debug("RPCClient::SendRequest - .. returned");
        }
      catch (const std::exception& e)
        {
          log_notice("RPCClient::SendRequest - .. '%s' exception caught", e.what());
          ccs::HelperTools::SafeStringCopy(reason, e.what(), ccs::types::MaxStringLength);
          status = false;
        }
      catch (...)
        {
          log_notice("RPCClient::SendRequest - .. unknown exception caught");
          ccs::HelperTools::SafeStringCopy(reason, "Unknown exception", ccs::types::MaxStringLength);
          status = false;
        }
    }

  if (!status)
    {
      // Provide error reply
      reply = ccs::HelperTools::InstantiateRPCReply(status, "failure", reason);
    }

  return reply; 

}

// Use implementation plug-in factory with default (unnamed) transport
RPCClient::RPCClient (void) { __impl = RPCClientImplFactory::GetInstance()->Instantiate(); return; }

RPCClient::RPCClient (const ccs::types::char8 * const service)
{ 
  // Use implementation plug-in factory with default
  __impl = RPCClientImplFactory::GetInstance()->Instantiate();

  bool status = (static_cast<RPCClientImpl*>(NULL) != __impl);

  if (status)
    {
      status = this->SetService(service);
    }

  if (status)
    {
      status = this->Launch();
    }

  if (status)
    {
      log_info("RPCClient::RPCClient('%s') - Initialisation successful", service);
    }
  else
    {
      log_error("RPCClient::RPCClient('%s') - Initialisation error", service);
    }

  return; 

}

RPCClient::~RPCClient (void)
{ 

  if (static_cast<RPCClientImpl*>(NULL) != __impl)
    { 
      delete __impl; 
    }

  __impl= static_cast<RPCClientImpl*>(NULL);

  return;

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
