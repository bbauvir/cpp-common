/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file TemplateFactory.h
 * @brief Header file for Factory<T> singleton.
 * @date 01/07/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @details This header file contains the definition of the Factory<T> singleton.
 */

#ifndef _TemplateFactory_h_
#define _TemplateFactory_h_

// Global header files

// Local header files

#include "BasicTypes.h"

#include "LookUpTable.h"

// Constants

// Type definition

namespace ccs {

namespace base {

template <typename Type> class Factory : public LookUpTable<Type*(*)(void)> 
{

  private:

    ccs::types::string __default;
    static Factory* __instance;

    Factory (void);
    Factory (const Factory& copy); // Undefined
    Factory& operator= (const Factory& copy); // Undefined

  public:

    static Factory* GetInstance (void)
    {

      if (NULL_PTR_CAST(Factory*) == __instance)
        {
          __instance = new (std::nothrow) Factory;
        }
      
      return __instance;

    }

    virtual ~Factory (void);

    const ccs::types::char8* GetDefault (void) const;
    bool SetDefault (const ccs::types::char8 * const name);

    Type* Instantiate (const ccs::types::char8 * const name);

    /**
     * @brief Instantiates default plug-in.
     * @return Plug-in instance if successful.
     */
  
    Type* Instantiate (void);

};

// Global variables

template <typename Type> Factory<Type>* Factory<Type>::__instance = NULL_PTR_CAST(Factory*);

// Function declaration

// Function definition

template <typename Type> Factory<Type>::Factory (void) : LookUpTable<Type*(*)(void)>()
{

  // Initialise attributes
  SetDefault(STRING_UNDEFINED);
  __instance = NULL_PTR_CAST(Factory*);

  return;

}

template <typename Type> Factory<Type>::~Factory (void)
{

  if (NULL_PTR_CAST(Factory*) != __instance)
    {
      delete __instance;
      __instance = NULL_PTR_CAST(Factory*);
    }

  return;

}

template <typename Type> const ccs::types::char8* Factory<Type>::GetDefault (void) const
{ 

  const ccs::types::char8* def = __default;

  if (ccs::HelperTools::IsUndefinedString(def) && (1u == LookUpTable<Type*(*)(void)>::GetSize()))
    {
      // Single entry in the plug-in database
      def = LookUpTable<Type*(*)(void)>::GetName(0u);

      // Update default .. no for const method
      //SetDefault(def);
    }

  return def; 

}

template <typename Type> bool Factory<Type>::SetDefault (const ccs::types::char8 * const name) { (void)ccs::HelperTools::SafeStringCopy(__default, name, ccs::types::MaxStringLength); return true; }

template <typename Type> Type* Factory<Type>::Instantiate (const ccs::types::char8 * const name)
{

  Type* ref = NULL_PTR_CAST(Type*);

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != name);

  if (status)
    {
      status = LookUpTable<Type*(*)(void)>::IsValid(name);
    }

  Type*(*constructor)(void) = NULL_PTR_CAST(Type*(*)(void));

  if (status)
    {
      constructor = LookUpTable<Type*(*)(void)>::GetElement(name);
      status = (NULL_PTR_CAST(Type*(*)(void)) != constructor);
    }

  if (status)
    {
      ref = (*constructor)();
    }

  return ref;

}

template <typename Type> Type* Factory<Type>::Instantiate (void) { return Instantiate(GetDefault()); }

} // namespace base

} // namespace ccs

#endif // _TemplateFactory_h_

