/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVMonitor.h
 * @brief Header file for PVMonitor class.
 * @date 01/07/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @details This header file contains the definition of the PVMonitor class.
 */

#ifndef _PVMonitor_h_
#define _PVMonitor_h_

// Global header files

#include "BasicTypes.h" // Data type definitions ..

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

// Local header files

// Constants

// Type definition

namespace ccs {

namespace base {

class PVMonitorImpl; // Forward class declaration

/**
 * @brief Interface class providing support for Process Variable (PV) monitoring.
 * @details The base class is instantiated with the name of the PV.
 * The base class constructors and destructor take care of infrastructure
 * life-cycle management.
 *
 * Implementation classes specialise the HandleRequest method to be notified
 * of PV updates, perform application-specific processing, etc.
 *
 * The aim is to rely on dynamic linking and static initialisation, or other
 * mechanisms to associate the client class with a particular transport, i.e.
 * the ITER Synchronous Databus Network (SDN), EPICS PVAccess over the ITER Plant
 * Operation Network (PON), etc.
 *
 * @code
   class SpecialisedPVHandler : public ccs::base::PVMonitor
   {

     private:

       std::ofstream __file;

     public:

       SpecialisedPVHandler (const char* name) : ccs::base::PVMonitor(name) {
         std::string file = std::string("/tmp/") + std::string(this->GetChannel()) + std::string(".record");
         __file.open (file, { std::ios::out | std::ios::trunc });
       };

       virtual ~SpecialisedPVHandler (void) {
         __file.close(); 
       };

       virtual void HandleMonitor (const ccs::types::AnyValue& value) {

         log_info("Channel '%s' updated at '%lu'", this->GetChannel(), ccs::HelperTools::GetCurrentTime());

         char buffer [1024u] = STRING_UNDEFINED;
         bool status = value.SerialiseInstance(buffer, 1024u);

         if (status)
         {
           // Record monitor to file
           __file << buffer << std::endl;
         }

       };

   };

   @endcode
 *
 * @note The design is based on a bridge pattern to avoid exposing server-specific
 * internals through the interface class, e.g. transport technology, etc.
 */

typedef class PVMonitor
{

  private:

    /**
     * @brief Attribute. 
     * @details Bridge (PIMPL) pattern.
     */

    PVMonitorImpl* _impl;

    PVMonitor (const PVMonitor& copy); // Undefined
    PVMonitor& operator= (const PVMonitor& copy); // Undefined

  protected:

  public:

    /**
     * @brief Constructor. NOOP.
     */

    PVMonitor (void);

    /**
     * @brief Constructor.
     * @details PV monitor implementation is instantiated, callbacks registered and
     * PVMonitorImpl::Initialise() called to finalise the two-step instantiation
     * process.
     * @param channel PV name (nil-terminated character array).
     */

    PVMonitor (const ccs::types::char8 * const channel);

    /**
     * @brief Constructor.
     * @details PV monitor is instantiated and callbacks registered.
     * @param channel PV name (nil-terminated character array).
     * @param plugin Implementation plug-in (nil-terminated character array).
     */

    PVMonitor (const ccs::types::char8 * const channel, const ccs::types::char8 * const plugin);

    /**
     * @brief Destructor.
     */

    virtual ~PVMonitor (void); 

    /**
     * @brief Initialise method.
     * @details Finalise the two-step instantiation process of the PV monitor implementation.
     * @code
       class SpecialisedPVHandler : public ccs::base::PVMonitor
       {

         private:

         public:

           SpecialisedPVHandler (const char* name) : ccs::base::PVMonitor() {

             // Set channel name
             (void)ccs::base::PVMonitor::SetChannel(name);

             // Set parameters ..
             (void)ccs::base::PVMonitor::SetParameter("interface", "lo");
             (void)ccs::base::PVMonitor::SetParameter("affinity", 0u);

             // Finalise the instantiation process
             (void)ccs::base::PVMonitor::Initialise();

           };

           virtual ~SpecialisedPVHandler (void) {}; 

           virtual void HandleMonitor (const ccs::types::AnyValue& value) {
             // Etc.
           };

       };
       @endcode
     */

    bool Initialise (void);

    /**
     * @brief Accessor.
     * @return True if the channel is currently connected.
     */

    bool IsConnected (void) const;

    /**
     * @brief Configure method.
     * @details The implementation class may provide additional settings offered to the application.
     * @return True if successful.
     * @todo Re-assess if useful now that GetImplementation() method exists to support plug-in
     * specific access which is currently const but could also be used to set parameters, etc.
     * @todo Evaluate template implementation rather than rely on ccs::base::AnyValue.
     */

    bool SetParameter (const ccs::types::char8 * const name, const ccs::types::char8 * const value);
    bool SetParameter (const ccs::types::char8 * const name, const ::ccs::types::AnyValue& value);

    /**
     * @brief Accessor.
     * @param channel Process variable name.
     */

    bool SetChannel (const ccs::types::char8 * const channel);

    /**
     * @brief Accessor.
     * @return Associated PV name (nil-terminated character array).
     */

    const ccs::types::char8* GetChannel (void) const;

    /**
     * @brief Accessor.
     * @return Imlementation.
     * @todo Re-assess if necessary.
     */

    const PVMonitorImpl* GetImplementation (void) const;

    /**
     * @brief Self-explanatory.
     */

    enum Event {
      Undefined,
      Timeout,
      Connect,
      Disconnect,
    };

    /**
     * @brief Virtual event handler method.
     * @param event Event to be handled.
     */

    virtual void HandleEvent (const Event& event); // Virtual method

    /**
     * @brief Virtual data handler method.
     * @param value Buffer associated to an introspectable type definition.
     */

    virtual void HandleMonitor (const ccs::types::AnyValue& value) = 0; // Pure virtual method

} ProcessVariableMonitor;

// Global variables

// Function declaration

// Function definition

} // namespace base

} // namespace ccs

#endif // _PVMonitor_h_

