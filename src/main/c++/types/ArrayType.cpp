/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

//#include <memory> // std::shared_ptr, etc.

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "JSONTools.h" // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE 
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE 
#include "log-api.h" // Syslog wrapper routines

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyType.h"

#include "ArrayType.h"
#include "CompoundType.h"
#include "ScalarType.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ccs::types"

// Type definition

namespace ccs {

namespace types {

// Global variables

// Function declaration

// Function definition
  
bool ArrayType::HasElement (const uint32 index) const
{ 

  bool status = (index < this->GetMultiplicity());

  log_debug("ArrayType::HasElement('%u') - Size is '%u'", index, this->GetMultiplicity());

  return status; 

}

ccs::base::SharedReference<const AnyType> ArrayType::GetElementType (void) const 
{ 

  return __base; 

}

ArrayType* ArrayType::SetElementType (const ccs::base::SharedReference<const AnyType>& type) 
{ 

  __base = type; 

  bool status = static_cast<bool>(__base);

  if (status)
    {
      (void)this->SetSize(this->GetMultiplicity() * __base->GetSize()); 
    }

  return this; 

}

ArrayType* ArrayType::SetElementType (const AnyType* type) 
{ 

  __base = ccs::base::SharedReference<const AnyType>(type);

  bool status = static_cast<bool>(__base);

  if (status)
    {
      (void)this->SetSize(this->GetMultiplicity() * __base->GetSize()); 
    }

  return this; 

}

uint32 ArrayType::GetElementNumber (void) const { return GetMultiplicity(); }

uint32 ArrayType::GetElementOffset (const uint32 index) const 
{ 
#ifndef INHERIT_FROM_COMPOUND_TYPE
  uint32 offset = 0u;

  if (this->HasElement(index))
    {
      offset = index * __base->GetSize();
    }

  return offset; 
#else
  return CompoundType::GetAttributeOffset(index);
#endif
}

void* ArrayType::GetElementReference (const void * const ref, const uint32 index) const
{
#ifndef INHERIT_FROM_COMPOUND_TYPE
  void* elem = NULL_PTR_CAST(void*);

  if ((NULL_PTR_CAST(void*) != ref) && (index < this->GetElementNumber())) 
    {
      elem = const_cast<void*>(static_cast<const void*>(static_cast<const uint8_t*>(ref) + this->GetElementOffset(index)));
    }

  return elem;
#else
  return CompoundType::GetAttributeReference(ref, index);
#endif
}
 
uint32 ArrayType::GetMultiplicity (void) const 
{ 
#ifndef INHERIT_FROM_COMPOUND_TYPE
  return __multiplicity; 
#else
  return CompoundType::GetAttributeNumber();
#endif
}

ArrayType* ArrayType::SetMultiplicity (uint32 multiplicity) 
{ 
#ifndef INHERIT_FROM_COMPOUND_TYPE
  __multiplicity = multiplicity;
#endif
  bool status = static_cast<bool>(__base);

  if (status)
    {
#ifndef INHERIT_FROM_COMPOUND_TYPE
      (void)this->SetSize(__multiplicity * __base->GetSize()); 
#else
      for (uint32 index = 0u; (status && (index < multiplicity)); index++)
        {
          string name = "";
          ccs::HelperTools::ToString(index, name, MaxStringLength);
          status = CompoundType::AddAttribute(name, __base);
        }
#endif
    }

  return this; 

}

bool ArrayType::ToNetworkByteOrder (void * const ref) const
{

  log_trace("ArrayType::ToNetworkByteOrder - Entering method");

  bool status = (NULL_PTR_CAST(void*) != ref);

  for (uint32 index = 0u; ((index < this->GetElementNumber()) && status); index++)
    {
      void* attr = static_cast<void*>(static_cast<uint8_t*>(ref) + this->GetElementOffset(index));
      status = __base->ToNetworkByteOrder(attr);
    }

  log_trace("ArrayType::ToNetworkByteOrder - Leaving method");

  return status;

}

bool ArrayType::FromNetworkByteOrder (void * const ref) const
{

  log_trace("ArrayType::FromNetworkByteOrder - Entering method");

  bool status = (NULL_PTR_CAST(void*) != ref);

  for (uint32 index = 0u; ((index < this->GetElementNumber()) && status); index++)
    {
      void* attr = static_cast<void*>(static_cast<uint8_t*>(ref) + this->GetElementOffset(index));
      status = __base->FromNetworkByteOrder(attr);
    }

  log_trace("ArrayType::FromNetworkByteOrder - Leaving method");

  return status;

}

static uint32 ParseInstance_Char8Array (const ArrayType * const self, void * const ref, const char8 * const buffer)
{

  uint32 ret = 0u;

  int32 start = ccs::HelperTools::Find(buffer, '"');
  int32 close = ccs::HelperTools::Find(buffer, '"', start+1u);
  
  bool status = (-1 != start);

  if (status)
    {
      ret = static_cast<uint32>(close + 1);
      (void)ccs::HelperTools::SafeStringCopy(static_cast<ccs::types::char8*>(ref), buffer+start+1u, self->GetSize(), '"');
    }
  
  return ret;

}

static uint32 ParseInstance_OtherArray (const ArrayType * const self, void * const ref, const char8 * const buffer)
{

  uint32 ret = 0u;

  int32 start = ccs::HelperTools::Find(buffer, '[');
  int32 close = ccs::HelperTools::FindMatchingBrace(buffer, start);

  bool status = ((-1 != start) && (-1 != close));

  if (status)
    {
      ret = static_cast<uint32>(close + 1);
    }
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("ArrayType::ParseInstance('%s') - Try and parse '%s' .. will return '%u' if successful", self->GetName(), buffer, ret);
    }
#endif
  uint32 elem = 0u;
  uint32 next = 0u;

  for (uint32 index = 0u; ((index < self->GetElementNumber()) && status); index++)
    {
      status = ccs::HelperTools::FindJSONArrayElement(buffer, elem, next, next);

      if (status)
        {
          const char8* p_buf = buffer + elem; // p_buf points to 'attr_value,' 
          void* attr = static_cast<void*>(static_cast<uint8*>(ref) + self->GetElementOffset(index));
          log_debug("ArrayType::ParseInstance('%s') - Try and parse '%s'", type->GetName(), p_buf);
          status = (0u < (self->GetElementType())->ParseInstance(attr, p_buf));
        }

      if (!status)
        {
          log_error("ArrayType::ParseInstance('%s') - Failed to find element '%u' in '%s'", self->GetName(), index, buffer);
        }

      next++;
    }

  if (!status)
    {
      ret = 0u;
    }

  log_debug("ArrayType::ParseInstance('%s') - Returning '%u'", self->GetName(), ret);

  return ret;

}

uint32 ArrayType::ParseInstance (void * const ref, const char8 * const buffer) const
{

  log_trace("ArrayType::ParseInstance - Entering method");

  bool status = ((NULL_PTR_CAST(void*) != ref) && 
                 (NULL_PTR_CAST(const char8*) != buffer));

  uint32 ret = 0u;

  if (status && (ccs::types::Character8 == this->GetElementType()))
    {
      // Treat as string and return
      ret = ParseInstance_Char8Array(this, ref, buffer);
    }
  else if (status)
    {
      ret = ParseInstance_OtherArray(this, ref, buffer);
    }

  log_trace("ArrayType::ParseInstance - Leaving method");

  return ret;

}

static bool SerialiseInstance_Char8Array (const ArrayType * const self, const void * const ref, char8 * const buffer, const uint32 size)
{

  bool status = (size > self->GetSize());

  if (status)
    {
      ccs::HelperTools::SafeStringCopy(buffer, "\"", size);
      ccs::HelperTools::SafeStringAppend(buffer, static_cast<const ccs::types::char8*>(ref), size);
      ccs::HelperTools::SafeStringAppend(buffer, "\"", size);
    }

  return status;

}

static bool SerialiseInstance_OtherArray (const ArrayType * const self, const void * const ref, char8 * const buffer, const uint32 size)
{

  char8* p_buf = buffer;
  uint32 _size = size;

  (void)ccs::HelperTools::SafeStringCopy(p_buf, "[", _size); 

  _size -= ccs::HelperTools::StringLength(p_buf); 
  p_buf += ccs::HelperTools::StringLength(p_buf); // Re-align pointer

  bool status = (0u < _size);

  if (status && (0u == self->GetElementNumber()))
    { // Bug 13516 - Valid serialisation of zero-sized arrays
      (void)ccs::HelperTools::SafeStringCopy(p_buf, "null,", _size);

      _size -= ccs::HelperTools::StringLength(p_buf);
      p_buf += ccs::HelperTools::StringLength(p_buf); // Re-align pointer
    }

  for (uint32 index = 0u; (status && (index < self->GetElementNumber())); index++)
    {
      const void* attr = static_cast<const void*>(static_cast<const uint8*>(ref) + self->GetElementOffset(index));

      status = (self->GetElementType())->SerialiseInstance(attr, p_buf, _size);

      _size -= ccs::HelperTools::StringLength(p_buf); 
      p_buf += ccs::HelperTools::StringLength(p_buf); // Re-align pointer 

      (void)ccs::HelperTools::SafeStringCopy(p_buf, ",", _size);

      _size -= ccs::HelperTools::StringLength(p_buf); 
      p_buf += ccs::HelperTools::StringLength(p_buf); // Re-align pointer 
    }

  // Terminate buffer - remove final comma character 
  if (static_cast<uint32>(ccs::HelperTools::StringLength(buffer)) >= 1u) 
    {
      *(buffer + ccs::HelperTools::StringLength(buffer) - 1u) = 0; 
      p_buf = buffer + ccs::HelperTools::StringLength(buffer); // Re-align pointer 
    }

  (void)ccs::HelperTools::SafeStringCopy(p_buf, "]", _size); 

  return status;

}

bool ArrayType::SerialiseInstance (const void * const ref, char8 * const buffer, const uint32 size) const
{

  log_trace("ArrayType::SerialiseInstance - Entering method");

  bool status = ((NULL_PTR_CAST(const void*) != ref) && 
                 (NULL_PTR_CAST(char8*) != buffer));

  if (status && (ccs::types::Character8 == this->GetElementType()))
    {
      // Treat as string and return
      status = SerialiseInstance_Char8Array(this, ref, buffer, size);
    }
  else if (status)
    {
      status = SerialiseInstance_OtherArray(this, ref, buffer, size);
    }

  log_trace("ArrayType::SerialiseInstance - Leaving method");

  return status;

}

bool ArrayType::operator== (const AnyType& type) const
{

  bool status = (NULL_PTR_CAST(const ArrayType*) != dynamic_cast<const ArrayType*>(&type));

  if (status)
    {
      const ArrayType* ref = dynamic_cast<const ArrayType*>(&type);

      status = ((this->GetSize() == ref->GetSize()) &&
                (this->GetMultiplicity() == ref->GetMultiplicity()) &&
                (*((this->GetElementType()).GetReference()) == *((ref->GetElementType()).GetReference())));
    }

  return status;

}

bool ArrayType::operator== (const ArrayType& type) const
{

  bool status = ((this->GetSize() == type.GetSize()) &&
                 (this->GetMultiplicity() == type.GetMultiplicity()));

  if (status)
    {
      status = (*((this->GetElementType()).GetReference()) == *((type.GetElementType()).GetReference()));
    }

  return status;

}

ArrayType& ArrayType::operator= (const ArrayType& type)
{

  log_trace("ArrayType::operator= - Entering method");

  if (this != &type)
    {
      (void)this->SetName(type.GetName());
      (void)this->SetElementType(type.GetElementType());
      (void)this->SetMultiplicity(type.GetMultiplicity());
    }

  log_trace("ArrayType::operator= - Leaving method");

  return *this;

}

ArrayType::ArrayType (void) 
{

  // Initialise attributes
  (void)this->SetMultiplicity(0u);
  (void)this->SetSize(0u);

  return; 

}
#ifndef INHERIT_FROM_COMPOUND_TYPE
ArrayType::ArrayType (const ArrayType& type) : AnyType (type.GetName()) // Copy constructor
#else
ArrayType::ArrayType (const ArrayType& type) : CompoundType (type.GetName()) // Copy constructor
#endif
{

  // Initialise attributes
  (void)this->SetMultiplicity(0u);
  (void)this->SetSize(0u);

  (void)this->SetElementType(type.GetElementType());
  (void)this->SetMultiplicity(type.GetMultiplicity());

  return; 

}
#ifndef INHERIT_FROM_COMPOUND_TYPE
ArrayType::ArrayType (const char8 * const type, const ccs::base::SharedReference<const AnyType>& base, const uint32 multiplicity) : AnyType (type) 
#else
ArrayType::ArrayType (const char8 * const type, const ccs::base::SharedReference<const AnyType>& base, const uint32 multiplicity) : CompoundType (type) 
#endif
{ 

  // Initialise attributes
  (void)this->SetMultiplicity(0u);
  (void)this->SetSize(0u);

  (void)this->SetElementType(base);
  (void)this->SetMultiplicity(multiplicity);

  return; 

}
#ifndef INHERIT_FROM_COMPOUND_TYPE
ArrayType::ArrayType (const char8 * const type, const AnyType * const base, const uint32 multiplicity) : AnyType (type) 
#else
ArrayType::ArrayType (const char8 * const type, const AnyType * const base, const uint32 multiplicity) : CompoundType (type) 
#endif
{ 

  // Initialise attributes
  (void)this->SetMultiplicity(0u);
  (void)this->SetSize(0u);

  (void)this->SetElementType(base);
  (void)this->SetMultiplicity(multiplicity);

  return; 

}
#ifndef INHERIT_FROM_COMPOUND_TYPE
// v1.3.3 - Smart pointers impose to make a copy so as to avoid deleting the type when this goes out of scope
ArrayType::ArrayType (const char8 * const type, const ArrayType& base, const uint32 multiplicity) : AnyType (type) 
#else
ArrayType::ArrayType (const char8 * const type, const ArrayType& base, const uint32 multiplicity) : CompoundType (type) 
#endif
{ 

  // Relying on copy constructor
  ccs::base::SharedReference<const AnyType> __type = ccs::base::SharedReference<const AnyType>(ccs::base::SharedReference<const ArrayType>(new (std::nothrow) ArrayType (base)));

  // Initialise attributes
  (void)this->SetMultiplicity(0u);
  (void)this->SetSize(0u);

  (void)this->SetElementType(__type);
  (void)this->SetMultiplicity(multiplicity);

  return; 

}
#ifndef INHERIT_FROM_COMPOUND_TYPE
ArrayType::ArrayType (const char8 * const type, const CompoundType& base, const uint32 multiplicity) : AnyType (type) 
#else
ArrayType::ArrayType (const char8 * const type, const CompoundType& base, const uint32 multiplicity) : CompoundType (type) 
#endif
{ 

  // Relying on copy constructor
  ccs::base::SharedReference<const AnyType> __type = ccs::base::SharedReference<const AnyType>(ccs::base::SharedReference<const CompoundType>(new (std::nothrow) CompoundType (base)));

  // Initialise attributes
  (void)this->SetMultiplicity(0u);
  (void)this->SetSize(0u);

  (void)this->SetElementType(__type);
  (void)this->SetMultiplicity(multiplicity);

  return; 

}
#ifndef INHERIT_FROM_COMPOUND_TYPE
ArrayType::ArrayType (const char8 * const type, const ScalarType& base, const uint32 multiplicity) : AnyType (type) 
#else
ArrayType::ArrayType (const char8 * const type, const ScalarType& base, const uint32 multiplicity) : CompoundType (type) 
#endif
{ 

  // Relying on copy constructor
  ccs::base::SharedReference<const AnyType> __type = ccs::base::SharedReference<const AnyType>(ccs::base::SharedReference<const ScalarType>(new (std::nothrow) ScalarType (base)));

  // Initialise attributes
  (void)this->SetMultiplicity(0u);
  (void)this->SetSize(0u);

  (void)this->SetElementType(__type);
  (void)this->SetMultiplicity(multiplicity);

  return; 

}

ArrayType::~ArrayType (void) {}

} // namespace types

} // namespace ccs
