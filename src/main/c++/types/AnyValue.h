/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Generic type class definition
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file AnyValue.h
 * @brief Header file for AnyValue class.
 * @date 01/11/2017
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @details This header file contains the definition of the AnyValue class.
 */

#ifndef _AnyValue_h_
#define _AnyValue_h_

// Global header files

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "log-api.h" // Syslog wrapper routines

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyType.h"
#include "ArrayType.h"
#include "CompoundType.h"
#include "ScalarType.h"

#include "AnyTypeHelper.h"

// Constants

// Type definition

namespace ccs {

namespace types {

/**
 * @brief AnyValue associated to introspectable type definition.
 * @details The class associates a memory buffer to an introspectable
 * type definition. The memory area is allocated upon instantiation.
 * The class also provides assignment, comparison, and static cast
 * operator overload to basic types.
 *
 * @note The class is associated with static helper routines to
 * simplify access to attributes, etc. without replicating the
 * AnyType interface. See AnyValueHelper.h.
 */

class AnyValue
{

  private:

    uint8* _instance;
    bool _allocated;
    // Bug 12497 - Get rid of -std=c++11 requirement
    ccs::base::SharedReference<const AnyType> _type;
    ccs::types::Endianness _order;

    void* CreateInstance (void);
    void DeleteInstance (void);

    // Templated initialization to avoid code duplication
    template <typename T> void Initialise(const T & type);
    template <typename T, typename S> void Initialise(const T & type, const S & value);

  protected:

  public:

    /**
     * @brief Constructor. NOOP.
     */

    AnyValue (void);

    /**
     * @brief Constructors for built-in types.
     *
     * @code
       // Create AnyValue with integer literal
       ccs::types::AnyValue value (0ul);

       // Create AnyValue with string literal
       ccs::types::AnyValue value ("This is a string literal");
       @endcode
     */

    AnyValue (const boolean & value);
    AnyValue (const char8 & value);
    AnyValue (const int8 & value);
    AnyValue (const uint8 & value);
    AnyValue (const int16 & value);
    AnyValue (const uint16 & value);
    AnyValue (const int32 & value);
    AnyValue (const uint32 & value);
    AnyValue (const int64 & value);
    AnyValue (const uint64 & value);
    AnyValue (const float32 & value);
    AnyValue (const float64 & value);
    AnyValue (const string& value);
    template <uint32 N> AnyValue (const char8 (& value) [N]);

    /**
     * @brief Constructor.
     * @details Instantiates memory for type instance.
     * @param type Introspectable type definition as JSON stream.
     */

    explicit AnyValue (const char8 * const type);

    /**
     * @brief Constructors with type definitions.
     * @details Instantiates memory for type instance.
     * @param type Introspectable type definition.
     */
    // Bug 12497 - Get rid of -std=c++11 requirement
    AnyValue (const ccs::base::SharedReference<const AnyType>& type);
    AnyValue (const ccs::base::SharedReference<const ScalarType>& type);
    AnyValue (const ccs::base::SharedReference<const CompoundType>& type);
    AnyValue (const ccs::base::SharedReference<const ArrayType>& type);

    /**
     * @brief Constructor.
     * @details Instantiates memory for type instance.
     * @param type Introspectable type definition.
     */

    explicit AnyValue (AnyType * const type);
    explicit AnyValue (const AnyType * const type);

    /**
     * @brief Constructor.
     * @details Instantiates memory for type instance.
     * @param type Introspectable type definition.
     */

    AnyValue (const ArrayType& type);
    AnyValue (const CompoundType& type);
    AnyValue (const ScalarType& type);

    /**
     * @brief Constructor.
     * @details Memory is not allocated and assumed to be
     * managed by the calling application, e.g. a C-like
     * struct mapped to an introspectable type definition.
     * @param type Introspectable type definition.
     * @param instance Externally managed type instance.
     */
    // Bug 12497 - Get rid of -std=c++11 requirement
    AnyValue (const ccs::base::SharedReference<const AnyType>& type, void * const instance);

    /**
     * @brief Constructor.
     * @details Memory is not allocated and assumed to be
     * managed by the calling application, e.g. a C-like
     * struct mapped to an introspectable type definition.
     * @param type Introspectable type definition.
     * @param instance Externally managed type instance.
     */

    AnyValue (const AnyType * const type, void * const instance);

    /**
     * @brief Constructor.
     * @details Memory is not allocated and assumed to be
     * managed by the calling application, e.g. a C-like
     * struct mapped to an introspectable type definition.
     * @param type Introspectable type definition.
     * @param instance Externally managed type instance.
     */

    AnyValue (const ArrayType& type, void * const instance);
    AnyValue (const CompoundType& type, void * const instance);
    AnyValue (const ScalarType& type, void * const instance);

    /**
     * @brief Copy constructor.
     */

    AnyValue (const AnyValue& value); // Copy constructor

    /**
     * @brief Destructor.
     * @details Frees allocated memory for type instance.
     */

    virtual ~AnyValue (void);

    /**
     * @brief Accessor.
     * @return Byte size of type definition.
     */

    uint32 GetSize (void) const;

    /**
     * @brief Accessor.
     * @return Introspectable type definition.
     */
    // Bug 12497 - Get rid of -std=c++11 requirement
    ccs::base::SharedReference<const AnyType> GetType (void) const;

    /**
     * @brief Accessor.
     * @return Type instance memory location.
     */

    void* GetInstance (void) const;

    /**
     * @brief Byte ordering conversion.
     * @details In-place conversion from host platform endianness to network byte order (big-endianness).
     * @return True if successful.
     */

    bool ToNetworkByteOrder (void);

    /**
     * @brief Byte ordering conversion.
     * @details In-place conversion from network byte order (big-endianness).
     * @return True if successful.
     */

    bool FromNetworkByteOrder (void);

    /**
     * @brief Serialisation method.
     * @details Parses serialised buffer and provides binary equivalent.
     * @param buffer Serialised instance value.
     * @return True if successful.
     */

    bool ParseInstance (const char8 * const buffer);

    /**
     * @brief Serialisation method.
     * @details Serialises type instance to string buffer.
     * @param buffer Placeholder to receive serialised instance value.
     * @param size Size of string buffer.
     * @return True if successful.
     */

    bool SerialiseInstance (char8 * const buffer, uint32 size) const;

    /**
     * @brief Serialisation method.
     * @details Serialises type definition to string buffer.
     * @param buffer Placeholder to receive serialised type.
     * @param size Size of string buffer.
     * @return True if successful.
     */

    bool SerialiseType (char8 * const buffer, uint32 size) const;

    /**
     * @brief Comparison operator.
     * @details Compares instance with externally provided value. The operator allows to
     * compare to basic types, C-like struct, etc.
     * @return True if successful.
     *
     * @code
       ccs::types::AnyValue value (ccs::types::UnsignedInteger32);

       if (value == 0u)
         // Do something
       else
         // Do something else
       @endcode
     *
     * @todo Extend to other types of operators, e.g. increment.
     */

    template <typename Type> bool operator== (const Type& value) const;

    /**
     * @brief Assignment operator.
     * @details Set instance to externally provided value. The operator allows to
     * assign to basic types, from C-like struct, etc.
     *
     * @code
       // C-like structure definition
       typedef struct MyType {
         ccs::types::uint64 timestamp;
         ccs::types::float64 value;
       } MyType_t;

       // Equivalent introspectable type definition
       ccs::types::CompoundType type ("MyType_t");
       type.AddAttribute("timestamp", ccs::types::UnsignedInteger64);
       type.AddAttribute("value", ccs::types::Float64);

       MyType_t data;
       data.timestamp = ccs::HelperTools::GetCurrentTime();
       data.value = 0.0;

       // Assignment
       ccs::types::AnyValue value (type) = data;
       @endcode
     */

    template <typename Type> AnyValue& operator= (const Type& value); // Assign from externally defined variable

    /**
     * @brief Copy assignment operator.
     */

    AnyValue& operator= (const AnyValue& value); // Copy assignment operator

    /**
     * @brief Static cast operator.
     * @details Use instance to set externally provided value. The operator allows to
     * cast to to basic types, C-like struct, etc.
     *
     * @code
       // C-like structure definition
       typedef struct MyType {
         ccs::types::uint64 timestamp;
         ccs::types::float64 value;
       } MyType_t;

       // Equivalent introspectable type definition
       ccs::types::CompoundType type ("MyType_t");
       type.AddAttribute("timestamp", ccs::types::UnsignedInteger64);
       type.AddAttribute("value", ccs::types::Float64);

       ccs::types::AnyValue value (type);

       // Cast introspectable instance to C-like structure
       MyType_t data = static_cast<MyType_t>(value);
       @endcode
     */

    template <typename Type> operator Type() const; // Cast to externally defined variable

};

// Global variables

// Function declaration

// Function definition

template <typename T> void AnyValue::Initialise(const T & type)
{
  _type = type;
  (void)this->CreateInstance();
  return;
}

template <typename T, typename S> void AnyValue::Initialise(const T & type, const S & value)
{
  Initialise<T>(type);
  if (NULL != this->GetInstance())
  {
    *static_cast<S*>(this->GetInstance()) = value;
  }
}

template <uint32 N> AnyValue::AnyValue (const char8 (& value) [N]) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _order(ccs::HelperTools::GetNativeByteOrder())
{

  bool _copy = true;

  if (ccs::HelperTools::IsJSONObject(value))
    { // Datatype definition as JSON stream
      ccs::base::SharedReference<AnyType> tmp;
      bool status = (0u < ccs::HelperTools::Parse(tmp, value));

      if (status)
        {
          _type = tmp;
        }

      _copy = false;
    }
  else if (ccs::types::MaxStringLength > ccs::HelperTools::StringLength(value)) // Alternatively use 'N'
    {
      _type = String;
    }
  else
    { // Character array
      _type = ccs::base::SharedReference<const AnyType>(ccs::HelperTools::NewArrayType("char8[]", ccs::types::Character8, ccs::HelperTools::StringLength(value)+1u));
    }

  // Create value buffer
  (void)this->CreateInstance();

  if (_copy)
    {
      // Copy value
      (void)memcpy(this->GetInstance(), &value, this->GetSize());
    }

  return;

}

template <typename Type> bool AnyValue::operator== (const Type& value) const
{

  bool status = (this->GetSize() == sizeof(Type));

  if (status)
    {
      for (uint32 index = 0u; (status && (index < this->GetSize())); index += 1u)
        {
          // Byte per byte comparison
          status = (*(static_cast<const uint8*>(this->GetInstance()) + index) ==
                    *(reinterpret_cast<const uint8*>(&value) + index));
        }
    }

  return status;

}

template <typename Type> AnyValue& AnyValue::operator= (const Type& value)
{

  bool status = (this->GetSize() == sizeof(Type));

  if (status)
    {
      (void)memcpy(this->GetInstance(), &value, this->GetSize());
    }
  else
    {
      log_warning("template <typename Type> AnyValue& AnyValue::operator= - Size error '%u' vs '%u'", this->GetSize(), sizeof(Type));
    }

  return *this;

}

#define FUNCTION_DEFINITION(BASIC_TYPE, INTRO_TYPE) \
template <> inline AnyValue& AnyValue::operator=<BASIC_TYPE> (const BASIC_TYPE& value) \
{ \
\
  if (false == static_cast<bool>(this->GetType())) \
    { \
      _type = INTRO_TYPE; \
      (void)this->CreateInstance(); \
    } \
\
  bool status = (this->GetSize() == sizeof(BASIC_TYPE)); \
\
  if (status) \
    { \
      (void)memcpy(this->GetInstance(), &value, this->GetSize()); \
    } \
  else \
    { \
      log_warning("template <" #BASIC_TYPE "> AnyValue& AnyValue::operator= - Size error '%u' vs '%u'", this->GetSize(), sizeof(BASIC_TYPE)); \
    } \
\
  return *this; \
\
}

FUNCTION_DEFINITION(boolean, Boolean)
FUNCTION_DEFINITION(char8, Character8)
FUNCTION_DEFINITION(int8, SignedInteger8)
FUNCTION_DEFINITION(uint8, UnsignedInteger8)
FUNCTION_DEFINITION(int16, SignedInteger16)
FUNCTION_DEFINITION(uint16, UnsignedInteger16)
FUNCTION_DEFINITION(int32, SignedInteger32)
FUNCTION_DEFINITION(uint32, UnsignedInteger32)
FUNCTION_DEFINITION(int64, SignedInteger64)
FUNCTION_DEFINITION(uint64, UnsignedInteger64)
FUNCTION_DEFINITION(float32, Float32)
FUNCTION_DEFINITION(float64, Float64)
FUNCTION_DEFINITION(string, String)

#undef FUNCTION_DEFINITION

template <typename Type> AnyValue::operator Type() const
{

  Type value;

  (void)memset(&value, 0, sizeof(Type));

  bool status = (this->GetSize() == sizeof(Type));

  if (status)
    {
      (void)memcpy(&value, this->GetInstance(), sizeof(Type));
    }
  else
    {
      log_warning("template <typename Type> AnyValue::operator Type() - Size error '%u' vs '%u'", this->GetSize(), sizeof(Type));
    }

  return value;

}

} // namespace types

} // namespace ccs

#endif // _AnyValue_h_

