/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

// Local header files

#include "BasicTypes.h" // Global type definition
#include "StringTools.h" // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE 
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE 
#include "log-api.h" // Syslog wrapper routines

#include "AnyType.h" // Introspectable data type ..
#include "AnyTypeHelper.h" // .. associated helper routines
#include "AnyTypeDatabase.h"

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

#include "RPCTypes.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "rpc-if"

// Type definition

namespace ccs {

namespace types {

// Function declaration

// Global variables

ccs::base::SharedReference<const ccs::types::CompoundType> Reply_int; // Introspectable type definition
ccs::base::SharedReference<const ccs::types::CompoundType> Request_int; // Introspectable type definition

} // namespace types

// Function definition

namespace HelperTools {

bool InitialiseRPCTypes (void)
{

  using namespace ccs::types;

  bool status = ((Reply_int ? true : false) &&
                 (Request_int ? true : false));

  if (!status)
    {
      // Introspectable type definition
      Reply_int = ccs::base::SharedReference<const CompoundType> ((new (std::nothrow) CompoundType (RPCReply_TypeName))
                                                       ->AddAttribute<uint64>("timestamp")
                                                       ->AddAttribute<string>("qualifier")
                                                       ->AddAttribute<boolean>("status")
                                                       ->AddAttribute<string>("reason"));
      
      Request_int = ccs::base::SharedReference<const CompoundType> ((new (std::nothrow) CompoundType (RPCRequest_TypeName))
                                                         ->AddAttribute<uint64>("timestamp")
                                                         ->AddAttribute<string>("qualifier"));
#if 0 // Do not register types .. this prevents JSON serialisation of extended types
      status = (((Reply_int ? true : false) && GlobalTypeDatabase::Register(Reply_int)) &&
                ((Request_int ? true : false) && GlobalTypeDatabase::Register(Request_int)));
#else
      status = ((Reply_int ? true : false) && (Request_int ? true : false));
#endif
    }
#if 0 // Do not register types .. this prevents JSON serialisation of extended types
  if (status)
    {
      status = (GlobalTypeDatabase::IsValid(RPCReply_TypeName) &&
                GlobalTypeDatabase::IsValid(RPCRequest_TypeName));
    }
#endif
  if (status)
    {
      status = ((sizeof(Reply_t) == Reply_int->GetSize()) &&
                (sizeof(Request_t) == Request_int->GetSize()));
    }

  return status;

}

} // namespace HelperTools

} // namespace ccs

#undef LOG_ALTERN_SRC
