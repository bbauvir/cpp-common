/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Generic type class definition
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file ScalarType.h
 * @brief Header file for introspectable ScalarType class.
 * @date 01/11/2017
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @details This header file contains the definition of the ScalarType class.
 */

#ifndef _ScalarType_h_
#define _ScalarType_h_

// Global header files

//#include <memory> // std::shared_ptr, etc.

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "StringTools.h" // Misc. helper functions

#include "NetTools.h" 

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyType.h" // Base class definition

// Constants

// Type definition

namespace ccs {

namespace types {

/**
 * @brief Introspectable type definition for scalar types.
 */

class ScalarType : public AnyType
{

  private:

    /**
     * @brief Copy assignment operator.
     * @note Undefined since the class is used to wrap built-in types and should not be copied.
     */

    ScalarType& operator= (const ScalarType& type); // Undefined

  protected:

  public:

    /**
     * @brief Constructor.
     * @post
     *   __size = 0u && __type = "undefined"
     */

    ScalarType (void);

    /**
     * @brief Copy constructor.
     */

    ScalarType (const ScalarType& type);

    /**
     * @brief Constructor.
     * @param type Name of type definition.
     * @param size Byte size of type definition.
     * @post
     *   __size = size && __type = type
     */

    ScalarType (const char8 * const type, const uint32 size);

    /**
     * @brief Destructor. NOOP.
     */

    virtual ~ScalarType (void) {}; 

    /**
     * @brief Virtual method. See AnyType::ToNetworkByteOrder.
     */

    virtual bool ToNetworkByteOrder (void * const ref) const; // In-place conversion

    /**
     * @brief Virtual method. See AnyType::FromNetworkByteOrder.
     */

    virtual bool FromNetworkByteOrder (void * const ref) const; // In-place conversion

    /**
     * @brief Virtual method. See AnyType::ParseInstance.
     */

    virtual uint32 ParseInstance (void * const ref, const char8 * const buffer) const;

    /**
     * @brief Virtual method. See AnyType::SerialiseInstance.
     */

    virtual bool SerialiseInstance (const void * const ref, char8 * const buffer, const uint32 size) const;

    /**
     * @brief Comparison operator.
     * @details Compares against other type definition for equivalence.
     * @return True if successful.
     */

    virtual bool operator== (const AnyType& type) const; // Specialises virtual method
    bool operator== (const ScalarType& type) const;

};

/**
 * @brief Introspectable type definition for scalar types.
 */

template <typename Type> class ScalarTypeT : public ScalarType
{

  private:

    /**
     * @brief Copy constructor.
     * @note Undefined since the class is used to wrap built-in types and should not be copied.
     */

    ScalarTypeT (const ScalarTypeT& type); // Undefined

    /**
     * @brief Copy assignment operator.
     * @note Undefined since the class is used to wrap built-in types and should not be copied.
     */

    ScalarTypeT& operator= (const ScalarTypeT& type); // Undefined

  protected:

  public:

    /**
     * @brief Constructor.
     */

    ScalarTypeT (void);

    /**
     * @brief Destructor. NOOP.
     */

    virtual ~ScalarTypeT (void) {}; 

    /**
     * @brief Virtual method. See AnyType::ToNetworkByteOrder.
     */

    virtual bool ToNetworkByteOrder (void * const ref) const;

    /**
     * @brief Virtual method. See AnyType::FromNetworkByteOrder.
     */

    virtual bool FromNetworkByteOrder (void * const ref) const;

    /**
     * @brief Virtual method. See AnyType::ParseInstance.
     */

    virtual uint32 ParseInstance (void * const ref, const char8 * const buffer) const;

    /**
     * @brief Virtual method. See AnyType::SerialiseInstance.
     */

    virtual bool SerialiseInstance (const void * const ref, char8 * const buffer, const uint32 size) const;

};

// Global variables

extern const ccs::base::SharedReference<const ScalarType> Boolean;
extern const ccs::base::SharedReference<const ScalarType> Character8;
extern const ccs::base::SharedReference<const ScalarType> SignedInteger8;
extern const ccs::base::SharedReference<const ScalarType> UnsignedInteger8;
extern const ccs::base::SharedReference<const ScalarType> SignedInteger16;
extern const ccs::base::SharedReference<const ScalarType> UnsignedInteger16;
extern const ccs::base::SharedReference<const ScalarType> SignedInteger32;
extern const ccs::base::SharedReference<const ScalarType> UnsignedInteger32;
extern const ccs::base::SharedReference<const ScalarType> SignedInteger64;
extern const ccs::base::SharedReference<const ScalarType> UnsignedInteger64;
extern const ccs::base::SharedReference<const ScalarType> Float32;
extern const ccs::base::SharedReference<const ScalarType> Float64;
extern const ccs::base::SharedReference<const ScalarType> String;

// ToDo - Time with specialised parse/serialise methods

// Function declaration

// Function definition

template <> inline ScalarTypeT<boolean>::ScalarTypeT (void) : ScalarType("bool", sizeof(boolean)) {}
template <> inline ScalarTypeT<char8>::ScalarTypeT (void) : ScalarType("char8", sizeof(char8)) {}
template <> inline ScalarTypeT<int8>::ScalarTypeT (void) : ScalarType("int8", sizeof(int8)) {}
template <> inline ScalarTypeT<uint8>::ScalarTypeT (void) : ScalarType("uint8", sizeof(uint8)) {}
template <> inline ScalarTypeT<int16>::ScalarTypeT (void) : ScalarType("int16", sizeof(int16)) {}
template <> inline ScalarTypeT<uint16>::ScalarTypeT (void) : ScalarType("uint16", sizeof(uint16)) {}
template <> inline ScalarTypeT<int32>::ScalarTypeT (void) : ScalarType("int32", sizeof(int32)) {}
template <> inline ScalarTypeT<uint32>::ScalarTypeT (void) : ScalarType("uint32", sizeof(uint32)) {}
template <> inline ScalarTypeT<int64>::ScalarTypeT (void) : ScalarType("int64", sizeof(int64)) {}
template <> inline ScalarTypeT<uint64>::ScalarTypeT (void) : ScalarType("uint64", sizeof(uint64)) {}
template <> inline ScalarTypeT<float32>::ScalarTypeT (void) : ScalarType("float32", sizeof(float32)) {}
template <> inline ScalarTypeT<float64>::ScalarTypeT (void) : ScalarType("float64", sizeof(float64)) {}
template <> inline ScalarTypeT<string>::ScalarTypeT (void) : ScalarType("string", sizeof(string)) {}

template <> inline bool ScalarTypeT<string>::ToNetworkByteOrder (void * const ref) const { (void) ref; return true; }
template <> inline bool ScalarTypeT<string>::FromNetworkByteOrder (void * const ref) const { (void) ref; return true; }

template <typename Type> bool ScalarTypeT<Type>::ToNetworkByteOrder (void * const ref) const
{ 

  Type value = ccs::HelperTools::ToNetworkByteOrder(*(static_cast<Type*>(ref))); 
  *(static_cast<Type*>(ref)) = value; 

  return true; 

}

template <typename Type> bool ScalarTypeT<Type>::FromNetworkByteOrder (void * const ref) const
{ 

  Type value = ccs::HelperTools::FromNetworkByteOrder(*(static_cast<Type*>(ref))); 
  *(static_cast<Type*>(ref)) = value; 

  return true; 

}

#define SCALARTPARSEINSTANCE(BASIC_TYPE,FORMAT) \
template <> inline uint32 ScalarTypeT<BASIC_TYPE>::ParseInstance (void * const ref, const char8 * const buffer) const \
{ \
\
  log_debug("ScalarTypeT<" #BASIC_TYPE ">::ParseInstance - Try and parse '%s' ..", buffer); \
\
  bool status = ((NULL_PTR_CAST(void*) != ref) && \
                 (NULL_PTR_CAST(const char8*) != buffer)); \
\
  uint32 ret = 0u; \
\
  if (status) \
    { \
      if (ccs::HelperTools::StringCompare(buffer, "0x", 2)) \
        { \
          ret = this->ScalarType::ParseInstance(ref, buffer); \
        } \
      else \
        { \
          int num = 0; \
          status = (sscanf(buffer, FORMAT, static_cast<BASIC_TYPE*>(ref), &num) > 0); \
\
          if (status) \
            { \
              ret = static_cast<uint32>(num); \
            } \
        } \
    } \
\
  log_debug("ScalarTypeT<" #BASIC_TYPE ">::ParseInstance - .. returning '%d'", ret); \
\
  return ret; \
\
}

template <> inline uint32 ScalarTypeT<boolean>::ParseInstance (void * const ref, const char8 * const buffer) const
{

  log_debug("ScalarTypeT<boolean>::ParseInstance - Try and parse '%s' ..", buffer);

  bool status = ((NULL_PTR_CAST(void*) != ref) && 
                 (NULL_PTR_CAST(const char8*) != buffer));

  uint32 ret = 0u;

  if (status)
    {
      if (ccs::HelperTools::StringCompare(buffer, "false", 5))
        {
          *(static_cast<boolean*>(ref)) = false;
          ret = 5u;
        }
      else if (ccs::HelperTools::StringCompare(buffer, "0", 1))
        {
          *(static_cast<boolean*>(ref)) = false;
          ret = 1u;
        }
      else if (ccs::HelperTools::StringCompare(buffer, "true", 4))
        {
          *(static_cast<boolean*>(ref)) = true;
          ret = 4u;
        }
      else if (ccs::HelperTools::StringCompare(buffer, "1", 1))
        {
          *(static_cast<boolean*>(ref)) = true;
          ret = 1u;
        }
    }

  log_debug("ScalarTypeT<boolean>::ParseInstance - .. returning '%d'", ret);

  return ret;

}

SCALARTPARSEINSTANCE(char8,"\"%c\"%n")
SCALARTPARSEINSTANCE(int8,"%hhd%n")
SCALARTPARSEINSTANCE(uint8,"%hhu%n")
SCALARTPARSEINSTANCE(int16,"%hd%n")
SCALARTPARSEINSTANCE(uint16,"%hu%n")
SCALARTPARSEINSTANCE(int32,"%d%n")
SCALARTPARSEINSTANCE(uint32,"%u%n")
SCALARTPARSEINSTANCE(int64,"%ld%n")
SCALARTPARSEINSTANCE(uint64,"%lu%n")
SCALARTPARSEINSTANCE(float32,"%g%n")
SCALARTPARSEINSTANCE(float64,"%lg%n")

template <> inline uint32 ScalarTypeT<string>::ParseInstance (void * const ref, const char8 * const buffer) const
{

  log_debug("ScalarTypeT<string>::ParseInstance - Try and parse '%s' ..", buffer);

  bool status = ((NULL_PTR_CAST(void*) != ref) && 
                 (NULL_PTR_CAST(const char8*) != buffer));

  uint32 ret = 0u;

  if (status)
    {
      int32 start = ccs::HelperTools::Find(buffer, '"');
      int32 close = ccs::HelperTools::Find(buffer, '"', start+1u);

      status = (-1 != start);

      if (status)
        {
          ret = close + 1u;
          (void)ccs::HelperTools::SafeStringCopy(static_cast<ccs::types::char8*>(ref), buffer+start+1u, this->GetSize(), '"');
        }
    }

  log_debug("ScalarTypeT<string>::ParseInstance - .. returning '%d'", ret);

  return ret;

}

#define SCALARTSERIALISEINSTANCE(BASIC_TYPE,FORMAT) \
template <> inline bool ScalarTypeT<BASIC_TYPE>::SerialiseInstance (const void * const ref, char8 * const buffer, const uint32 size) const \
{ \
\
  bool status = ((NULL_PTR_CAST(const void*) != ref) && \
                 (NULL_PTR_CAST(char8*) != buffer)); \
\
  if (status) \
    { \
      status = (snprintf(buffer, size, FORMAT, *(static_cast<const BASIC_TYPE*>(ref))) > 0); \
    } \
\
  return status; \
\
}

template <> inline bool ScalarTypeT<boolean>::SerialiseInstance (const void * const ref, char8 * const buffer, const uint32 size) const
{

  bool status = ((NULL_PTR_CAST(const void*) != ref) &&
                 (NULL_PTR_CAST(char8*) != buffer));

  if (status)
    {
      if (true == *(static_cast<const boolean*>(ref)))
        {
          (void)ccs::HelperTools::SafeStringCopy(buffer, "true", size);
        }
      else
        {
          (void)ccs::HelperTools::SafeStringCopy(buffer, "false", size);
        }
    }

  return status;

}

SCALARTSERIALISEINSTANCE(char8,"\"%c\"")
SCALARTSERIALISEINSTANCE(int8,"%hhd")
SCALARTSERIALISEINSTANCE(uint8,"%hhu")
SCALARTSERIALISEINSTANCE(int16,"%hd")
SCALARTSERIALISEINSTANCE(uint16,"%hu")
SCALARTSERIALISEINSTANCE(int32,"%d")
SCALARTSERIALISEINSTANCE(uint32,"%u")
SCALARTSERIALISEINSTANCE(int64,"%ld")
SCALARTSERIALISEINSTANCE(uint64,"%lu")
SCALARTSERIALISEINSTANCE(float32,"%g")
SCALARTSERIALISEINSTANCE(float64,"%lg")

template <> inline bool ScalarTypeT<string>::SerialiseInstance (const void * const ref, char8 * const buffer, const uint32 size) const
{

  bool status = ((NULL_PTR_CAST(const void*) != ref) &&
                 (NULL_PTR_CAST(char8*) != buffer));

  if (status)
    {
      status = (snprintf(buffer, size, "\"%s\"", static_cast<const ccs::types::char8*>(ref)) > 0);
    }

  return status;

}

} // namespace types

} // namespace ccs

#endif // _ScalarType_h_

