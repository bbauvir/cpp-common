/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

// Local header files

#include "types.h" // Misc. type definition
#include "tools.h" // Misc. helper functions

#include "NetTools.h"

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include "log-api.h" // Syslog wrapper routines

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyType.h" // Introspectable type definition class ..
#include "AnyTypeHelper.h" // .. helper routines

#include "AnyValue.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ccs::types"

// Type definition

namespace ccs {

namespace types {

// Global variables

// Function declaration

// Function definition

void* AnyValue::CreateInstance (void)
{

  log_trace("AnyValue::CreateInstance - Entering method");

  bool status = ((this->GetSize() > 0u) && (NULL_PTR_CAST(uint8*) == _instance));

  if (status)
    {
      log_debug("AnyValue::CreateInstance - Create instance of size '%u'", this->GetSize());
      _instance = new (std::nothrow) uint8 [this->GetSize()];
      status = (NULL_PTR_CAST(uint8*) != _instance);
    }

  if (status)
    {
      _allocated = true;
      (void)memset(_instance, 0, this->GetSize());
    }

  if (!status)
    {
      log_error("AnyValue::CreateInstance - Issue with instance of size '%u'", this->GetSize());
    }

  log_trace("AnyValue::CreateInstance - Leaving method");

  return static_cast<void*>(_instance);

}

void AnyValue::DeleteInstance (void)
{

  log_trace("AnyValue::DeleteInstance - Entering method");

  if (_allocated && (NULL_PTR_CAST(uint8*) != _instance))
    {
      log_debug("AnyValue::DeleteInstance - Delete instance of size '%u'", this->GetSize());
      delete [] _instance;
    }

  _instance = NULL_PTR_CAST(uint8*);

  log_trace("AnyValue::DeleteInstance - Leaving method");

  return;

}

uint32 AnyValue::GetSize (void) const
{

  bool status = (true == static_cast<bool>(_type));

  uint32 size = 0u;

  if (status)
    {
      size = _type->GetSize();
    }

  return size;

}

ccs::base::SharedReference<const AnyType> AnyValue::GetType (void) const { return _type; }

void* AnyValue::GetInstance (void) const { return static_cast<void*>(_instance); }

bool AnyValue::ToNetworkByteOrder (void)
{

  bool status = (true == static_cast<bool>(_type));

  if (status)
    {
      status = (_order == ccs::HelperTools::GetNativeByteOrder());
    }

  if (status)
    {
      status = this->GetType()->ToNetworkByteOrder(this->GetInstance());
    }

  if (status)
    {
      _order = ccs::types::NetworkByteOrder;
    }

  return status;

}

bool AnyValue::FromNetworkByteOrder (void)
{

  bool status = (true == static_cast<bool>(_type));

  if (status)
    {
      status = (_order == ccs::types::NetworkByteOrder);
    }

  if (status)
    {
      status = this->GetType()->FromNetworkByteOrder(this->GetInstance());
    }

  if (status)
    {
      _order = ccs::HelperTools::GetNativeByteOrder();
    }

  return status;

}

bool AnyValue::ParseInstance (const char8 * const buffer)
{

  bool status = (true == static_cast<bool>(_type));

  if (status)
    {
      status = this->GetType()->ParseInstance(this->GetInstance(), buffer);
    }

  return status;

}

bool AnyValue::SerialiseInstance (char8 * const buffer, uint32 size) const
{

  bool status = (true == static_cast<bool>(_type));

  if (status)
    {
      status = (0u < this->GetType()->SerialiseInstance(this->GetInstance(), buffer, size));
    }

  return status;

}

bool AnyValue::SerialiseType (char8 * const buffer, uint32 size) const
{

  bool status = (true == static_cast<bool>(_type));

  if (status)
    {
      status = (0u < ccs::HelperTools::Serialise(this->GetType(), buffer, size));
    }

  return status;

}

AnyValue& AnyValue::operator= (const AnyValue& value) // Copy assignment operator
{

  log_trace("AnyValue:::operator= - Entering method");

  if (this != &value)
    {
      bool status = (true == static_cast<bool>(_type));

      if (!status) // Uninitialised instance
        {
          // Initialise attributes
          _type = value.GetType();

          // Create value buffer
          status = (NULL_PTR_CAST(void*) != this->CreateInstance());
        }
      else
        {
          status = (NULL_PTR_CAST(void*) != this->GetInstance());
        }

      if (status)
        {
          status = (NULL_PTR_CAST(void*) != value.GetInstance());
        }

      if (status)
        {
          status = (this->GetSize() == value.GetSize());
        }

      if (status)
        {
          (void)memcpy(this->GetInstance(), value.GetInstance(), this->GetSize());
        }
    }

  log_trace("AnyValue:::operator= - Leaving method");

  return *this;

}

AnyValue::AnyValue (void)
  : _instance(NULL_PTR_CAST(uint8*))
  , _allocated(false)
  , _order(ccs::HelperTools::GetNativeByteOrder())
{}

#define CONSTRUCTOR_DEFINITION(BASIC_TYPE, INTRO_TYPE) \
AnyValue::AnyValue (const BASIC_TYPE & value) \
  : AnyValue() \
{ \
  Initialise(INTRO_TYPE, value); \
  return; \
}

CONSTRUCTOR_DEFINITION(boolean, Boolean)
CONSTRUCTOR_DEFINITION(char8, Character8)
CONSTRUCTOR_DEFINITION(int8, SignedInteger8)
CONSTRUCTOR_DEFINITION(uint8, UnsignedInteger8)
CONSTRUCTOR_DEFINITION(int16, SignedInteger16)
CONSTRUCTOR_DEFINITION(uint16, UnsignedInteger16)
CONSTRUCTOR_DEFINITION(int32, SignedInteger32)
CONSTRUCTOR_DEFINITION(uint32, UnsignedInteger32)
CONSTRUCTOR_DEFINITION(int64, SignedInteger64)
CONSTRUCTOR_DEFINITION(uint64, UnsignedInteger64)
CONSTRUCTOR_DEFINITION(float32, Float32)
CONSTRUCTOR_DEFINITION(float64, Float64)

#undef CONSTRUCTOR_DEFINITION

AnyValue::AnyValue (const string& value) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _order(ccs::HelperTools::GetNativeByteOrder())
{

  // Relying on copy constructor
  _type = String;

  // Create value buffer
  (void)this->CreateInstance();

  if (NULL != this->GetInstance())
    { // Copy value
      (void)memcpy(this->GetInstance(), &value, this->GetSize());
    }

  return;

}

AnyValue::AnyValue (const char8 * const type) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _order(ccs::HelperTools::GetNativeByteOrder())
{

  log_trace("AnyValue::AnyValue('%s') - Entering method", type);

  bool _copy = true;

  if (ccs::HelperTools::IsJSONObject(type))
    { // Datatype definition as JSON stream
      ccs::base::SharedReference<AnyType> tmp;
      bool status = (0u < ccs::HelperTools::Parse(tmp, type));

      if (status)
        {
          _type = tmp;
        }

      _copy = false;
    }
  else if (ccs::types::MaxStringLength > ccs::HelperTools::StringLength(type)) // Alternatively use 'N'
    {
      log_debug("AnyValue::AnyValue('%s') - Map to 'string' ..", type);
      _type = String;
    }
  else
    { // Character array
      log_debug("AnyValue::AnyValue('%s') - Map to 'char8[]' ..", type);
      _type = ccs::base::SharedReference<const AnyType>(ccs::HelperTools::NewArrayType("char8[]", ccs::types::Character8, ccs::HelperTools::StringLength(type)+1u));
    }

  // Create value buffer
  (void)this->CreateInstance();

  if (_copy)
    {
      log_debug("AnyValue::AnyValue('%s') - .. and copy", type);
      // Copy value
      (void)memcpy(this->GetInstance(), type, this->GetSize());
    }

  log_trace("AnyValue::AnyValue('%s') - Leaving method", type);

  return;

}

#define SHAREDREF_CONSTRUCTOR_DEFINITION(ANY_TYPE) \
AnyValue::AnyValue (const ccs::base::SharedReference<const ANY_TYPE>& type) \
  : AnyValue() \
{ \
  Initialise(type); \
  return; \
}

SHAREDREF_CONSTRUCTOR_DEFINITION(AnyType)
SHAREDREF_CONSTRUCTOR_DEFINITION(ScalarType)
SHAREDREF_CONSTRUCTOR_DEFINITION(CompoundType)
SHAREDREF_CONSTRUCTOR_DEFINITION(ArrayType)

#undef SHAREDREF_CONSTRUCTOR_DEFINITION


AnyValue::AnyValue (AnyType * const type) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _type(ccs::base::SharedReference<const AnyType>(const_cast<const AnyType*>(type))), _order(ccs::HelperTools::GetNativeByteOrder())
{

  // Create value buffer
  (void)this->CreateInstance();

  return;

}

AnyValue::AnyValue (const AnyType * const type) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _type(ccs::base::SharedReference<const AnyType>(type)), _order(ccs::HelperTools::GetNativeByteOrder())
{

  // Create value buffer
  (void)this->CreateInstance();

  return;

}

AnyValue::AnyValue (const ArrayType& type) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _order(ccs::HelperTools::GetNativeByteOrder()) // Shared reference from using copy constructor
{

  // Relying on copy constructor
  _type = ccs::base::SharedReference<const AnyType>(ccs::base::SharedReference<const ArrayType>(new (std::nothrow) ArrayType (type)));

  // Create value buffer
  (void)this->CreateInstance();

  return;

}

AnyValue::AnyValue (const CompoundType& type) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _order(ccs::HelperTools::GetNativeByteOrder()) // Shared reference from using copy constructor
{

  // Relying on copy constructor
  _type = ccs::base::SharedReference<const AnyType>(ccs::base::SharedReference<const CompoundType>(new (std::nothrow) CompoundType (type)));

  // Create value buffer
  (void)this->CreateInstance();

  return;

}

AnyValue::AnyValue (const ScalarType& type) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _order(ccs::HelperTools::GetNativeByteOrder()) // Shared reference from using copy constructor
{

  // Relying on copy constructor
  _type = ccs::base::SharedReference<const AnyType>(ccs::base::SharedReference<const ScalarType>(new (std::nothrow) ScalarType (type)));

  // Create value buffer
  (void)this->CreateInstance();

  return;

}

AnyValue::AnyValue (const ccs::base::SharedReference<const AnyType>& type, void * const instance) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _type(type), _order(ccs::HelperTools::GetNativeByteOrder())
{

  // Initialise attributes
  _instance = static_cast<uint8*>(instance);

  return;

}

AnyValue::AnyValue (const AnyType * const type, void * const instance) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _type(ccs::base::SharedReference<const AnyType>(type)), _order(ccs::HelperTools::GetNativeByteOrder())
{

  // Initialise attributes
  _instance = static_cast<uint8*>(instance);

  return;

}

AnyValue::AnyValue (const ArrayType& type, void * const instance) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _order(ccs::HelperTools::GetNativeByteOrder()) // shared_ptr from using copy constructor
{

  // Relying on copy constructor
  _type = ccs::base::SharedReference<const AnyType>(ccs::base::SharedReference<const ArrayType>(new (std::nothrow) ArrayType (type)));

  // Initialise attributes
  _instance = static_cast<uint8*>(instance);

  return;

}

AnyValue::AnyValue (const CompoundType& type, void * const instance) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _order(ccs::HelperTools::GetNativeByteOrder()) // shared_ptr from using copy constructor
{

  // Relying on copy constructor
  _type = ccs::base::SharedReference<const AnyType>(ccs::base::SharedReference<const CompoundType>(new (std::nothrow) CompoundType (type)));

  // Initialise attributes
  _instance = static_cast<uint8*>(instance);

  return;

}

AnyValue::AnyValue (const ScalarType& type, void * const instance) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _order(ccs::HelperTools::GetNativeByteOrder()) // shared_ptr from using copy constructor
{

  // Relying on copy constructor
  _type = ccs::base::SharedReference<const AnyType>(ccs::base::SharedReference<const ScalarType>(new (std::nothrow) ScalarType (type)));

  // Initialise attributes
  _instance = static_cast<uint8*>(instance);

  return;

}

AnyValue::AnyValue (const AnyValue& value) : _instance(NULL_PTR_CAST(uint8*)), _allocated(false), _type(value.GetType()), _order(ccs::HelperTools::GetNativeByteOrder()) // Copy constructor
{

  // Create value buffer
  (void)this->CreateInstance();

  // Copy value
  (void)memcpy(this->GetInstance(), value.GetInstance(), this->GetSize());

  return;

}

AnyValue::~AnyValue (void)
{

  (void)this->DeleteInstance();

  return;

}

} // namespace types

} // namespace ccs
