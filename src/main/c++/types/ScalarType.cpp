/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

//#include <memory> // std::shared_ptr, etc.

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "SysTools.h" // Misc. helper functions

#include "NetTools.h" 

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE 
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE 
#include "log-api.h" // Syslog wrapper routines

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyType.h"
#include "ScalarType.h"

//#include "AnyTypeHelper.h"

// Constants

#ifdef LOG_TRACE_ENABLE
#undef log_trace
#define log_trace(arg_msg...) { fprintf(stdout, arg_msg); }
#endif

#ifdef LOG_DEBUG_ENABLE
#undef log_debug
#define log_debug(arg_msg...) { fprintf(stdout, arg_msg); }
#endif

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ccs::types"

// Type definition

namespace ccs {

namespace types {

// Global variables

const ccs::base::SharedReference<const ScalarType> Boolean (new (std::nothrow) ScalarTypeT<boolean>);
const ccs::base::SharedReference<const ScalarType> Character8 (new (std::nothrow) ScalarTypeT<char8>);
const ccs::base::SharedReference<const ScalarType> SignedInteger8 (new (std::nothrow) ScalarTypeT<int8>);
const ccs::base::SharedReference<const ScalarType> UnsignedInteger8 (new (std::nothrow) ScalarTypeT<uint8>);
const ccs::base::SharedReference<const ScalarType> SignedInteger16 (new (std::nothrow) ScalarTypeT<int16>);
const ccs::base::SharedReference<const ScalarType> UnsignedInteger16 (new (std::nothrow) ScalarTypeT<uint16>);
const ccs::base::SharedReference<const ScalarType> SignedInteger32 (new (std::nothrow) ScalarTypeT<int32>);
const ccs::base::SharedReference<const ScalarType> UnsignedInteger32 (new (std::nothrow) ScalarTypeT<uint32>);
const ccs::base::SharedReference<const ScalarType> SignedInteger64 (new (std::nothrow) ScalarTypeT<int64>);
const ccs::base::SharedReference<const ScalarType> UnsignedInteger64 (new (std::nothrow) ScalarTypeT<uint64>);
const ccs::base::SharedReference<const ScalarType> Float32 (new (std::nothrow) ScalarTypeT<float32>);
const ccs::base::SharedReference<const ScalarType> Float64 (new (std::nothrow) ScalarTypeT<float64>);
const ccs::base::SharedReference<const ScalarType> String (new (std::nothrow) ScalarTypeT<string>);

// Function declaration

// Function definition

bool ScalarType::ToNetworkByteOrder (void * const ref) const // In-place conversion
{

  log_trace("ScalarType::ToNetworkByteOrder - Entering method");

  bool status = (NULL_PTR_CAST(void*) != ref);

  if (status)
    {
      if (this->GetSize() == 1u)
        {
          // Do nothing
        }
      else if (this->GetSize() == 2u)
        {
          uint16 value = ccs::HelperTools::ToNetworkByteOrder(*(static_cast<uint16*>(ref)));
          *(static_cast<uint16*>(ref)) = value;
        }
      else if (this->GetSize() == 4u)
        {
          uint32 value = ccs::HelperTools::ToNetworkByteOrder(*(static_cast<uint32*>(ref)));
          *(static_cast<uint32*>(ref)) = value;
        }
      else if (this->GetSize() == 8u)
        {
          uint64 value = ccs::HelperTools::ToNetworkByteOrder(*(static_cast<uint64*>(ref)));
          *(static_cast<uint64*>(ref)) = value;
        }
      else
        {
          status = false;
        }
     }

  log_trace("ScalarType::ToNetworkByteOrder - Leaving method");

  return status;

}

bool ScalarType::FromNetworkByteOrder (void * const ref) const // In-place conversion
{

  log_trace("ScalarType::FromNetworkByteOrder - Entering method");

  bool status = (NULL_PTR_CAST(void*) != ref);

  if (status)
    {
      if (this->GetSize() == 1u)
        {
          // Do nothing
        }
      else if (this->GetSize() == 2u)
        {
          uint16 value = ccs::HelperTools::FromNetworkByteOrder(*(static_cast<uint16*>(ref)));
          *(static_cast<uint16*>(ref)) = value;
        }
      else if (this->GetSize() == 4u)
        {
          uint32 value = ccs::HelperTools::FromNetworkByteOrder(*(static_cast<uint32*>(ref)));
          *(static_cast<uint32*>(ref)) = value;
        }
      else if (this->GetSize() == 8u)
        {
          uint64 value = ccs::HelperTools::FromNetworkByteOrder(*(static_cast<uint64*>(ref)));
          *(static_cast<uint64*>(ref)) = value;
        }
      else
        {
          status = false;
        }
    }

  log_trace("ScalarType::FromNetworkByteOrder - Leaving method");

  return status;

}

uint32 ScalarType::ParseInstance (void * const ref, const char8 * const buffer) const
{

  log_trace("ScalarType::ParseInstance - Entering method");

  bool status = ((NULL_PTR_CAST(void*) != ref) && 
                 (NULL_PTR_CAST(const char8*) != buffer));

  uint32 ret = 0u;

  if (status)
    {
      int num = 0; // '%n' requires int*

      if (this->GetSize() == 1u)
        {
          status = (sscanf(buffer, "0x%hhx%n", static_cast<uint8*>(ref), &num) > 0);
        }
      else if (this->GetSize() == 2u)
        {
          status = (sscanf(buffer, "0x%hx%n", static_cast<uint16*>(ref), &num) > 0);
        }
      else if (this->GetSize() == 4u)
        {
          status = (sscanf(buffer, "0x%x%n", static_cast<uint32*>(ref), &num) > 0);
        }
      else if (this->GetSize() == 8u)
        {
          status = (sscanf(buffer, "0x%lx%n", static_cast<uint64*>(ref), &num) > 0);
        }
      else
        {
          status = false;
        }

      if (status)
        {
          ret = static_cast<ccs::types::uint32>(num);
        }
    }

  log_trace("ScalarType::ParseInstance - Leaving method");

  return ret;

}

bool ScalarType::SerialiseInstance (const void * const ref, char8 * const buffer, const uint32 size) const
{

  log_trace("ScalarType::SerialiseInstance - Entering method");

  bool status = ((NULL_PTR_CAST(const void*) != ref) && 
                 (NULL_PTR_CAST(char8*) != buffer));

  if (status)
    {
      if (this->GetSize() == 1u)
        {
          status = (snprintf(buffer, size, "0x%2.2hhx", *(static_cast<const uint8*>(ref))) > 0);
        }
      else if (this->GetSize() == 2u)
        {
          status = (snprintf(buffer, size, "0x%4.4hx", *(static_cast<const uint16*>(ref))) > 0);
        }
      else if (this->GetSize() == 4u)
        {
          status = (snprintf(buffer, size, "0x%8.8x", *(static_cast<const uint32*>(ref))) > 0);
        }
      else if (this->GetSize() == 8u)
        {
          status = (snprintf(buffer, size, "0x%16.16lx", *(static_cast<const uint64*>(ref))) > 0);
        }
      else
        {
          status = false;
        }
     }

  log_trace("ScalarType::SerialiseInstance - Leaving method");

  return status;

}

bool ScalarType::operator== (const ScalarType& type) const
{

  log_trace("bool ScalarType::operator== - Entering method");

  bool status = (this->GetSize() == type.GetSize());

  if (status)
    {
      status = ccs::HelperTools::StringCompare(this->GetName(), type.GetName());
    }

  log_trace("bool ScalarType::operator== - Leaving method");

  return status;

}

bool ScalarType::operator== (const AnyType& type) const
{

  log_trace("bool ScalarType::operator== - Entering method");

  bool status = (NULL_PTR_CAST(const ScalarType*) != dynamic_cast<const ScalarType*>(&type));

  if (status)
    {
      status = (this->GetSize() == type.GetSize());
    }

  if (status)
    {
      status = ccs::HelperTools::StringCompare(this->GetName(), type.GetName());
    }

  log_trace("bool ScalarType::operator== - Leaving method");

  return status;

}

ScalarType::ScalarType (void) : AnyType () {}
ScalarType::ScalarType (const char8 * const type, const uint32 size) : AnyType (type, size) {}
ScalarType::ScalarType (const ScalarType& type) : AnyType ()
{

  (void)this->SetName(type.GetName());
  (void)this->SetSize(type.GetSize());

}

} // namespace types

} // namespace ccs
