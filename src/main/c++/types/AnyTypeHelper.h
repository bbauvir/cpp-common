/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Generic type class definition
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file AnyTypeHelper.h
 * @brief Header file for AnyType helper methods.
 * @date 01/11/2017
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @details This header file contains the definition of the AnyType helper methods.
 */

#ifndef _AnyTypeHelper_h_
#define _AnyTypeHelper_h_

// Global header files

// Local header files

#include "log-api.h" // Syslog wrapper routines

#include "JSONTools.h"
#include "SysTools.h" // ccs::HelperTools::IsIntegerString, etc.

#include "SharedReference.h" // In lieu of std::shared_ptr

#include "AnyType.h"
#include "ArrayType.h"
#include "CompoundType.h"
#include "ScalarType.h"

#include "AnyTypeDatabase.h"

// Constants

// Type definition

namespace ccs {

namespace HelperTools {

// Global variables

// Function declaration

/**
 * @brief Test AnyType against specialised type.
 * @param type Pointer to AnyType instance
 * @return true if instance is of specialised type.
 */

template <typename Type> inline bool Is (const ccs::types::AnyType * const type);
template <typename Type> inline bool Is (const ccs::base::SharedReference<const ccs::types::AnyType>& type);
template <typename Type> inline bool Is (const ccs::types::char8 * const buffer); // JSON type description

static inline ccs::types::ArrayType* NewArrayType (const ccs::types::char8 * const type, const ccs::types::char8 * const base, const ccs::types::uint32 multiplicity = 0u); // Registered name
static inline ccs::types::ArrayType* NewArrayType (const ccs::types::char8 * const type, const ccs::base::SharedReference<const ccs::types::AnyType>& base, const ccs::types::uint32 multiplicity = 0u); // Specialised class depends on base type
static inline ccs::types::ArrayType* NewArrayType (const ccs::types::char8 * const type, const ccs::types::AnyType * const base, const ccs::types::uint32 multiplicity = 0u); // Specialised class depends on base type
static inline ccs::types::ArrayType* NewArrayType (const ccs::types::char8 * const type, const ccs::types::AnyType& base, const ccs::types::uint32 multiplicity = 0u); // Specialised class depends on base type

/**
 * @brief Parse AnyType routine.
 * @details The routine tests if the path is valid within the defined type.
 * @param type Pointer to AnyType instance
 * @param name Attribute fully-qualified name, i.e. path within AnyType instance.
 * @return true if attribute exists within the defined type.
 * @note The routine follows a recursion pattern and can therefore not be inline.
 * @note Bug 12521 - sdn::topic::GetAttributeName(uint32) returns NULL in case of out-of-bounds
 * index. The code should ensure that ccs::HelperTools::HasAttribute(NULL) also returns false.
 */

static bool HasAttribute (const ccs::types::AnyType * const type, const ccs::types::char8 * const name); // SDN core library does not yet use SharedReference
static bool HasAttribute (const ccs::base::SharedReference<const ccs::types::AnyType>& type, const ccs::types::char8 * const name);

/**
 * @brief Parse AnyType routine.
 * @details The routine provides the offset in memory corresponding to the defined
 * type.
 * @param type Pointer to AnyType instance
 * @param name Attribute fully-qualified name, i.e. path within AnyType instance.
 * @return Offset within memory if attribute exists in the defined type, 0u otherwise.
 * @note The routine returns 0u in case of invalid attribute name. The calling application
 * should itself perform test that the name is valid to discriminate between first
 * attribute and error condition.
 * @note The routine follows a recursion pattern and can therefore not be inline.
 */

static ccs::types::uint32 GetAttributeOffset (const ccs::types::AnyType * const type, const ccs::types::char8 * const name); // SDN core library does not yet use SharedReference
static ccs::types::uint32 GetAttributeOffset (const ccs::base::SharedReference<const ccs::types::AnyType>& type, const ccs::types::char8 * const name);

/**
 * @brief Parse AnyType routine.
 * @details The routine returns the size (memory footprint) of the AnyType instance
 * corresponding to the attribute.
 * @param type Pointer to AnyType instance
 * @param name Attribute fully-qualified name, i.e. path within AnyType instance.
 * @return Size of the AnyType instance if attribute exists in the defined type, 0u otherwise.
 * @note The routine returns 0u in case of invalid attribute name. The calling application
 * should itself perform test that the name is valid to discriminate between first
 * attribute and error condition.
 */

static inline ccs::types::uint32 GetAttributeSize (const ccs::types::AnyType * const type, const ccs::types::char8 * const name); // SDN core library does not yet use SharedReference
static inline ccs::types::uint32 GetAttributeSize (const ccs::base::SharedReference<const ccs::types::AnyType>& type, const ccs::types::char8 * const name);

/**
 * @brief Parse AnyType routine.
 * @details The routine returns the AnyType instance corresponding to the attribute.
 * @param type Pointer to AnyType instance
 * @param name Attribute fully-qualified name, i.e. path within AnyType instance.
 * @return AnyType instance if attribute exists in the defined type, invalid pointer otherwise.
 * @note The routine follows a recursion pattern and can therefore not be inline.
 */

static ccs::base::SharedReference<const ccs::types::AnyType> GetAttributeType (const ccs::types::AnyType * const type, const ccs::types::char8 * const name); // SDN core library does not yet use SharedReference
static ccs::base::SharedReference<const ccs::types::AnyType> GetAttributeType (const ccs::base::SharedReference<const ccs::types::AnyType>& type, const ccs::types::char8 * const name);

/**
 * @brief Parse JSON stream and fill AnyType instance.
 * @details The routine parses a JSON type description string and instantiates the appropriate AnyType
 * corresponding to the description.
 * @param type Pointer where the  AnyType instance will be stored.
 * @param buffer A JSON type description string.
 * @pre
 *   false == static_cast<bool>(type) &&
 *   NULL != buffer &&
 *   JSON string represents a type description * @return Number of consumed characters, 0 in case of error.
 * @note The routine follows a recursion pattern and can therefore not be inline.
 */

static ccs::types::uint32 Parse (ccs::base::SharedReference<ccs::types::AnyType>& type, const ccs::types::char8 * const buffer);
static ccs::types::uint32 Parse (ccs::base::SharedReference<ccs::types::ArrayType>& type, const ccs::types::char8 * const buffer);
static ccs::types::uint32 Parse (ccs::base::SharedReference<ccs::types::CompoundType>& type, const ccs::types::char8 * const buffer);
static ccs::types::uint32 Parse (ccs::base::SharedReference<ccs::types::ScalarType>& type, const ccs::types::char8 * const buffer);

/**
 * @brief Serialisation routine.
 * @details The routine provides a human-readable JSON string representing an AnyType instance.
 * @param type Pointer to AnyType instance
 * @param buffer Output string
 * @param size Output string maximum size
 * @pre
 *   true == static_cast<bool>(type) &&
 *   NULL != buffer
 * @return true if pre-conditions are met, false otherwise.
 * @note The routine follows a recursion pattern and can therefore not be inline.
 */

static bool Serialise (const ccs::base::SharedReference<const ccs::types::AnyType>& type, ccs::types::char8 * const buffer, const ccs::types::uint32 size, ccs::base::AnyTypeDatabase * deps = 0);
static bool Serialise (const ccs::base::SharedReference<const ccs::types::ArrayType>& type, ccs::types::char8 * const buffer, ccs::types::uint32 size, ccs::base::AnyTypeDatabase * deps = 0);
static bool Serialise (const ccs::base::SharedReference<const ccs::types::CompoundType>& type, ccs::types::char8 * const buffer, ccs::types::uint32 size, ccs::base::AnyTypeDatabase * deps = 0);
static bool Serialise (const ccs::base::SharedReference<const ccs::types::ScalarType>& type, ccs::types::char8 * const buffer, ccs::types::uint32 size, ccs::base::AnyTypeDatabase * deps = 0);

template <typename Type> bool Serialise (const ccs::base::SharedReference<Type>& type, ccs::types::char8 * const buffer, ccs::types::uint32 size, ccs::base::AnyTypeDatabase * deps = 0)
{

  (void)deps;
  ccs::base::SharedReference<const Type> __type (const_cast<const Type*>(type.GetReference()), type.GetCounter());
  return Serialise(__type, buffer, size);

}

/**
 * @brief Type conversion routine.
 * @details The routine converts an AnyType instance to a flattened structure.
 * @param type Pointer to AnyType instance
 * @pre
 *   true == static_cast<bool>(type)
 * @return CompoundType instance upon success, invalid pointer otherwise.
 * @note The routine follows a recursion pattern and can therefore not be inline.
 */

static ccs::base::SharedReference<const ccs::types::CompoundType> Flatten (const ccs::base::SharedReference<const ccs::types::AnyType>& type);
#ifndef INHERIT_FROM_COMPOUND_TYPE
static ccs::base::SharedReference<const ccs::types::CompoundType> Flatten (const ccs::base::SharedReference<const ccs::types::ArrayType>& type);
#endif
static ccs::base::SharedReference<const ccs::types::CompoundType> Flatten (const ccs::base::SharedReference<const ccs::types::CompoundType>& type);

template <typename Type> ccs::base::SharedReference<const ccs::types::CompoundType> Flatten (const ccs::base::SharedReference<Type>& type)
{

  ccs::base::SharedReference<const Type> __type (const_cast<const Type*>(type.GetReference()), type.GetCounter());
  return Flatten(__type);

}

/**
 * @brief Logs contents on the GlobalTypeDatabase for investigation purposes.
 */

static inline void DumpGTDBToLog (void);

// Function definition

template <typename Type> inline bool Is (const ccs::types::AnyType * const type) 
{ 

  return (NULL_PTR_CAST(const Type * const) != dynamic_cast<const Type * const>(type)); 

}

template <typename Type> inline bool Is (const ccs::base::SharedReference<const ccs::types::AnyType>& type) 
{
 
  const ccs::base::SharedReference<const Type> __type (type);
  return static_cast<bool>(__type); 

}

static inline bool IsString (const ccs::base::SharedReference<const ccs::types::AnyType>& type)
{

  bool status = (ccs::HelperTools::Is<ccs::types::ScalarType>(type) && (ccs::types::String == type));

  if (!status)
    {
      status = ccs::HelperTools::Is<ccs::types::ArrayType>(type);

      if (status)
        {
          const ccs::base::SharedReference<const ccs::types::ArrayType> a_type (type); // Safe dynamic cast
          const ccs::base::SharedReference<const ccs::types::ScalarType> s_type (a_type->GetElementType());
          status = ((ccs::types::Character8 == s_type) || (ccs::types::UnsignedInteger8 == s_type));
        }
    }

  return status;

}

static inline bool GetTypeNameFromJSONContent (const ccs::types::char8 * const buffer, ccs::types::char8 * const name, const ccs::types::uint32 size)
{

  bool status = (NULL_PTR_CAST(ccs::types::char8*) != buffer);
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("GetTypeNameFromJSONContent - Try and parse '%s'", buffer);
    }
#endif
  if (status)
    {
      ccs::types::string type;

      status = CopyJSONObjectElement(buffer, "type", type, ccs::types::MaxStringLength);

      if (status) // Type name includes '"'
        {
          status = ccs::HelperTools::Strip(type, "\"");
          (void)SafeStringCopy(name, type, size);
        }
    }
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("GetTypeNameFromJSONContent - Found type name '%s'", name);
    }
#endif
  return status;
  
}

template <> inline bool Is<ccs::types::ArrayType> (const ccs::types::char8 * const buffer) 
{ 

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("Is<ccs::types::ArrayType> - Try and parse '%s'", buffer);
    }
#endif
  // Should be supporting the following cases:
  //   Named types '{"type":"MyArrayType","multiplicity":4,"element":{"type":..}}'
  //   Named types with out-of-order type name '{"multiplicity":4,"element":{"type":..},"type":"MyArrayType"}' .. need to count braces.
  //   Unnamed types '{"multiplicity":4,"element":{"type":..}}'
  //   Already registered types '{"type":"MyArrayType"}'
  //   Out-of-order declaration '{"type":"MyArrayType","element":{"type":..},"multiplicity":4}'
  //   Indented declaration '{"type":"MyArrayType",\n
  //                          "element":{"type":..},\n
  //                          "multiplicity":4\n
  //                         }'
  //   First of a list of types ':{"type":"MyArrayType","multiplicity":4,"element":{"type":..}},"attribute":{"type":..},..'
  //
  // Let's not consider the cases:
  //
  // May or may not contain a '"type":"name"' attribute.
  // The type 'name' may or may not be registered in the GlobalTypeDatabase.

  if (status)
    { // Extract type name
      ccs::types::string name = STRING_UNDEFINED; 

      status = GetTypeNameFromJSONContent(buffer, name, ccs::types::MaxStringLength);

      if (status)
        {
          status = ccs::base::GlobalTypeDatabase::IsValid(name);
        }

      if (status)
        {
          log_debug("Is<ccs::types::ArrayType> - Name '%s' is registered .. return", name);
          return Is<ccs::types::ArrayType>(ccs::base::GlobalTypeDatabase::GetType(name));
        }
      else
        {
          log_debug("Is<ccs::types::ArrayType> - Name '%s' is not registered .. continue", name);
          status = true;
        }
    }

  // Reaching here, we may have or not a 'type' name but we are sure the type is not registered ..
  // .. test the rest of the type specification 

  const ccs::types::char8 * p_buf = buffer;

  if (status)
    {
      while ((*p_buf != 0) && (*p_buf != '{')) { p_buf++; }
      status = (*p_buf == '{');
    }

  if (status) // p_buf points to '{'
    { // Must have 'element' and 'multiplicity'
      status = ((true == ccs::HelperTools::HasJSONObjectElement(p_buf, "element")) &&
                (true == ccs::HelperTools::HasJSONObjectElement(p_buf, "multiplicity")));
    }
  
  return status;

}

template <> inline bool Is<ccs::types::CompoundType> (const ccs::types::char8 * const buffer) 
{ 

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("Is<ccs::types::CompoundType> - Try and parse '%s'", buffer);
    }
#endif
  if (status)
    { // Extract type name
      ccs::types::string name = STRING_UNDEFINED; 

      status = GetTypeNameFromJSONContent(buffer, name, ccs::types::MaxStringLength);

      if (status)
        {
          status = ccs::base::GlobalTypeDatabase::IsValid(name);
        }

      if (status)
        {
          log_debug("Is<ccs::types::CompoundType> - Name '%s' is registered .. return", name);
          return Is<ccs::types::CompoundType>(ccs::base::GlobalTypeDatabase::GetType(name));
        }
      else
        {
          log_debug("Is<ccs::types::CompoundType> - Name '%s' is not registered .. continue", name);
          status = true;
        }
    }

  // Reaching here, we may have or not a 'type' name but we are sure the type is not registered ..
  // .. test the rest of the type specification 

  const ccs::types::char8 * p_buf = buffer;

  if (status)
    {
      while ((*p_buf != 0) && (*p_buf != '{')) { p_buf++; }
      status = (*p_buf == '{');
    }

  if (status) // p_buf points to '{'
    { // Must have 'attributes'
      status = (true == ccs::HelperTools::HasJSONObjectElement(p_buf, "attributes"));
    }

  return status;

}

template <> inline bool Is<ccs::types::ScalarType> (const ccs::types::char8 * const buffer) 
{ 

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("Is<ccs::types::ScalarType> - Try and parse '%s'", buffer);
    }
#endif
  if (status)
    { // Extract type name
      ccs::types::string name = STRING_UNDEFINED; 

      status = GetTypeNameFromJSONContent(buffer, name, ccs::types::MaxStringLength);

      if (status)
        {
          status = ccs::base::GlobalTypeDatabase::IsValid(name);
        }

      if (status)
        {
          log_debug("Is<ccs::types::ScalarType> - Name '%s' is registered .. return", name);
          return Is<ccs::types::ScalarType>(ccs::base::GlobalTypeDatabase::GetType(name));
        }
      else
        {
          log_debug("Is<ccs::types::ScalarType> - Name '%s' is not registered .. continue", name);
          status = true;
        }
    }

  // Reaching here, we may have or not a 'type' name but we are sure the type is not registered ..
  // .. test the rest of the type specification 

  const ccs::types::char8 * p_buf = buffer;

  if (status)
    {
      while ((*p_buf != 0) && (*p_buf != '{')) { p_buf++; }
      status = (*p_buf == '{');
    }

  if (status) // p_buf points to '{'
    { // Must have 'size'
      status = ((true == ccs::HelperTools::Contain(p_buf, "size")) &&
                (ccs::HelperTools::Find(p_buf, "size") < ccs::HelperTools::Find(p_buf, "}")));
    }

  return status;

}

static inline bool IsArrayType (const ccs::base::SharedReference<const ccs::types::AnyType>& type) { return Is<ccs::types::ArrayType>(type); }
static inline bool IsCompoundType (const ccs::base::SharedReference<const ccs::types::AnyType>& type) { return Is<ccs::types::CompoundType>(type); }
static inline bool IsScalarType (const ccs::base::SharedReference<const ccs::types::AnyType>& type) { return Is<ccs::types::ScalarType>(type); }

static inline bool IsCompoundArray (const ccs::base::SharedReference<const ccs::types::AnyType>& type) 
{ 

  bool status = Is<ccs::types::ArrayType>(type); 

  if (status)
    {
      const ccs::base::SharedReference<const ccs::types::ArrayType> __type (type);
      status = Is<ccs::types::CompoundType>(__type->GetElementType());
    }

  return status; 

}

static inline bool IsScalarArray (const ccs::base::SharedReference<const ccs::types::AnyType>& type)
{ 

  bool status = Is<ccs::types::ArrayType>(type); 

  if (status)
    {
      const ccs::base::SharedReference<const ccs::types::ArrayType> __type (type);
      status = Is<ccs::types::ScalarType>(__type->GetElementType());
    }

  return status; 

}

static inline ccs::types::ArrayType* NewArrayType (const ccs::types::char8 * const type, const ccs::types::char8 * const base, const ccs::types::uint32 multiplicity) // Registered name
{

  ccs::types::ArrayType* ref = NULL_PTR_CAST(ccs::types::ArrayType*);

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != base) &&
                 (true == ccs::base::GlobalTypeDatabase::IsValid(base)));

  if (status)
    {
      ref = NewArrayType(type, ccs::base::GlobalTypeDatabase::GetType(base), multiplicity);
    }

  return ref;

}

static inline ccs::types::ArrayType* NewArrayType (const ccs::types::char8 * const type, const ccs::base::SharedReference<const ccs::types::AnyType>& base, const ccs::types::uint32 multiplicity)
{

  ccs::types::ArrayType* ref = NULL_PTR_CAST(ccs::types::ArrayType*);

  bool status = static_cast<bool>(base);

  if (status)
    {
      ref = new (std::nothrow) ccs::types::ArrayType (type, base, multiplicity);
    }

  return ref;

}

static inline ccs::types::ArrayType* NewArrayType (const ccs::types::char8 * const type, const ccs::types::AnyType * const base, const ccs::types::uint32 multiplicity) { return NewArrayType(type, ccs::base::SharedReference<const ccs::types::AnyType>(base), multiplicity); }
static inline ccs::types::ArrayType* NewArrayType (const ccs::types::char8 * const type, const ccs::types::AnyType& base, const ccs::types::uint32 multiplicity) { return NewArrayType(type, &base, multiplicity); }

static bool HasAttribute (const ccs::types::AnyType * const type, const ccs::types::char8 * const name)
{ 

  log_trace("HasAttribute('%s') - Entering routine", name);

  bool status = ((NULL_PTR_CAST(const ccs::types::AnyType*) != type) &&
                 (NULL_PTR_CAST(const ccs::types::char8*) != name)); // Empty string returns 'true' .. but NULL returns 'false'

  if (status && (false == ccs::HelperTools::IsUndefinedString(name)))
    {

      // The method should eventually work with nested attributes
      // It should reply true with attribute name 'a.b.c' or 'a[0].b' or even 'a[0]' if any such path is valid, 
      // e.g. HasAttribute('a') == true && Is<ArrayType>(GetAtributeType('a')) == true
      
      ccs::types::string buffer = STRING_UNDEFINED;
      
      ccs::types::char8* p_next = NULL_PTR_CAST(ccs::types::char8*);
      ccs::types::char8* p_curr = ccs::HelperTools::SafeStringCopy(buffer, name, ccs::types::MaxStringLength);
      ccs::types::char8* p_first = strtok_r(p_curr, ".[]", &p_next);
      
      if ((p_next == NULL_PTR_CAST(ccs::types::char8*)) || (*p_next == 0)) // Token without separator .. End of recursion
        {
          log_debug("HasAttribute('%s') - No separator found ..", name);
#ifndef INHERIT_FROM_COMPOUND_TYPE          
          if (true == ccs::HelperTools::IsIntegerString(p_first)) // An integer token .. Test element within array
            {
              const ccs::types::ArrayType* __type = dynamic_cast<const ccs::types::ArrayType*>(type);

              status = (Is<ccs::types::ArrayType>(type) && __type->HasElement(ccs::HelperTools::ToInteger(p_first)));
#ifdef LOG_DEBUG_ENABLE
              if (status)
                {
                  log_debug("HasAttribute('%s') - Success", name);
                }
              else
                {
                  log_debug("HasAttribute('%s') - Error", name);
                }
#endif
            }
          else // A non-integer token
            {
              const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

              status = (Is<ccs::types::CompoundType>(type) && __type->HasAttribute(p_first));
#ifdef LOG_DEBUG_ENABLE
              if (status)
                {
                  log_debug("HasAttribute('%s') - Success", name);
                }
              else
                {
                  log_debug("HasAttribute('%s') - Error", name);
                }
#endif
            }
#else
          const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);
          status = (Is<ccs::types::CompoundType>(type) && __type->HasAttribute(p_first));
#endif
        }
      else // Token with a remainder .. Further recursion
        {
          log_debug("HasAttribute('%s') - Token '%s' found (rest '%s') ..", name, p_first, p_next);
#ifndef INHERIT_FROM_COMPOUND_TYPE          
          if (true == ccs::HelperTools::IsIntegerString(p_first)) // An integer token .. Test element within array
            {
              const ccs::types::ArrayType* __type = dynamic_cast<const ccs::types::ArrayType*>(type);
              
              status = (Is<ccs::types::ArrayType>(type) && __type->HasElement(ccs::HelperTools::ToInteger(p_first)));
              
              if (status)
                {
                  log_debug("HasAttribute('%s') - Recursion on ArrayType ..", name);
                  
                  // Recursion
                  status = HasAttribute(__type->GetElementType(), p_next); 
                }
            }
          else // A non-integer token
            {
              const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

              status = (Is<ccs::types::CompoundType>(type) && __type->HasAttribute(p_first));
              
              if (status)
                {
                  log_debug("HasAttribute('%s') - Recursion on CompoundType ..", name);
                  
                  // Recursion
                  status = HasAttribute(__type->GetAttributeType(p_first), p_next); 
                }
            }
#else
          const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);
          
          status = (Is<ccs::types::CompoundType>(type) && __type->HasAttribute(p_first));
          
          if (status)
            {
              log_debug("HasAttribute('%s') - Recursion on CompoundType ..", name);
              
              // Recursion
              status = HasAttribute(__type->GetAttributeType(p_first), p_next); 
            }
#endif
        }
    }      

  log_trace("HasAttribute('%s') - Leaving routine", name);

  return status; 

}

static bool HasAttribute (const ccs::base::SharedReference<const ccs::types::AnyType>& type, const ccs::types::char8 * const name)
{ 

  log_trace("HasAttribute('%s') - Entering routine", name);

  bool status = static_cast<bool>(type);

  if (status)
    {
      status = HasAttribute(type.GetReference(), name);
    }      

  log_trace("HasAttribute('%s') - Leaving routine", name);

  return status; 

}

static ccs::types::uint32 GetAttributeOffset (const ccs::types::AnyType * const type, const ccs::types::char8 * const name)
{ 

  log_trace("GetAttributeOffset('%s') - Entering routine", name);

  ccs::types::uint32 offset = 0u;

  bool status = HasAttribute(type, name); // Tests both for type and name validity

  if (status && (false == ccs::HelperTools::IsUndefinedString(name)))
    {
      ccs::types::string buffer = STRING_UNDEFINED; 
  
      ccs::types::char8* p_next = NULL_PTR_CAST(ccs::types::char8*);
      ccs::types::char8* p_curr = ccs::HelperTools::SafeStringCopy(buffer, name, ccs::types::MaxStringLength);
      ccs::types::char8* p_first = strtok_r(p_curr, ".[]", &p_next);

      if ((p_next == NULL_PTR_CAST(ccs::types::char8*)) || (*p_next == 0)) // Token without separator .. End of recursion
        {
          log_debug("GetAttributeOffset('%s') - No separator found ..", name);
#ifndef INHERIT_FROM_COMPOUND_TYPE          
          if (true == ccs::HelperTools::IsIntegerString(p_first)) // An integer token .. Locate element within array
            {
              const ccs::types::ArrayType* __type = dynamic_cast<const ccs::types::ArrayType*>(type);

              if (NULL_PTR_CAST(const ccs::types::ArrayType*) != __type)
                {
                  offset = __type->GetElementOffset(ccs::HelperTools::ToInteger(p_first));
                }
            }
          else // A non-integer token
            {
              const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

              if (NULL_PTR_CAST(const ccs::types::CompoundType*) != __type)
                {
                  offset = __type->GetAttributeOffset(p_first);
                }
            }
#else
          const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

          if (NULL_PTR_CAST(const ccs::types::CompoundType*) != __type)
            {
              offset = __type->GetAttributeOffset(p_first);
            }
#endif
        }
      else // Token with a remainder .. Further recursion
        {
          log_debug("GetAttributeOffset('%s') - Token '%s' found (rest '%s') ..", name, p_first, p_next);
#ifndef INHERIT_FROM_COMPOUND_TYPE          
          if (true == ccs::HelperTools::IsIntegerString(p_first)) // An integer token .. Locate element within array
            {
              log_debug("GetAttributeOffset('%s') - Recursion on ArrayType ..", name);
                  
              const ccs::types::ArrayType* __type = dynamic_cast<const ccs::types::ArrayType*>(type);

              if (NULL_PTR_CAST(const ccs::types::ArrayType*) != __type)
                {
                  offset = __type->GetElementOffset(ccs::HelperTools::ToInteger(p_first)) 
                    + GetAttributeOffset(__type->GetElementType(), p_next); 
                }
            }
          else // A non-integer token
            {
              log_debug("GetAttributeOffset('%s') - Recursion on CompoundType ..", name);
                  
              const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

              if (NULL_PTR_CAST(const ccs::types::CompoundType*) != __type)
                {
                  offset = __type->GetAttributeOffset(p_first) 
                    + GetAttributeOffset(__type->GetAttributeType(p_first), p_next);
                }
            }
#else
          const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

          if (NULL_PTR_CAST(const ccs::types::CompoundType*) != __type)
            {
              offset = __type->GetAttributeOffset(p_first) 
                + GetAttributeOffset(__type->GetAttributeType(p_first), p_next);
            }
#endif
        }
    }

  log_trace("GetAttributeOffset('%s') - Leaving routine", name);

  return offset;

}

static ccs::types::uint32 GetAttributeOffset (const ccs::base::SharedReference<const ccs::types::AnyType>& type, const ccs::types::char8 * const name)
{
 
  ccs::types::uint32 offset = GetAttributeOffset(type.GetReference(), name);

  return offset;

}

static inline ccs::types::uint32 GetAttributeMultiplicity (const ccs::types::AnyType * const type, const ccs::types::char8 * const name) 
{ 

  ccs::types::uint32 mult = 0u;

  bool status = HasAttribute(type, name);

  if (status)
    {
      mult = 1u;

      if (Is<ccs::types::ArrayType>(GetAttributeType(type, name)))
        {
          ccs::base::SharedReference<const ccs::types::ArrayType> attr_type = GetAttributeType(type, name);
          mult = attr_type->GetMultiplicity(); 
        }
    }

  return mult;

}

static inline ccs::types::uint32 GetAttributeMultiplicity (const ccs::base::SharedReference<const ccs::types::AnyType>& type, const ccs::types::char8 * const name) 
{ 

  ccs::types::uint32 mult = GetAttributeMultiplicity(type.GetReference(), name);

  return mult;

}

static inline ccs::types::uint32 GetAttributeSize (const ccs::types::AnyType * const type, const ccs::types::char8 * const name) 
{ 

  ccs::types::uint32 size = 0u;

  bool status = HasAttribute(type, name);

  if (status)
    {
      ccs::base::SharedReference<const ccs::types::AnyType> attr_type = GetAttributeType(type, name);
      size = attr_type->GetSize(); 
    }

  return size;

}

static inline ccs::types::uint32 GetAttributeSize (const ccs::base::SharedReference<const ccs::types::AnyType>& type, const ccs::types::char8 * const name) 
{ 

  ccs::types::uint32 size = GetAttributeSize(type.GetReference(), name);

  return size;

}

static ccs::base::SharedReference<const ccs::types::AnyType> GetAttributeType (const ccs::types::AnyType * const type, const ccs::types::char8 * const name) 
{ 

  log_trace("GetAttributeType('%s') - Entering routine", name);

  ccs::base::SharedReference<const ccs::types::AnyType> ret;

  bool status = HasAttribute(type, name); // Tests both for type and name validity

  if (status && (true == ccs::HelperTools::IsUndefinedString(name))) // '' is a valid name
    {
      ret = type; // Return this type
    }

  if (status && (false == ccs::HelperTools::IsUndefinedString(name))) // Called with a valid non-empty name
    {
      ccs::types::string buffer = STRING_UNDEFINED; 
  
      ccs::types::char8* p_next = NULL_PTR_CAST(ccs::types::char8*);
      ccs::types::char8* p_curr = ccs::HelperTools::SafeStringCopy(buffer, name, ccs::types::MaxStringLength);
      ccs::types::char8* p_first = strtok_r(p_curr, ".[]", &p_next);

      if ((p_next == NULL_PTR_CAST(ccs::types::char8*)) || (*p_next == 0)) // Token without separator .. End of recursion
        {
          log_debug("GetAttributeType('%s') - No separator found ..", name);
#ifndef INHERIT_FROM_COMPOUND_TYPE          
          if (true == ccs::HelperTools::IsIntegerString(p_first)) // An integer token .. Return element type
            {
              const ccs::types::ArrayType* __type = dynamic_cast<const ccs::types::ArrayType*>(type);

              if (NULL_PTR_CAST(const ccs::types::ArrayType*) != __type)
                {
                  ret = __type->GetElementType();
                }
            }
          else // A non-integer token
            {
              const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

              if (NULL_PTR_CAST(const ccs::types::CompoundType*) != __type)
                {
                  ret = __type->GetAttributeType(p_first);
                }
            }
#else
          const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

          if (NULL_PTR_CAST(const ccs::types::CompoundType*) != __type)
            {
              ret = __type->GetAttributeType(p_first);
            }
#endif
        }
      else // Token with a remainder .. Further recursion
        {
          log_debug("GetAttributeType('%s') - Token '%s' found (rest '%s') ..", name, p_first, p_next);
#ifndef INHERIT_FROM_COMPOUND_TYPE          
          if (true == ccs::HelperTools::IsIntegerString(p_first)) // An integer token .. Proceed with element type
            {
              log_debug("GetAttributeType('%s') - Recursion on ArrayType ..", name);
                  
              const ccs::types::ArrayType* __type = dynamic_cast<const ccs::types::ArrayType*>(type);

              if (NULL_PTR_CAST(const ccs::types::ArrayType*) != __type)
                {
                  ret = GetAttributeType(__type->GetElementType(), p_next);
                } 
            }
          else // A non-integer token
            {
              log_debug("GetAttributeType('%s') - Recursion on CompoundType ..", name);
                  
              const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

              if (NULL_PTR_CAST(const ccs::types::CompoundType*) != __type)
                {
                  ret = GetAttributeType(__type->GetAttributeType(p_first), p_next); 
                }
            }
#else
          const ccs::types::CompoundType* __type = dynamic_cast<const ccs::types::CompoundType*>(type);

          if (NULL_PTR_CAST(const ccs::types::CompoundType*) != __type)
            {
              ret = GetAttributeType(__type->GetAttributeType(p_first), p_next); 
            }
#endif
        }
    }

  log_trace("GetAttributeType('%s') - Leaving routine", name);

  return ret;

}

static ccs::base::SharedReference<const ccs::types::AnyType> GetAttributeType (const ccs::base::SharedReference<const ccs::types::AnyType>& type, const ccs::types::char8 * const name) 
{ 

  log_trace("GetAttributeType('%s') - Entering routine", name);

  ccs::base::SharedReference<const ccs::types::AnyType> ret;

  bool status = HasAttribute(type, name); // Tests both for type and name validity

  if (status && (true == ccs::HelperTools::IsUndefinedString(name))) // '' is a valid name
    {
      ret = type; // Return this type
    }

  if (status && (false == ccs::HelperTools::IsUndefinedString(name))) // Called with a valid non-empty name
    {
      ret = GetAttributeType(type.GetReference(), name);
    }

  log_trace("GetAttributeType('%s') - Leaving routine", name);

  return ret;

}

static bool RegisterDependencies (const ccs::types::char8 * const buffer)
{

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);

  if (status)
    {
      ccs::types::uint32 start = 0u;
      ccs::types::uint32 close = 0u;

      if (ccs::HelperTools::HasJSONObjectElement(buffer, "dependencies", start, close))
        {
          std::string line = buffer;
          line = line.substr(start, close - start + 1u);
          log_debug("RegisterDependencies - '%s'", line.c_str());
          ccs::base::GlobalTypeDatabase::TryAndRegister(line.c_str());
        }
    }

  return status;

}

static ccs::types::uint32 Parse (ccs::base::SharedReference<ccs::types::AnyType>& type, const ccs::types::char8 * const buffer)
{

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("Parse<AnyType> - Try and parse '%s' ..", buffer);
    }
#endif
#if 0
  if (status && static_cast<bool>(type))
    {
      type.Discard();
      status = !static_cast<bool>(type);
    }
#else
  // Enforce empty reference
  type.Discard();
#endif
  ccs::types::uint32 ret = 0u;

  if (status)
    {
      if (ccs::HelperTools::Is<ccs::types::ArrayType>(buffer))
        {
          log_debug("Parse<AnyType> - .. Is<ArrayType> ..");

          ccs::base::SharedReference<ccs::types::ArrayType> __type (new (std::nothrow) ccs::types::ArrayType);
          ret = Parse(__type, buffer);

          if (ret > 0u)
            {
              log_debug("Parse<AnyType> - .. consumed '%u' characters", ret);
              type = __type;
            }
        }
      else if (ccs::HelperTools::Is<ccs::types::CompoundType>(buffer))
        {
          log_debug("Parse<AnyType> - .. Is<CompoundType> ..");

          ccs::base::SharedReference<ccs::types::CompoundType> __type (new (std::nothrow) ccs::types::CompoundType);
          ret = Parse(__type, buffer);

          if (ret > 0u)
            {
              log_debug("Parse<AnyType> - .. consumed '%u' characters", ret);
              type = __type;
            }
        }
      else if (ccs::HelperTools::Is<ccs::types::ScalarType>(buffer))
        {
          log_debug("Parse<AnyType> - .. Is<ScalarType> ..");

          ccs::base::SharedReference<ccs::types::ScalarType> __type;
          ret = Parse(__type, buffer);

          if (ret > 0u)
            {
              log_debug("Parse<AnyType> - .. consumed '%u' characters", ret);
              type = __type;
            }
        }
      else
        {
          log_error("Parse<AnyType> - Unable to parse '%s'", buffer);
        }

      status = (ret > 0u); // Unused
    }
#ifdef LOG_DEBUG_ENABLE
  if (!status)
    {
      log_error("Parse<AnyType> - .. failure");
    }
#endif
  log_debug("Parse<AnyType> - Returning '%u'", ret);

  return ret;

}
  
static ccs::types::uint32 Parse (ccs::base::SharedReference<ccs::types::ArrayType>& type, const ccs::types::char8 * const buffer)
{

  bool status = (static_cast<bool>(type) && 
                 (NULL_PTR_CAST(const ccs::types::char8*) != buffer));
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("Parse<ArrayType> - Try and parse '%s'", buffer);
    }
#endif
  ccs::types::uint32 ret = 0u;

  if (status)
    {
      // Assumptions and contraints
      //   - Function called with a 'valid' JSON format, i.e. true == Is<ArrayType>(buffer)
      //   implies that the type 'name' is registered as an ArrayType or the type specification
      //   includes element 'type' and multiplicity.

      ret = static_cast<ccs::types::uint32>(ccs::HelperTools::FindMatchingBrace(buffer));
      ret++;

      std::string line;

      // Copy type specification till matching brace
      line = std::string(buffer, ret);
      log_debug("Parse<ArrayType> - Try and parse '%s'", line.c_str());

      // Try and register dependencies
      (void)RegisterDependencies(line.c_str());

      ccs::types::string name = STRING_UNDEFINED;
      ccs::types::char8 elem [1024] = STRING_UNDEFINED;
      ccs::types::string mult = STRING_UNDEFINED;

      // ToDo - Replace element copy with parse at offset

      (void)GetTypeNameFromJSONContent(line.c_str(), name, ccs::types::MaxStringLength);
      (void)ccs::HelperTools::CopyJSONObjectElement(line.c_str(), "element", elem, 1024u);
      (void)ccs::HelperTools::CopyJSONObjectElement(line.c_str(), "multiplicity", mult, ccs::types::MaxStringLength);
#ifdef LOG_DEBUG_ENABLE
      if (!ccs::HelperTools::IsUndefinedString(name))
        {
          log_debug("Parse<ArrayType> - Found type '%s'", name);
        }
      if (!ccs::HelperTools::IsUndefinedString(elem))
        {
          log_debug("Parse<ArrayType> - Found element '%s'", elem);
        }
      if (!ccs::HelperTools::IsUndefinedString(mult))
        {
          log_debug("Parse<ArrayType> - Found multiplicity '%s'", mult);
        }
#endif
      if (true == ccs::base::GlobalTypeDatabase::IsValid(name))
        {
          log_debug("Parse<ArrayType> - Registered type '%s' ..", name);
          //type = std::const_pointer_cast<ccs::types::ArrayType>(std::dynamic_pointer_cast<const ccs::types::ArrayType>(ccs::base::GlobalTypeDatabase::GetType(name)));
          ccs::base::SharedReference<const ccs::types::ArrayType> __type = ccs::base::GlobalTypeDatabase::GetType(name);
          type = ccs::base::SharedReference<ccs::types::ArrayType>(const_cast<ccs::types::ArrayType*>(__type.GetReference()), __type.GetCounter());
        }
      else if (!ccs::HelperTools::IsUndefinedString(elem) && !ccs::HelperTools::IsUndefinedString(mult))
        {
          log_debug("Parse<ArrayType> - Not registered type '%s' ..", name);

          ccs::base::SharedReference<ccs::types::AnyType> __type;

          status = (0u < Parse(__type, elem));

          if (status)
            {
              (void)type->SetName(name);
              (void)type->SetElementType(__type);
              (void)type->SetMultiplicity(static_cast<ccs::types::uint32>(ccs::HelperTools::ToInteger(mult)));
            }
        }
    }

  if (!status)
    {
      ret = 0u;
    }

  log_debug("Parse<ArrayType> - Returning '%u'", ret);

  return ret;

}
  
static ccs::types::uint32 Parse (ccs::base::SharedReference<ccs::types::CompoundType>& type, const ccs::types::char8 * const buffer)
{

  bool status = (static_cast<bool>(type) && 
                 (NULL_PTR_CAST(const ccs::types::char8*) != buffer));
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("Parse<CompoundType> - Try and parse '%s'", buffer);
    }
#endif
  ccs::types::uint32 ret = 0u;

  if (status)
    {
      // Assumptions and contraints
      //   - Function called with a 'valid' JSON format, i.e. true == Is<CompoundType>(buffer)
      //   implies that the type 'name' is registered as a CompoundType or the type specification
      //   includes attributes list.

      ret = static_cast<ccs::types::uint32>(ccs::HelperTools::FindMatchingBrace(buffer));
      ret++;

      log_debug("Parse<CompoundType> - Try and parse '%s'", buffer);

      // Try and register dependencies
      (void)RegisterDependencies(buffer);

      ccs::types::string name = STRING_UNDEFINED;
      (void)GetTypeNameFromJSONContent(buffer, name, ccs::types::MaxStringLength);

      if (true == ccs::base::GlobalTypeDatabase::IsValid(name))
        {
          log_debug("Parse<CompoundType> - Registered type '%s' ..", name);
          
          ccs::base::SharedReference<const ccs::types::CompoundType> __type = ccs::base::GlobalTypeDatabase::GetType(name);
          type = ccs::base::SharedReference<ccs::types::CompoundType>(const_cast<ccs::types::CompoundType*>(__type.GetReference()), __type.GetCounter());
        }
      else
        {
          log_debug("Parse<CompoundType> - Not registered type '%s' ..", name);
          
          (void)type->SetName(name);
          
          ccs::types::uint32 start = 0u;
          ccs::types::uint32 close = 0u;
          
          (void)ccs::HelperTools::HasJSONObjectElement(buffer, "attributes", start, close);
          ccs::types::uint32 offst = start; // Starting offset where array starts
          
          for (; ccs::HelperTools::FindJSONArrayElement(buffer, start, close, offst);)
            {
              // Align to newly found element
              offst = start;

              // Outside indexes are for looping through name-value pairs
              ccs::types::uint32 __start = 0u;
              ccs::types::uint32 __close = 0u;
              
              ccs::types::string attr;
              (void)CopyJSONObjectElementName(buffer, attr, ccs::types::MaxStringLength, offst);
              (void)FindJSONObjectElementValue(buffer, __start, __close, offst);
              
              ccs::base::SharedReference<ccs::types::AnyType> __type;
              
              log_debug("Parse<CompoundType> - Value at '%d' for '%s' at offset '%d' in '%s' ..", __start, attr, offst, buffer);

              status = (0u < Parse(__type, buffer + __start));

              if (status)
                {
                  log_debug("Parse<CompoundType> - AddAttribute('%s') ..", attr);
                  (void)type->AddAttribute(attr, __type);
                }
              
              // Prepare for next element
              offst = close + 1u;
            }
        }
    }

  if (!status)
    {
      ret = 0u;
    }

  log_debug("info<CompoundType> - Returning '%u'", ret);

  return ret;

}

static ccs::types::uint32 Parse (ccs::base::SharedReference<ccs::types::ScalarType>& type, const ccs::types::char8 * const buffer)
{

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("Parse<ScalarType> - Try and parse '%s'", buffer);
    }
#endif
  ccs::types::uint32 ret = 0u;

  if (status)
    {
      // Assumptions and contraints
      //   - Function called with a 'valid' JSON format, i.e. true == Is<ScalarType>(buffer)
      //   implies that the type 'name' is registered as an ArrayType or the type specification
      //   includes element 'type' and multiplicity.

      ret = static_cast<ccs::types::uint32>(ccs::HelperTools::FindMatchingBrace(buffer));
      ret++;
    }

  std::string line;

  if (status)
    {
      // Copy type specification till matching brace
      line = std::string(buffer, ret);
      log_debug("Parse<ScalarType> - Try and parse '%s'", line.c_str());
    }

  if (status)
    {
      ccs::types::string name = STRING_UNDEFINED;
      status = GetTypeNameFromJSONContent(line.c_str(), name, ccs::types::MaxStringLength);

      if (status)
        {
          if (true == ccs::base::GlobalTypeDatabase::IsValid(name))
            {
              log_debug("Parse<ScalarType> - Registered type '%s' ..", name);
              //type = std::const_pointer_cast<ccs::types::ScalarType>(std::dynamic_pointer_cast<const ccs::types::ScalarType>(ccs::base::GlobalTypeDatabase::GetType(name)));
              ccs::base::SharedReference<const ccs::types::ScalarType> __type = ccs::base::GlobalTypeDatabase::GetType(name);
              type = ccs::base::SharedReference<ccs::types::ScalarType>(const_cast<ccs::types::ScalarType*>(__type.GetReference()), __type.GetCounter());
            }
          else
            {
              status = false;
            }
        }
    }

  if (!status)
    {
      ret = 0u;
    }

  log_debug("Parse<ScalarType> - Returning '%u'", ret);

  return ret;

}
  
static bool CollectDependencies (const ccs::base::SharedReference<const ccs::types::ArrayType>& type, ccs::base::AnyTypeDatabase& deps);
static bool CollectDependencies (const ccs::base::SharedReference<const ccs::types::CompoundType>& type, ccs::base::AnyTypeDatabase& deps);
//static bool CollectDependencies (const ccs::base::SharedReference<const ccs::types::ScalarType>& type, ccs::base::AnyTypeDatabase& deps);

static bool CollectDependencies (const ccs::base::SharedReference<const ccs::types::AnyType>& type, ccs::base::AnyTypeDatabase& deps)
{

  bool status = static_cast<bool>(type);

  if (status)
    {
      if (Is<ccs::types::ArrayType>(type))
        {
          const ccs::base::SharedReference<const ccs::types::ArrayType> __type = type;
          status = CollectDependencies(__type, deps);
        }
      else if (Is<ccs::types::CompoundType>(type))
        {
          const ccs::base::SharedReference<const ccs::types::CompoundType> __type = type;
          status = CollectDependencies(__type, deps);
        }
#if 0
      else if (Is<ccs::types::ScalarType>(type))
        {
          const ccs::base::SharedReference<const ccs::types::ScalarType> __type = type;
          status = CollectDependencies(__type, deps);
        }
      else
        {
          status = false;
        }
#endif
    }

  return status;

}

static bool CollectDependencies (const ccs::base::SharedReference<const ccs::types::ArrayType>& type, ccs::base::AnyTypeDatabase& deps)
{

  bool status = static_cast<bool>(type);

  if (status)
    {
      ccs::base::SharedReference<const ccs::types::AnyType> __type = type->GetElementType();
      (void)CollectDependencies(__type, deps);

      const ccs::types::char8 * type_name = __type->GetName();

      if ((false == Is<ccs::types::ScalarType>(__type)) && (false == deps.IsValid(type_name)))
        {
          status = deps.Register(type_name, __type);
        }
    }

  return status;

}

static bool CollectDependencies (const ccs::base::SharedReference<const ccs::types::CompoundType>& type, ccs::base::AnyTypeDatabase& deps)
{

  bool status = static_cast<bool>(type);

  for (ccs::types::uint32 index = 0u; ((index < type->GetAttributeNumber()) && status); index++)
    {
      ccs::base::SharedReference<const ccs::types::AnyType> __type = type->GetAttributeType(index);
      (void)CollectDependencies(__type, deps);

      const ccs::types::char8 * type_name = __type->GetName();

      if ((false == Is<ccs::types::ScalarType>(__type)) && (false == deps.IsValid(type_name)))
        {
          status = deps.Register(type_name, __type);
        }
    }

  return status;

}
#if 0
static bool CollectDependencies (const ccs::base::SharedReference<const ccs::types::ScalarType>& type, ccs::base::AnyTypeDatabase& deps)
{

  bool status = static_cast<bool>(type);

  return status;

}
#endif
static bool SerialiseDependencies (const ccs::base::AnyTypeDatabase& deps, ccs::types::char8 * const buffer, ccs::types::uint32 size)
{

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);

  ccs::types::char8* p_buf = buffer;

  if (status && (0u != deps.GetSize()))
    {
      // Schema v1.1 .. incl. dependencies
      (void)ccs::HelperTools::SafeStringCopy(p_buf, "\"schema\":\"v1.1\",\"dependencies\":[", size); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 

      for (ccs::types::uint32 index = 0u; ((index < deps.GetSize()) && status); index++)
        {
          status = Serialise(deps.GetElement(index), p_buf, size, const_cast<ccs::base::AnyTypeDatabase*>(&deps)); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer
          (void)ccs::HelperTools::SafeStringCopy(p_buf, ",", size); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
        }

      // Terminate buffer - remove final comma character 
      p_buf--; size++;
      (void)ccs::HelperTools::SafeStringCopy(p_buf, "],", size); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
    }

  return status;

}

static bool Serialise (const ccs::base::SharedReference<const ccs::types::AnyType>& type, ccs::types::char8 * const buffer, const ccs::types::uint32 size, ccs::base::AnyTypeDatabase * deps)
{

  bool status = (static_cast<bool>(type) &&
                 (NULL_PTR_CAST(const ccs::types::char8*) != buffer));

  if (status)
    {
      if (Is<ccs::types::ArrayType>(type))
        {
          const ccs::base::SharedReference<const ccs::types::ArrayType> __type = type;
          status = Serialise(__type, buffer, size, deps);
        }
      else if (Is<ccs::types::CompoundType>(type))
        {
          const ccs::base::SharedReference<const ccs::types::CompoundType> __type = type;
          status = Serialise(__type, buffer, size, deps);
        }
      else if (Is<ccs::types::ScalarType>(type))
        {
          const ccs::base::SharedReference<const ccs::types::ScalarType> __type = type;
          status = Serialise(__type, buffer, size, deps);
        }
      else
        {
          status = false;
        }
    }

  return status;

}

static bool Serialise (const ccs::base::SharedReference<const ccs::types::ArrayType>& type, ccs::types::char8 * const buffer, ccs::types::uint32 size, ccs::base::AnyTypeDatabase * deps)
{

  bool status = (static_cast<bool>(type) &&
                 (NULL_PTR_CAST(const ccs::types::char8*) != buffer));

  ccs::types::char8* p_buf = buffer;

  bool allocate = (NULL_PTR_CAST(ccs::base::AnyTypeDatabase*) == deps);

  if (allocate)
    {
      deps = new (std::nothrow) ccs::base::AnyTypeDatabase ();
      status = (NULL_PTR_CAST(ccs::base::AnyTypeDatabase*) != deps);
    }

  if (status)
    {
      //(void)snprintf(p_buf, size, "{\"type\":\"%s\",\"size\":%u,", type->GetName(), type->GetSize()); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
      (void)snprintf(p_buf, size, "{\"type\":\"%s\",", type->GetName()); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
    }

  if (status && allocate)
    {
      // Schema v1.1 .. incl. dependencies
      log_debug("Serialise<ArrayType>('%s') - Collect dependencies ..", type->GetName());

      // Try and list type dependencies
      status = CollectDependencies(type, *deps); 

      if (status && (0u != deps->GetSize()))
        {
          status = SerialiseDependencies(*deps, p_buf, size); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
        }
    }

  if (status)
    {
      const ccs::types::char8 * type_name = type->GetElementType()->GetName();
      (void)snprintf(p_buf, size, "\"multiplicity\":%u,\"size\":%u,\"element\":{\"type\":\"%s\"}}", type->GetElementNumber(), type->GetSize(), type_name); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
    }

  if (status && allocate)
    {
      delete deps;
    }

  return status;

}

static bool Serialise (const ccs::base::SharedReference<const ccs::types::CompoundType>& type, ccs::types::char8 * const buffer, ccs::types::uint32 size, ccs::base::AnyTypeDatabase * deps)
{

  bool status = (static_cast<bool>(type) &&
                 (NULL_PTR_CAST(const ccs::types::char8*) != buffer));

  ccs::types::char8* p_buf = buffer;

  bool allocate = (NULL_PTR_CAST(ccs::base::AnyTypeDatabase*) == deps);

  if (allocate)
    {
      deps = new (std::nothrow) ccs::base::AnyTypeDatabase ();
      status = (NULL_PTR_CAST(ccs::base::AnyTypeDatabase*) != deps);
    }

  if (status)
    {
      //(void)snprintf(p_buf, size, "{\"type\":\"%s\",\"size\":%u,", type->GetName(), type->GetSize()); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
      (void)snprintf(p_buf, size, "{\"type\":\"%s\",", type->GetName()); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
    }

  if (status && allocate)
    {
      // Schema v1.1 .. incl. dependencies
      log_debug("Serialise<CompoundType>('%s') - Collect dependencies ..", type->GetName());

      // Try and list type dependencies
      status = CollectDependencies(type, *deps); 

      if (status && (0u != deps->GetSize()))
        {
          status = SerialiseDependencies(*deps, p_buf, size); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
        }
    }

  if (status)
    {
      (void)ccs::HelperTools::SafeStringCopy(p_buf, "\"attributes\":[", size); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer 
    }

  for (ccs::types::uint32 index = 0u; ((index < type->GetAttributeNumber()) && status); index++)
    {
      const ccs::types::char8 * attr_name = type->GetAttributeName(index);
      const ccs::types::char8 * type_name = type->GetAttributeType(index)->GetName();

      (void)snprintf(p_buf, size, "{\"%s\":{\"type\":\"%s\"}},", attr_name, type_name); size -= strlen(p_buf); p_buf += strlen(p_buf); // Re-align pointer
    }

  // Terminate buffer - remove final comma character 
  if (static_cast<ccs::types::uint32>(strlen(buffer)) >= 1u) 
    {
      *(buffer + strlen(buffer) - 1u) = 0; p_buf = buffer + strlen(buffer); // Re-align pointer 
    }

  (void)ccs::HelperTools::SafeStringCopy(p_buf, "]}", size); 

  if (status && allocate)
    {
      delete deps;
    }

  return status;

}

static bool Serialise (const ccs::base::SharedReference<const ccs::types::ScalarType>& type, ccs::types::char8 * const buffer, ccs::types::uint32 size, ccs::base::AnyTypeDatabase * deps)
{

  bool status = (static_cast<bool>(type) &&
                 (NULL_PTR_CAST(const ccs::types::char8*) != buffer));

  // Not relevant here
  (void)deps;

  if (status)
    {
      (void)snprintf(buffer, size, "{\"type\":\"%s\",\"size\":%u}", type->GetName(), type->GetSize()); 
    }

  return status;

}

static ccs::base::SharedReference<const ccs::types::CompoundType> Flatten (const ccs::base::SharedReference<const ccs::types::AnyType>& type)
{

  log_trace("Flatten('%s') - Entering method", type->GetName());
#ifndef INHERIT_FROM_COMPOUND_TYPE
  bool status = static_cast<bool>(type);

  ccs::base::SharedReference<const ccs::types::CompoundType> flattened; // ToDo - Name derived from this->GetType()

  if (status)
    {
      if (Is<ccs::types::ArrayType>(type))
        {
          ccs::base::SharedReference<const ccs::types::ArrayType> __type = type;
          flattened = Flatten(__type);
        } 
      else if (Is<ccs::types::CompoundType>(type))
        {
          ccs::base::SharedReference<const ccs::types::CompoundType> __type = type;
          flattened = Flatten(__type);
        }
      else
        {
          log_warning("Flatten('%s') - Is<ScalarType> .. unsupported");
        }
    }
#else
  bool status = (static_cast<bool>(type) && Is<ccs::types::CompoundType>(type));

  ccs::base::SharedReference<const ccs::types::CompoundType> flattened; // ToDo - Name derived from this->GetType()

  if (status)
    {
      ccs::base::SharedReference<const ccs::types::CompoundType> __type = type;
      flattened = Flatten(__type);
    }
#endif
  log_trace("Flatten('%s') - Leaving method", type->GetName());

  return flattened;

}
#ifndef INHERIT_FROM_COMPOUND_TYPE
static ccs::base::SharedReference<const ccs::types::CompoundType> Flatten (const ccs::base::SharedReference<const ccs::types::ArrayType>& type)
{

  log_trace("Flatten('%s') - Entering method", type->GetName());

  bool status = static_cast<bool>(type);

  ccs::base::SharedReference<ccs::types::CompoundType> flattened;

  if (status)
    {
      flattened = ccs::base::SharedReference<ccs::types::CompoundType>(new (std::nothrow) ccs::types::CompoundType); // ToDo - Name derived from this->GetType()
      status = static_cast<bool>(flattened);
    }

  ccs::base::SharedReference<const ccs::types::AnyType> elem_type;

  if (status)
    {
      elem_type = type->GetElementType();
      status = static_cast<bool>(elem_type);
    }

  if (status)
    {
      if (Is<ccs::types::ScalarType>(elem_type))
        {
          for (ccs::types::uint32 index = 0u; (status && (index < type->GetMultiplicity())); index++)
            {
              ccs::types::string attr_name = STRING_UNDEFINED;
              snprintf(attr_name, ccs::types::MaxStringLength, "%u", index);
              
              flattened->AddAttribute(attr_name, elem_type);
            }
        }
      else
        {
          const ccs::base::SharedReference<const ccs::types::CompoundType> nested = Flatten(elem_type);
          status = static_cast<bool>(nested); // Then it is a flattened type .. no more recursion
          
          for (ccs::types::uint32 outer = 0u; (status && (outer < type->GetMultiplicity())); outer++)
            {
              for (ccs::types::uint32 inner = 0u; (status && (inner < nested->GetAttributeNumber())); inner++)
                {
                  ccs::types::string attr_name = STRING_UNDEFINED;
                  snprintf(attr_name, ccs::types::MaxStringLength, "%u/%s", outer, nested->GetAttributeName(inner));
                  
                  flattened->AddAttribute(attr_name, nested->GetAttributeType(inner));
                }
            }
        }
    }

  log_debug("Flatten('%s') - Flattened '%u' attributes", type->GetName(), flattened->GetAttributeNumber());

  log_trace("Flatten('%s') - Leaving method", type->GetName());

  return flattened;

}
#endif
static ccs::base::SharedReference<const ccs::types::CompoundType> Flatten (const ccs::base::SharedReference<const ccs::types::CompoundType>& type)
{

  log_trace("Flatten('%s') - Entering method", type->GetName());

  bool status = static_cast<bool>(type);

  ccs::base::SharedReference<ccs::types::CompoundType> flattened;

  if (status)
    {
      flattened = ccs::base::SharedReference<ccs::types::CompoundType>(new (std::nothrow) ccs::types::CompoundType); // ToDo - Name derived from this->GetType()
      status = static_cast<bool>(flattened);
    }

  for (ccs::types::uint32 outer = 0u; (status && (outer < type->GetAttributeNumber())); outer++)
    {
      ccs::base::SharedReference<const ccs::types::AnyType> attr_type = type->GetAttributeType(outer);
      status = static_cast<bool>(attr_type);

      if (status)
        {
          if (Is<ccs::types::ScalarType>(attr_type))
            {
              flattened->AddAttribute(type->GetAttributeName(outer), attr_type);
            }
          else
            {
              ccs::base::SharedReference<const ccs::types::CompoundType> nested = Flatten(type->GetAttributeType(outer));
              status = static_cast<bool>(nested); // Then it is a flattened type .. no more recursion
              
              for (ccs::types::uint32 inner = 0u; (status && (inner < nested->GetAttributeNumber())); inner++)
                {
                  ccs::types::string attr_name = STRING_UNDEFINED;
                  snprintf(attr_name, ccs::types::MaxStringLength, "%s.%s", type->GetAttributeName(outer), nested->GetAttributeName(inner));
                  
                  flattened->AddAttribute(attr_name, nested->GetAttributeType(inner));
                }
            }
        }
    }

  log_debug("Flatten('%s') - Flattened '%u' attributes", type->GetName(), flattened->GetAttributeNumber());
  
  log_trace("Flatten('%s') - Leaving method", type->GetName());

  return flattened;

}

static inline void DumpGTDBToLog (void)
{

  for (::ccs::types::uint32 index = 0ul; index < ::ccs::base::GlobalTypeDatabase::GetInstance()->GetSize(); index++)
    {
      log_info("[%u] - '%s' ..", index, ::ccs::base::GlobalTypeDatabase::GetInstance()->GetName(index));
      ::ccs::types::char8 buffer [1024];
      (void)::ccs::HelperTools::Serialise(::ccs::base::GlobalTypeDatabase::GetInstance()->GetElement(index), buffer, 1024u);
      log_info(".. '%s'", buffer);
    }

  return;

}

} // namespace HelperTools

} // namespace ccs

#endif // _AnyTypeHelper_h_ 

