/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file RPCTypes.h
 * @brief Header file for RPC-specific types.
 * @date 11/11/2018
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @details This header file contains the definition of the RPC-specific types.
 */

#ifndef _RPCTypes_h_
#define _RPCTypes_h_

// Global header files

// Local header files

#include "BasicTypes.h"
#include "TimeTools.h" // GetCurrentTime, etc.

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

// Constants

#define RPCReply_TypeName "ccs::RPCReply/v1.0"
#define RPCReplyWithValue_TypeName "ccs::RPCReply<T>/v1.0"
#define RPCRequest_TypeName "ccs::RPCRequest/v1.0"
#define RPCRequestWithValue_TypeName "ccs::RPCRequest<T>/v1.0"

// Type definition

namespace ccs {

namespace types {

typedef struct __attribute__((packed)) Request {
  ccs::types::uint64 timestamp;
  ccs::types::string qualifier = "";
} Request_t;

template <typename Type> struct __attribute__((packed)) RequestWithValue : public Request {
  Type value;
};

typedef struct __attribute__((packed)) Reply {
  ccs::types::uint64 timestamp;
  ccs::types::string qualifier = "reply";
  ccs::types::boolean status = true;
  ccs::types::string reason = ""; // Optional substantiating information related to status
} Reply_t;

template <typename Type> struct __attribute__((packed)) ReplyWithValue : public Reply {
  Type value;
};

// Global variables

extern ccs::base::SharedReference<const ccs::types::CompoundType> Request_int; // Introspectable type definition
extern ccs::base::SharedReference<const ccs::types::CompoundType> Reply_int; // Introspectable type definition

} // namespace types

namespace HelperTools {

// Function declaration

// WARNING - ScalarType are statically initialised but there is no way to guarantee static
// initialisation order. The above types use ScalarType as attributes and must therefore be
// dynamically initialised .. called by RPC participants classes upon initialisation.
bool InitialiseRPCTypes (void);

// Function definition

static inline ccs::types::AnyValue InstantiateRPCRequest (const ccs::types::char8 * const qualifier)
{

  // Instantiate RPC request ..
  ccs::types::AnyValue request (*ccs::types::Request_int); // Default RPC request type
  
  ccs::HelperTools::SetAttributeValue<ccs::types::uint64>(&request, "timestamp", ccs::HelperTools::GetCurrentTime());
  ccs::HelperTools::SetAttributeValue(&request, "qualifier", qualifier);

  // Provide default request
  return request;

}

static inline ccs::types::AnyValue InstantiateRPCRequest (const ccs::types::char8 * const qualifier, const ccs::types::AnyValue& value)
{

  // Extend base reply type definition ..
  ccs::types::CompoundType type (*ccs::types::Request_int); // Copy base request type ..
  type.AddAttribute("value", value.GetType()); // .. add attribute
  type.SetName(RPCRequestWithValue_TypeName); // .. change type name

  // Instantiate RPC request ..
  ccs::types::AnyValue request (type);
  
  ccs::HelperTools::SetAttributeValue<ccs::types::uint64>(&request, "timestamp", ccs::HelperTools::GetCurrentTime());
  ccs::HelperTools::SetAttributeValue(&request, "qualifier", qualifier);
  ccs::HelperTools::SetAttributeValue(&request, "value", value);

  // Provide request
  return request;

}

static inline ccs::types::AnyValue InstantiateRPCReply (bool status, const ccs::types::char8 * const qualifier, const ccs::types::char8 * const reason)
{

  // Instantiate RPC reply ..
  ccs::types::AnyValue reply (*ccs::types::Reply_int); // Default RPC reply type
  
  ccs::HelperTools::SetAttributeValue<ccs::types::uint64>(&reply, "timestamp", ccs::HelperTools::GetCurrentTime());
  ccs::HelperTools::SetAttributeValue<ccs::types::boolean>(&reply, "status", status);
  ccs::HelperTools::SetAttributeValue(&reply, "qualifier", qualifier);
  ccs::HelperTools::SetAttributeValue(&reply, "reason", reason);

  // Provide default reply
  return reply;

}

static inline ccs::types::AnyValue InstantiateRPCReply (bool status, const ccs::types::AnyValue& value, const ccs::types::char8 * const qualifier, const ccs::types::char8 * const reason)
{

  // Extend base reply type definition ..
  ccs::types::CompoundType type (*ccs::types::Reply_int); // Copy base reply type ..
  type.AddAttribute("value", value.GetType()); // .. add attribute
  type.SetName(RPCReplyWithValue_TypeName); // .. change type name

  // Instantiate RPC reply ..
  ccs::types::AnyValue reply (type);
  
  ccs::HelperTools::SetAttributeValue<ccs::types::uint64>(&reply, "timestamp", ccs::HelperTools::GetCurrentTime());
  ccs::HelperTools::SetAttributeValue<ccs::types::boolean>(&reply, "status", status);
  ccs::HelperTools::SetAttributeValue(&reply, "qualifier", qualifier);
  ccs::HelperTools::SetAttributeValue(&reply, "reason", reason);
  ccs::HelperTools::SetAttributeValue(&reply, "value", value);

  // Provide reply
  return reply;

}

} // namespace HelperTools

} // namespace ccs

#endif // _RPCTypes_h_

