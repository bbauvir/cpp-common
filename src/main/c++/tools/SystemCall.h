/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Misc. system-level helper routines
*
* Author        : B.Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

#ifndef _SystemCall_h_
#define _SystemCall_h_

// Global header files

#include <cstdlib> // std::system, etc.

// Local header files

#include "BasicTypes.h" // Misc. type definition

#include "StringTools.h" // Misc. helper routines

#include "log-api.h"

// Constants

// Type definition

#ifdef __cplusplus

namespace ccs {

namespace HelperTools {

// Function declaration

/**
 * @brief Helper routine.
 * @details Performs system call according to the provided command.
 * @param command System command to be executed.
 * @return True if system call returns 0.
 * @code
     bool status = ccs::HelperTools::ExecuteSystemCall("/usr/bin/screen -d -m /usr/bin/softIoc -d ./softioc-records.db &> /dev/null");
   @endcode
 */

static inline bool ExecuteSystemCall (const ccs::types::char8 * const command);

// Global variables

// Function definition

static inline bool ExecuteSystemCall (const ccs::types::char8 * const command)
{

  bool status = (false == ccs::HelperTools::IsUndefinedString(command));
#ifdef SYSTEM_CALL_VERIFY_COMMAND_PROCESSOR
  if (status)
    { // Tests for availability of command processor
      status = (0 != std::system(NULL_PTR_CAST(const ccs::types::char8*));
    }
#endif
  if (status)
    {
      status = (0 == std::system(command));
    }

  if (status)
    {
      log_debug("ExecuteSystemCall - Invoking command '%s' successful", command);
    }
  else
    {
      log_error("ExecuteSystemCall - Error with '%s' command", command);
    }

  return status;

}

} // namespace HelperTools

} // namespace ccs

#endif // __cplusplus

#endif // _SystemCall_h_

