/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Misc. system-level helper routines
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

#ifndef _StringTools_h
#define _StringTools_h

/**
 * @file StringTools.h
 * @brief Header file for various string-related (character arrays) helper routines
 * @date 14/09/2017
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @details This header file contains the definition of string-related helper routines.
 */

// Global header files

#include <stdlib.h> // strtol, getenv, etc.
#include <string.h> // strtok, etc.

#ifdef __cplusplus
#include <string> // std::string
#include <sstream> // std::cout
#include <fstream> // std::ifstream
#endif // __cplusplus

// Local header files

#include "BasicTypes.h"

// Constants

// Type definition

#ifdef __cplusplus

namespace ccs {

namespace HelperTools {

// Global variables

// Function declaration

// Function definition

/**
 * @brief Converts char array to uppercase characters.
 * @return The modified char array.
 */

static inline ccs::types::char8* ToUppercase (ccs::types::char8 * const s)
{ 

  ccs::types::char8* p = s; 

  for (; *p; ++p)
    {
      *p = static_cast<ccs::types::char8>(toupper(static_cast<int>(*p)));
    }

  return s; 

}

/**
 * @brief Converts char array to lowercase characters.
 * @return The modified char array.
 */

static inline ccs::types::char8* ToLowercase (ccs::types::char8 * const s)
{ 

  ccs::types::char8* p = s; 

  for (; *p; ++p)
    {
      *p = static_cast<ccs::types::char8>(tolower(static_cast<int>(*p)));
    }

  return s; 

}

/**
 * @brief Compute string length.
 * @return Position of the last non-zero character.
 */

static inline ccs::types::uint32 StringLength (const ccs::types::char8 * const buffer)
{ 

  bool status = (buffer != NULL_PTR_CAST(const ccs::types::char8 *)); 

  ccs::types::uint32 ret = 0u; 
  const ccs::types::char8* p_buf = buffer;

  while (status && (*p_buf != 0))
    {
      ret++;
      p_buf++;
    } 

  return ret; 

}

/**
 * @brief Performs a string comparison.
 * @return TRUE if strings are identical.
 */

static inline bool StringCompare (const ccs::types::char8 * const str1, const ccs::types::char8 * const str2, const ccs::types::int32 size = 0) 
{ 

  bool status = ((str1 != NULL_PTR_CAST(const ccs::types::char8 *)) && (str2 != NULL_PTR_CAST(const ccs::types::char8 *)));

  if (status)
    {
      if (0 == size)
        {
          status = (0 == strcmp(str1, str2));
        }
      else
        {
          status = (0 == strncmp(str1, str2, size));
        }
    }

  return status; 

}

/**
 * @brief Performs a safe string copy by ensuring the destination char array is zero terminated.
 * @return The destination char array.
 */

static inline ccs::types::char8* SafeStringCopy (ccs::types::char8 * const dst, const ccs::types::char8 * const src, const ccs::types::int32 size, const ccs::types::char8 trm = 0) 
{ 

  if ((dst != NULL_PTR_CAST(ccs::types::char8*)) && (src != NULL_PTR_CAST(const ccs::types::char8 *)) && (size > 0)) 
    { 
      (void)strncpy(dst, src, size); 
      dst[size-1] = 0; // Force buffer termination
    } 

  if (trm != 0)
    {
      ccs::types::char8* buf = dst;
      while ((*buf != 0) && (*buf != trm)) { buf++; } // Skip character
      *buf = 0; // Force buffer termination
    }

  return dst; 

}

/**
 * @brief Performs a safe string append operation by ensuring the destination char array is zero terminated.
 * @return The destination char array.
 */

static inline ccs::types::char8* SafeStringAppend (ccs::types::char8 * const dst, const ccs::types::char8 * const src, const ccs::types::int32 size, const ccs::types::char8 separator = '\0')
{ 

  ccs::types::int32 length = static_cast<ccs::types::int32>(ccs::HelperTools::StringLength(dst));

  if ((dst != NULL_PTR_CAST(ccs::types::char8*)) && (src != NULL_PTR_CAST(const ccs::types::char8 *)) && (size > 0) && (size > (length + 1))) 
    {
      if ('\0' == separator)
        {
          (void)SafeStringCopy(dst+length, src, size-length);
        }
      else
        {
          ccs::types::char8* p_buf = dst+length;

          *p_buf = separator; 
          p_buf++; 
          *p_buf = 0; 
          length++;

          (void)SafeStringCopy(dst+length, src, size-length);
        }
    } 

  return dst; 

}

/**
 * @brief Tests if the char array is invalid or empty.
 * @return TRUE if the char array is invalid or empty.
 */

static inline bool IsUndefinedString (const ccs::types::char8 * const buffer) { return ((buffer == NULL_PTR_CAST(const ccs::types::char8 *)) || (*buffer == 0)); }

/**
 * @brief Tests is the string contains the specified token.
 * @return TRUE if the token is found in the string.
 */

static inline bool Contain (const ccs::types::char8 * const buffer, const ccs::types::char8 token, const ccs::types::uint32 offset = 0u)
{ 

  bool status = ((false == IsUndefinedString(buffer)) && 
                 (0 != token) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  if (status)
    {
      std::string helper (buffer);
      status = (std::string::npos != helper.find(token, offset));
    }
  
  return status; 
  
}
 
static inline bool Contain (const ccs::types::char8 * const buffer, const ccs::types::char8 * const token, const ccs::types::uint32 offset = 0u)
{ 

  bool status = ((false == IsUndefinedString(buffer)) && 
                 (false == IsUndefinedString(token)) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  if (status)
    {
      std::string helper (buffer);
      status = (std::string::npos != helper.find(token, offset));
    }
  
  return status; 
  
}
 
/**
 * @brief Locate token in the string buffer.
 * @return Offset within buffer if successful, -1 otherwise.
 */
 
static inline ccs::types::int32 Find (const ccs::types::char8 * const buffer, const ccs::types::char8 token, const ccs::types::uint32 offset = 0u)
{ 

  ccs::types::int32 ret = -1;

  bool status = ((false == IsUndefinedString(buffer)) && 
                 (0 != token) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));
  
  std::size_t found = std::string::npos;

  if (status)
    {
      std::string helper (buffer);
      found = helper.find(token, offset);
      status = (std::string::npos != found);
    }

  if (status)
    {
      ret = static_cast<ccs::types::int32>(found);
    }
  
  return ret; 
  
}

static inline ccs::types::int32 Find (const ccs::types::char8 * const buffer, const ccs::types::char8 * const token, const ccs::types::uint32 offset = 0u)
{ 

  ccs::types::int32 ret = -1;

  bool status = ((false == IsUndefinedString(buffer)) && 
                 (false == IsUndefinedString(token)) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));
  
  std::size_t found = std::string::npos;

  if (status)
    {
      std::string helper (buffer);
      found = helper.find(token, offset);
      status = (std::string::npos != found);
    }

  if (status)
    {
      ret = static_cast<ccs::types::int32>(found);
    }
  
  return ret; 
  
}

static inline ccs::types::int32 FindFirstOf (const ccs::types::char8 * const buffer, const ccs::types::char8 * const tokens, const ccs::types::uint32 offset = 0u)
{ 

  ccs::types::int32 ret = -1;
  ccs::types::uint32 pos = ccs::HelperTools::StringLength(buffer);

  bool status = ((false == IsUndefinedString(buffer)) && 
                 (false == IsUndefinedString(tokens)) &&
                 (offset < pos));

  ccs::types::uint32 index = 0u;

  while (status && (tokens[index] != 0))
    {
      ccs::types::int32 token = ccs::HelperTools::Find(buffer, tokens[index], offset);

      if ((-1 != token) && (static_cast<ccs::types::uint32>(token) < pos)) // Found one token
        {
          pos = static_cast<ccs::types::uint32>(token);
          ret = token;
        }

      index++;
    }

  return ret; 
  
}

static inline ccs::types::int32 FindFirstNotOf (const ccs::types::char8 * const buffer, const ccs::types::char8 * const tokens, const ccs::types::uint32 offset = 0u)
{ 

  ccs::types::int32 ret = -1;

  bool status = ((false == IsUndefinedString(buffer)) && 
                 (false == IsUndefinedString(tokens)) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  bool found = false;
  ccs::types::uint32 __offset = offset;

  while (status && (false == found))
    {
      found = (static_cast<ccs::types::int32>(__offset) != ccs::HelperTools::FindFirstOf(buffer, tokens, __offset));

      if (found)
        {
          break;
        }
      else
        {
          __offset++;
        }
    }

  if (status)
    {
      ret = static_cast<ccs::types::int32>(__offset);
    }

  return ret; 
  
}

/**
 * @brief Count number of tokens in the string buffer.
 * @return Number of token instances within buffer.
 */

static inline ccs::types::uint32 Count (const ccs::types::char8 * const buffer, const ccs::types::char8 token, const ccs::types::uint32 offset = 0u)
{ 

  ccs::types::uint32 ret = 0u;
  
  bool status = ccs::HelperTools::Contain(buffer, token, offset);
  
  if (status)
    {
      ccs::types::uint32 index = static_cast<ccs::types::uint32>(Find(buffer, token, offset));
      ret = 1u + ccs::HelperTools::Count(buffer, token, index + 1u);
    }
  
  return ret; 
  
}

static inline ccs::types::uint32 Count (const ccs::types::char8 * const buffer, const ccs::types::char8 * const token, const ccs::types::uint32 offset = 0u)
{ 

  ccs::types::uint32 ret = 0u;
  
  bool status = ccs::HelperTools::Contain(buffer, token, offset);
  
  if (status)
    {
      ccs::types::uint32 index = static_cast<ccs::types::uint32>(Find(buffer, token, offset));
      ret = 1u + ccs::HelperTools::Count(buffer, token, index + 1u);
    }
  
  return ret; 
  
}

/**
 * @brief Locate nth instance of the token in the string buffer.
 * @return Offset within buffer if successful, -1 otherwise.
 */

static inline ccs::types::int32 Locate (const ccs::types::char8 * const buffer, const ccs::types::char8 * const token, const ccs::types::uint32 instance = 1u)
{ 

  ccs::types::int32 ret = -1;
  
  bool status = ((0u != instance) &&
                 (instance <= ccs::HelperTools::Count(buffer, token)));
  
  if (status)
    {
      ccs::types::uint32 offset = 0u;
      ccs::types::uint32 count = instance;

      while (count != 0u)
        {
          ret = ccs::HelperTools::Find(buffer, token, offset);
          offset += ret + 1u;
          count -= 1u;
        }
    }

  return ret; 

}

/**
 * @brief Strip token out of the string buffer.
 * @return TRUE if token removed, FALSE otherwise.
 */

static inline bool Strip (ccs::types::char8 * const buffer, const ccs::types::char8 * const token)
{ 

  bool status = ((NULL_PTR_CAST(ccs::types::char8*) != buffer) &&
                 (true == Contain(buffer, token)));
  
  if (status)
    {
      std::string helper (buffer);
      (void)SafeStringCopy(buffer, "", helper.size());

      while (std::string::npos != helper.find(token))
        {
          (void)helper.erase(helper.find(token), ccs::HelperTools::StringLength(token));
        }

      (void)SafeStringCopy(buffer, helper.c_str(), static_cast<ccs::types::uint32>(helper.size()) + 1u);
    }

  return status; 

}

/**
 * @brief Tests if the char array refers to an environment variable.
 * @return TRUE if the char array is interpretable by reference to an environment variable.
 */

static inline bool IsVariableString (const ccs::types::char8 * const buffer) 
{ 

  bool status = (IsUndefinedString(buffer) == false);

  if (status)
    {
      status = (*buffer == '$');
    }

  return status; 

}

/**
 * @brief Tests if the char array corresponds to an boolean, i.e. true or false.
 * @return TRUE if the char array is interpretable as a boolean
 */

static inline bool IsBooleanString (const ccs::types::char8 * const buffer)
{

  bool status = (IsUndefinedString(buffer) == false);

  if (status)
    {
      status = (StringCompare(buffer, "true") || StringCompare(buffer, "false"));
    }

  return status;

}

/**
 * @brief Tests if the char array corresponds to a number.
 * @return TRUE if the char array is interpretable as a number.
 */

static inline bool IsNumberString (const ccs::types::char8 * const buffer)
{

  bool status = (IsUndefinedString(buffer) == false);

  if (status)
    {
      // Try and convert the string
      ccs::types::char8* p = NULL_PTR_CAST(ccs::types::char8*);
      (void)strtod(buffer, &p); // p would point to the first character not interpreted as part of a number
      status = (*p == '\0');
    }

  return status;

}

/**
 * @brief Tests if the char array corresponds to an integer number.
 * @return TRUE if the char array is interpretable as an integer.
 */

static inline bool IsIntegerString (const ccs::types::char8 * const buffer)
{

  bool status = (IsUndefinedString(buffer) == false);

  if (status)
    {
      status = ((0 != isdigit(*buffer)) || (*buffer == '-') || (*buffer == '+'));
    }

  if (status)
    {
      // Try and convert the string in base 10
      ccs::types::char8* p = NULL_PTR_CAST(ccs::types::char8*);
      (void)strtol(buffer, &p, 10); // p would point to the first character not interpreted as part of a number
      status = (*p == '\0');
    }

  return status;

}

static inline ccs::types::int32 ToInteger (const ccs::types::char8 * const buffer)
{

  ccs::types::int32 ret = 0u; 

  bool status = (IsIntegerString(buffer) == true);

  if (status)
    {
      ccs::types::char8* p = NULL_PTR_CAST(ccs::types::char8*);
      ret = static_cast<ccs::types::int32>(strtol(buffer, &p, 10));
    }

  return ret;

}

static inline bool ToString (ccs::types::int32 value, ccs::types::char8 * const buffer, ccs::types::int32 size = STRING_MAX_LENGTH)
{

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);

  if (status)
    {
      status = (0 < snprintf(buffer, size, "%d", value));
    }

  return status;

}

static inline bool ToString (ccs::types::uint32 value, ccs::types::char8 * const buffer, ccs::types::int32 size = STRING_MAX_LENGTH)
{

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);

  if (status)
    {
      status = (0 < snprintf(buffer, size, "%u", value));
    }

  return status;

}

static inline bool IsAlphaNumeric (ccs::types::char8 c)
{

  bool status = (((c >= '0') && (c <= '9')) ||
                 ((c >= 'A') && (c <= 'Z')) ||
                 ((c >= 'a') && (c <= 'z')));

  return status;

}

} // namespace HelperTools

} // namespace ccs

#endif // __cplusplus

#endif // _StringTools_h
