/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file JSONTools.h
 * @brief Header file for JSON helper methods.
 * @date 20/05/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @details This header file contains the definition of the JSON helper methods.
 * @todo Revisit API specification for better homogeneity, etc.
 */

#ifndef _JSONTools_h_
#define _JSONTools_h_

// Global header files

// Local header files

#include "log-api.h" // Syslog wrapper routines
#include "SysTools.h" // ccs::HelperTools::IsIntegerString, etc.

// Constants

// Type definition

namespace ccs {

namespace HelperTools {

// Global variables

// Function declaration

static inline ccs::types::int32 FindMatchingBrace (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 ret = -1;

  const ccs::types::char8 * p_buf = buffer + offset;

  if (status)
    {
      status = ((*p_buf == '[') || (*p_buf == '{'));
    }

  if (status) // p_buf points to the opening brace
    {
      ccs::types::char8 brace = *p_buf;
      ccs::types::char8 close = brace + static_cast<ccs::types::char8>(2); // Check ASCII table
      ccs::types::uint32 count = 0u;

      ret = 0;
      count++;

      ccs::types::char8 tokens [3] = { brace, close, '\0' };

      while (count > 0u)
        {
          p_buf++; ret++; // Skip found brace

          ccs::types::int32 pos = ccs::HelperTools::FindFirstOf(p_buf, tokens);

          status = (-1 != pos);

          if (status)
            {
              p_buf += pos; // p_buf points at a brace
              ret += pos;

              // Assume a closing brace
              count--;

              if (*p_buf == brace) // Opening brace
                {
                  count += 2u;
                }
            }
        }
    }

  if (!status)
    {
      ret = -1;
    }

  return ret;

}

static inline bool IsJSONArray (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  const ccs::types::char8* p_buf = buffer + offset;

  if (status)
    {
      status = (('[' == *p_buf) && (-1 != FindMatchingBrace(p_buf)));
    }

  // To be implemented further ..
  // .. comma-separated elements

  return status;

}

static inline ccs::types::int32 FindJSONArrayStart (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 ret = -1;

  if (status)
    {
      ret = ccs::HelperTools::Find(buffer, '[', offset);
      status = (-1 != ret);
    }

  if (status)
    {
      status = IsJSONArray(buffer, ret);
    }

  if (!status)
    {
      ret = -1;
    }

  return ret;

}

static inline ccs::types::int32 FindJSONArrayEnd (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 ret = -1;
  ccs::types::int32 sta = -1;

  if (status)
    {
      sta = FindJSONArrayStart(buffer, offset);
      status = (-1 != sta);
    }

  ccs::types::int32 end = -1;

  if (status)
    {
      const ccs::types::char8* p_buf = buffer + sta;
      end = FindMatchingBrace(p_buf);
      status = (-1 != end);
    }

  if (status)
    {
      ret = sta + end;
    }

  return ret;

}

static inline bool FindJSONArray (const ccs::types::char8 * const buffer, ccs::types::uint32& start, ccs::types::uint32& close, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 _start = -1;
  ccs::types::int32 _close = -1;

  if (status)
    {
      _start = FindJSONArrayStart(buffer, offset);
      _close = FindJSONArrayEnd(buffer, offset);
      status = ((-1 != _start) && (-1 != _close) && (start <= close));
    }

  if (status)
    {
      start = static_cast<ccs::types::uint32>(_start);
      close = static_cast<ccs::types::uint32>(_close);
    }

  return status;

}

static inline ccs::types::uint32 JSONArrayLength (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::uint32 ret = 0;

  ccs::types::uint32 start = 0;
  ccs::types::uint32 close = 0;

  if (status)
    {
      status = FindJSONArray(buffer, start, close, offset);
    }

  if (status)
    {
      ret = close - start + 1u;
    }

  return ret;

}

static inline bool CopyJSONArray (const ccs::types::char8 * const buffer, ccs::types::char8 * const array, const ccs::types::uint32 size, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (size > JSONArrayLength(buffer, offset)));

  ccs::types::uint32 start = 0;
  ccs::types::uint32 close = 0;

  if (status)
    {
      status = FindJSONArray(buffer, start, close, offset);
    }

  ccs::types::uint32 _size = 0u;

  if (status)
    {
      _size = close - start + 2u; // + 1u + 1u (for null character)
      status = (size >= _size);
    }

  if (status)
    {
      (void)ccs::HelperTools::SafeStringCopy(array, buffer + start, _size);
    } 

  return status;

}

static inline ccs::types::int32 FindJSONArrayElementStart (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 ret = -1;

  if (status)
    {
      ret = ccs::HelperTools::FindFirstNotOf(buffer, "[ ,", offset);
      status = (-1 != ret);

      // Assume well formed JSON array

      if (status)
        {
          status = ((0 != buffer[ret]) && (']' != buffer[ret]));
        }
    }

  if (!status)
    {
      ret = -1;
    }

  return ret;

}

static inline ccs::types::int32 FindJSONArrayElementEnd (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 ret = -1;
  ccs::types::int32 sta = -1;

  if (status)
    {
      sta = FindJSONArrayElementStart(buffer, offset);
      status = (-1 != sta);
    }

  ccs::types::int32 end = -1;

  if (status)
    {
      const ccs::types::char8* p_buf = buffer + sta;

      switch (*p_buf)
        {
          case '{': // Object
            end = FindMatchingBrace(p_buf);
            break;
          case '"': // String
            end = ccs::HelperTools::Find(p_buf, '"', 1u);
            break;
          default:
            end = ccs::HelperTools::FindFirstOf(p_buf, " ,]");

            if (-1 != end)
              {
                end--;
              }

            break;
        }

      status = (-1 != end);
    }

  if (status)
    {
      ret = sta + end;
    }

  return ret;

}

static inline bool FindJSONArrayElement (const ccs::types::char8 * const buffer, ccs::types::uint32& start, ccs::types::uint32& close, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 _start = -1;
  ccs::types::int32 _close = -1;

  if (status)
    {
      _start = FindJSONArrayElementStart(buffer, offset);
      _close = FindJSONArrayElementEnd(buffer, offset);
      status = ((-1 != _start) && (-1 != _close) && (start <= close));
    }

  if (status)
    {
      start = static_cast<ccs::types::uint32>(_start);
      close = static_cast<ccs::types::uint32>(_close);
    }

  return status;

}

static inline bool IsJSONObject (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  const ccs::types::char8* p_buf = buffer + offset;

  if (status)
    {
      status = (('{' == *p_buf) && (-1 != FindMatchingBrace(p_buf)));
    }

  // To be implemented further ..
  // .. comma-separated name-value pairs

  return status;

}

static inline ccs::types::int32 FindJSONObjectStart (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 ret = -1;

  if (status)
    {
      ret = ccs::HelperTools::Find(buffer, '{', offset);
      status = (-1 != ret);
    }

  if (status)
    {
      status = IsJSONObject(buffer, ret);
    }

  if (!status)
    {
      ret = -1;
    }

  return ret;

}

static inline ccs::types::int32 FindJSONObjectEnd (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 ret = -1;
  ccs::types::int32 sta = -1;

  if (status)
    {
      sta = FindJSONObjectStart(buffer, offset);
      status = (-1 != sta);
    }

  ccs::types::int32 end = -1;

  if (status)
    {
      const ccs::types::char8* p_buf = buffer + sta;
      end = FindMatchingBrace(p_buf);
      status = (-1 != end);
    }

  if (status)
    {
      ret = sta + end;
    }

  return ret;

}

static inline bool FindJSONObject (const ccs::types::char8 * const buffer, ccs::types::uint32& start, ccs::types::uint32& close, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 _start = -1;
  ccs::types::int32 _close = -1;

  if (status)
    {
      _start = FindJSONObjectStart(buffer, offset);
      _close = FindJSONObjectEnd(buffer, offset);
      status = ((-1 != _start) && (-1 != _close) && (start <= close));
    }

  if (status)
    {
      start = static_cast<ccs::types::uint32>(_start);
      close = static_cast<ccs::types::uint32>(_close);
    }

  return status;

}

static inline ccs::types::uint32 JSONObjectLength (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::uint32 ret = 0;

  ccs::types::uint32 start = 0;
  ccs::types::uint32 close = 0;

  if (status)
    {
      status = FindJSONObject(buffer, start, close, offset);
    }

  if (status)
    {
      ret = close - start + 1u;
    }

  return ret;

}

static inline bool CopyJSONObject (const ccs::types::char8 * const buffer, ccs::types::char8 * const array, const ccs::types::uint32 size, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (size > JSONObjectLength(buffer, offset)));

  ccs::types::uint32 start = 0;
  ccs::types::uint32 close = 0;

  if (status)
    {
      status = FindJSONObject(buffer, start, close, offset);
    }

  ccs::types::uint32 _size = 0u;

  if (status)
    {
      _size = close - start + 2u; // + 1u + 1u (for null character)
      status = (size >= _size);
    }

  if (status)
    {
      (void)ccs::HelperTools::SafeStringCopy(array, buffer + start, _size);
    } 

  return status;

}

static inline ccs::types::int32 FindJSONObjectElementStart (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 ret = -1;

  if (status)
    {
      ret = ccs::HelperTools::FindFirstNotOf(buffer, "{ ,", offset);
      status = (-1 != ret);

      // Assume well formed JSON array

      if (status)
        {
          //status = ((0 != buffer[ret]) && ('}' != buffer[ret]));
          status = ('"' == buffer[ret]);
        }
    }

  if (!status)
    {
      ret = -1;
    }

  return ret;

}

static inline ccs::types::int32 FindJSONObjectElementEnd (const ccs::types::char8 * const buffer, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < ccs::HelperTools::StringLength(buffer)));

  ccs::types::int32 ret = -1;
  ccs::types::int32 sta = -1;

  if (status)
    {
      sta = ccs::HelperTools::Find(buffer, ':', offset); // :..
      status = (-1 != sta);
    }

  if (status)
    {
      sta = ccs::HelperTools::FindFirstOf(buffer, "\"{[0123456789+-.tfn", sta); // string, object, array, value (incl. boolean and null)
      status = (-1 != sta);
    }

  ccs::types::int32 end = -1;

  if (status)
    {
      const ccs::types::char8* p_buf = buffer + sta;

      switch (*p_buf)
        {
          case '{': // Object
          case '[': // Array
            end = FindMatchingBrace(p_buf);
            break;
          case '"': // String
            end = ccs::HelperTools::Find(p_buf, '"', 1u);
            break;
          default:
            end = ccs::HelperTools::FindFirstOf(p_buf, " ,}");

            if (-1 != end)
              {
                end--;
              }

            break;
        }

      status = (-1 != end);
    }

  if (status)
    {
      ret = sta + end;
    }

  return ret;

}

static inline bool FindJSONObjectElement (const ccs::types::char8 * const buffer, ccs::types::uint32& start, ccs::types::uint32& close, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < StringLength(buffer)));

  if (status)
    {
      start = FindJSONObjectElementStart(buffer, offset); // Points to opening '"'
      close = FindJSONObjectElementEnd(buffer, offset); // Points to last character of the value
      status = ((-1 != static_cast<ccs::types::int32>(start)) && (-1 != static_cast<ccs::types::int32>(close)) && (start < close));
    }

  return status;

}

static inline bool FindJSONObjectElementName (const ccs::types::char8 * const buffer, ccs::types::uint32& start, ccs::types::uint32& close, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < StringLength(buffer)));

  if (status)
    {
      status = FindJSONObjectElement(buffer, start, close, offset);
    }

  if (status)
    {
      start = ccs::HelperTools::Find(buffer, '"', start); // ".."
      status = (-1 != static_cast<ccs::types::int32>(start));
    }

  if (status)
    {
      close = ccs::HelperTools::Find(buffer, '"', start + 1u);
      status = (-1 != static_cast<ccs::types::int32>(close));
    }

  return status;

}

static inline bool CopyJSONObjectElementName (const ccs::types::char8 * const buffer, ccs::types::char8 * const array, const ccs::types::uint32 size, const ccs::types::uint32 offset = 0u)
{

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);

  ccs::types::uint32 start = 0;
  ccs::types::uint32 close = 0;

  if (status)
    {
      status = FindJSONObjectElementName(buffer, start, close, offset);
    }

  ccs::types::uint32 _size = 0u;

  if (status)
    { // '"' are encopassed in the returned array indexes
      _size = close - start; // + 1u + 1u (for null character) - 2u (for removing '"')
      status = (size >= _size);
    }

  if (status)
    {
      (void)ccs::HelperTools::SafeStringCopy(array, buffer + start + 1u, size, '"');
    } 

  return status;

}

static inline bool FindJSONObjectElementValue (const ccs::types::char8 * const buffer, ccs::types::uint32& start, ccs::types::uint32& close, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (offset < StringLength(buffer)));

  if (status)
    {
      status = FindJSONObjectElement(buffer, start, close, offset);
    }

  if (status)
    {
      start = ccs::HelperTools::Find(buffer, ':', start); // :..
      status = (-1 != static_cast<ccs::types::int32>(start));
    }

  if (status)
    {
      start = ccs::HelperTools::FindFirstOf(buffer, "\"{[0123456789+-.tfn", start); // string, object, array, value (incl. boolean and null)
      status = (-1 != static_cast<ccs::types::int32>(start));
    }

  return status;

}

static inline bool HasJSONObjectElement (const ccs::types::char8 * const buffer, const ccs::types::char8 * const name, ccs::types::uint32& start, ccs::types::uint32& close, const ccs::types::uint32 offset = 0u)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (NULL_PTR_CAST(ccs::types::char8*) != name) &&
                 (offset < StringLength(buffer)));

  bool ret = false;

  ccs::types::uint32 _offset = offset;

  while (status)
    {
      const ccs::types::char8 * p_buf = buffer + _offset;

      log_debug("HasJSONObjectElement - Try and parse '%s' ..", p_buf);

      ccs::types::string _name = STRING_UNDEFINED;
      status = CopyJSONObjectElementName(p_buf, _name, ccs::types::MaxStringLength);

      if (status)
        {
          log_debug("HasJSONObjectElement - .. found '%s' attribute", _name);

          if (ccs::HelperTools::StringCompare(_name, name))
            {
              log_debug("HasJSONObjectElement - .. match");
              ret = FindJSONObjectElementValue(p_buf, start, close);
              break;
            }
          else
            {
              log_debug("HasJSONObjectElement - .. continue");
              ccs::types::int32 close = ccs::HelperTools::FindJSONObjectElementEnd(p_buf);
              _offset += static_cast<ccs::types::uint32>(close) + 1u;
              status = (-1 != close);
            }
        }
    }

  if (ret)
    {
      start += _offset;
      close += _offset;
      log_debug("HasJSONObjectElement - .. offsets '%u %u'", start, close);
    }

  return ret;

}

static inline bool HasJSONObjectElement (const ccs::types::char8 * const buffer, const ccs::types::char8 * const name, const ccs::types::uint32 offset = 0u)
{

  ccs::types::uint32 start = 0u;
  ccs::types::uint32 close = 0u;

  return HasJSONObjectElement(buffer, name, start, close, offset);

}

static inline bool CopyJSONObjectElement (const ccs::types::char8 * const buffer, const ccs::types::char8 * const name, ccs::types::char8 * const attr, const ccs::types::uint32 size)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (NULL_PTR_CAST(const ccs::types::char8*) != name) &&
                 (NULL_PTR_CAST(ccs::types::char8*) != attr));
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("CopyJSONObjectElement - Try and parse '%s' ..", buffer);
      log_debug("CopyJSONObjectElement - .. looking for '%s'", name);
    }
#endif
  ccs::types::uint32 start = 0u;
  ccs::types::uint32 close = 0u;

  if (status)
    {
      status = HasJSONObjectElement(buffer, name, start, close);
    }

  ccs::types::uint32 _size = 0u;

  if (status)
    {
      close++;
      _size = close - start + 1u;
#if 0 // Truncate anser
      status = (size >= _size);
#else
      if (size < _size)
	{
	  _size = size;
	}
#endif
    }

  if (status)
    {
      (void)ccs::HelperTools::SafeStringCopy(attr, buffer + start, _size);
    }
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("CopyJSONObjectElement - Found attribute '%s':'%s'", name, attr);
    }
#endif
  return status;

}

static inline bool IsJSONString (const ccs::types::char8 * const buffer)
{

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);

  if (status)
    {
      status = ('"' == *buffer);
    }

  // To be implemented further ..

  return status;

}

// Some legacy code maintained for compatibility

static inline ccs::types::int32 FindJSONAttributeStart (const ccs::types::char8 * const buffer) { return FindJSONObjectElementStart(buffer); } // Position of the '"'

static inline ccs::types::int32 FindJSONAttributeName (const ccs::types::char8 * const buffer)
{

  ccs::types::int32 ret = ccs::HelperTools::FindJSONAttributeStart(buffer);

  bool status = (-1 != ret);
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("FindJSONAttributeName - Try and parse '%s'", buffer);
    }
#endif
  if (status)
    {
      ret++;
    }

  if (!status)
    {
      ret = -1;
    }

  return ret;

}

static inline bool GetJSONAttributeName (const ccs::types::char8 * const buffer, ccs::types::char8 * const name, const ccs::types::uint32 size) { return CopyJSONObjectElementName(buffer, name, size); } 

static inline ccs::types::int32 FindJSONAttributeValue (const ccs::types::char8 * const buffer)
{

  ccs::types::int32 ret = -1;

  ccs::types::uint32 start = 0u;
  ccs::types::uint32 close = 0u;

  bool status = FindJSONObjectElementValue(buffer, start, close);

  if (status)
    {
      ret = static_cast<ccs::types::int32>(start);
    }

  return ret;

}

static inline ccs::types::int32 FindJSONAttributeEnd (const ccs::types::char8 * const buffer) { return FindJSONObjectElementEnd(buffer); } // Last character of the value

static inline bool GetJSONAttributeValue (const ccs::types::char8 * const buffer, ccs::types::char8 * const value, const ccs::types::uint32 size)
{

  bool status = ((NULL_PTR_CAST(const ccs::types::char8*) != buffer) &&
                 (NULL_PTR_CAST(ccs::types::char8*) != value));

  ccs::types::int32 begin = FindJSONAttributeValue(buffer);
  ccs::types::int32 close = FindJSONAttributeEnd(buffer);

  if (status)
    {
      status = ((-1 != begin) && (close >= begin)); // WARNING - (close == begin) in case of error OR 1 digit value
    }

  if (status)
    {
      close++;
      ccs::types::uint32 copy = static_cast<ccs::types::uint32>(close - begin) + 1u;

      if (copy > size) // Output buffer size is sufficient
        {
          copy = size;
        }

      (void)ccs::HelperTools::SafeStringCopy(value, buffer+static_cast<ccs::types::uint32>(begin), copy);
    }
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("GetJSONAttributeValue - Found '%s'", value);
    }
#endif
  return status;

}

static inline ccs::types::int32 GetJSONAttributePair (const ccs::types::char8 * const buffer, ccs::types::char8 * const attr, const ccs::types::uint32 size)
{

  ccs::types::int32 ret = 0;

  bool status = (NULL_PTR_CAST(const ccs::types::char8*) != buffer);
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("GetJSONAttributePair - Try and parse '%s'", buffer);
    }
#endif
  ccs::types::int32 begin = -1;
  ccs::types::int32 close = -1;

  if (status) // p_buf points to '"'
    {
      begin = ccs::HelperTools::FindJSONAttributeStart(buffer);
      close = ccs::HelperTools::FindJSONAttributeEnd(buffer);
      status = ((-1 != begin) && (-1 != close));
    }

  if (status)
    {
      close++;
      ret = close;
    }

  if (status)
    {
      status = (static_cast<ccs::types::uint32>(close - begin) < size);
    }

  if (status)
    {
      (void)ccs::HelperTools::SafeStringCopy(attr, buffer + static_cast<ccs::types::uint32>(begin), static_cast<ccs::types::uint32>(close - begin) + 1u);
    }

  if (!status)
    {
      ret = -1;
    }

  return ret;

}

static inline bool GetAttributeFromJSONContent (const ccs::types::char8 * const buffer, const ccs::types::char8 * const name, ccs::types::char8 * const attr, const ccs::types::uint32 size)
{

  return CopyJSONObjectElement(buffer, name, attr, size);

}

} // namespace HelperTools

} // namespace ccs

#endif // _JSONTools_h_ 
