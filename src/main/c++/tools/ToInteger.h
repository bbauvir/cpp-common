/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project       : CODAC Core System
*
* Description   : Helper routines
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

#ifndef _ToInteger_h_
#define _ToInteger_h_

// Global header files

#include <stdlib.h> // strtoul, etc.

// Local header files

#include "BasicTypes.h" // Misc. type definition

#include "StringTools.h" // Misc. helper routines

// Constants

// Type definition

#ifdef __cplusplus

namespace ccs {

namespace HelperTools {

// Function declaration

template <typename Type> inline Type ToInteger (const ccs::types::char8 * const buffer);

// Global variables

// Function definition

template <> inline ccs::types::int32 ToInteger<ccs::types::int32> (const ccs::types::char8 * const buffer)
{

  ccs::types::int32 ret = 0;

  bool status = (true == ccs::HelperTools::IsIntegerString(buffer));

  if (status)
    {
      ccs::types::char8* p = NULL_PTR_CAST(ccs::types::char8*);
      ret = static_cast<ccs::types::int32>(strtol(buffer, &p, 10));
    }

  return ret;

}

template <> inline ccs::types::uint32 ToInteger<ccs::types::uint32> (const ccs::types::char8 * const buffer)
{

  ccs::types::uint32 ret = 0u;

  bool status = (true == ccs::HelperTools::IsIntegerString(buffer));

  if (status)
    {
      ccs::types::char8* p = NULL_PTR_CAST(ccs::types::char8*);
      ret = static_cast<ccs::types::uint32>(strtoul(buffer, &p, 10));
    }

  return ret;

}

template <> inline ccs::types::int64 ToInteger<ccs::types::int64> (const ccs::types::char8 * const buffer)
{

  ccs::types::int64 ret = 0l;

  bool status = (true == ccs::HelperTools::IsIntegerString(buffer));

  if (status)
    {
      ccs::types::char8* p = NULL_PTR_CAST(ccs::types::char8*);
      ret = static_cast<ccs::types::int64>(strtol(buffer, &p, 10));
    }

  return ret;

}

template <> inline ccs::types::uint64 ToInteger<ccs::types::uint64> (const ccs::types::char8 * const buffer)
{

  ccs::types::uint64 ret = 0ul;

  bool status = (true == ccs::HelperTools::IsIntegerString(buffer));

  if (status)
    {
      ccs::types::char8* p = NULL_PTR_CAST(ccs::types::char8*);
      ret = static_cast<ccs::types::uint64>(strtoul(buffer, &p, 10));
    }

  return ret;

}

} // namespace HelperTools

} // namespace ccs

#endif // __cplusplus

#endif // _ToInteger_h_

