/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "StringTools.h" // Misc. helper functions
#include "TimeTools.h" // Misc. helper functions

#include "log-api.h" // Syslog wrapper routines

#include "PVMonitorUDP.h" // MCAST transort plug-in .. for test purposes

// Constants

#define PVServer_Channel_Name "CCS::TEST::PVNAME"

// Type definition

class PVMonitorHandler : public ccs::base::PVMonitor 
{
  
  private:

    ccs::types::AnyValue _value; // Placeholder
    bool _error = false;

  public:

    PVMonitorHandler (void) : ccs::base::PVMonitor(PVServer_Channel_Name) {};
    virtual ~PVMonitorHandler (void) {};
  
    // PV monitor handler callback
    virtual void HandleEvent (const ccs::base::PVMonitor::Event& event); // Specialises virtual method
    virtual void HandleMonitor (const ccs::types::AnyValue& value); // Specialises virtual method

    const ccs::types::AnyValue GetValue (void) const { return _value; };
    void SetError (bool err) { _error = err; };

};
  
// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();

static ccs::test::PVServerUDP* __server = static_cast<ccs::test::PVServerUDP*>(NULL);

// Function definition

static inline bool Initialise (void) // PVMonitorService plug-in implementations are statically registered .. do not assume this is done at this time
{

  bool status = (static_cast<ccs::test::PVServerUDP*>(NULL) != __server);

  if (!status)
    {
      __server = new (std::nothrow) ccs::test::PVServerUDP ();
      status = (static_cast<ccs::test::PVServerUDP*>(NULL) != __server);
#if 0 // Implicit
      if (status)
	{
	  status = __server->Initialise();
	}
#endif
    }
  
  return status;
  
}

void PVMonitorHandler::HandleEvent (const ccs::base::PVMonitor::Event& event)
{

  // Call default implementation
  ccs::base::PVMonitor::HandleEvent(event);

  if (_error)
    {
      throw std::runtime_error("(Event callback) Some exception happened");
    }

}

void PVMonitorHandler::HandleMonitor (const ccs::types::AnyValue& value)
{

  ccs::types::char8 buffer [1024] = STRING_UNDEFINED;

  (void)value.SerialiseInstance(buffer, 1024u);

  log_info("PVMonitorHandler::HandleMonitor('%s') - Received update '%s'", ccs::base::PVMonitor::GetChannel(), buffer);

  if (_error)
    {
      throw std::runtime_error("(Monitor callback) Some exception happened");
    }

  // Store for future test
  _value = value;

  // Future use-cases .. access plug-in specific attributes
  const ccs::test::PVMonitorUDP* impl = dynamic_cast<const ccs::test::PVMonitorUDP*>(ccs::base::PVMonitor::GetImplementation());

  if (NULL_PTR_CAST(ccs::test::PVMonitorUDP*) != impl)
    {
      log_info("PVMonitorHandler::HandleMonitor('%s') - Accessing plug-in reference", ccs::base::PVMonitor::GetChannel());
    }
  else
    {
      log_error("PVMonitorHandler::HandleMonitor('%s') - Invalid plug-in reference", ccs::base::PVMonitor::GetChannel());
    }

  return;

}

TEST(PVMonitor_Test, Constructor) // Static initialisation
{
  bool ret = Initialise();

  if (ret)
    {
      PVMonitorHandler __handler;
    }

  ASSERT_EQ(true, ret);
}

TEST(PVMonitor_Test, GetService)
{
  bool ret = Initialise();

  if (ret)
    {
      PVMonitorHandler __handler;

      std::string channel (__handler.GetChannel());
      ret = (channel == PVServer_Channel_Name);
    }

  ASSERT_EQ(true, ret);
}

TEST(PVMonitor_Test, SetParameter_string)
{
  bool ret = Initialise();

  if (ret)
    {
      PVMonitorHandler __handler;
      ret = __handler.SetParameter("string", "value");
    }

  ASSERT_EQ(true, ret);
}

TEST(PVMonitor_Test, SetParameter_any)
{
  bool ret = Initialise();

  if (ret)
    {
      PVMonitorHandler __handler;

      ccs::types::AnyValue value ("{\"type\":\"myTestStruct\",\"attributes\":[{\"value\":{\"type\":\"bool\"}}]}");
      value = true;

      ret = __handler.SetParameter("boolean", value);
    }

  ASSERT_EQ(true, ret);
}

TEST(PVMonitor_Test, HandleMonitor_default)
{
  bool ret = Initialise();

  PVMonitorHandler __handler;

  if (ret)
    {
      ccs::types::AnyValue value ("{\"type\":\"myTestStruct\",\"attributes\":[{\"value\":{\"type\":\"bool\"}}]}");
      value = true;

      ret = __server->Publish(value);
    }

  if (ret)
    {
      (void)ccs::HelperTools::SleepFor(1000000000ul);
    }

  if (ret)
    { // Test received value
      ret = (true == static_cast<bool>(__handler.GetValue()));
    }

  ASSERT_EQ(true, ret);
}

TEST(PVMonitor_Test, HandleMonitor_error)
{
  bool ret = Initialise();

  PVMonitorHandler __handler;
  __handler.SetError(true);

  if (ret)
    {
      ccs::types::AnyValue value ("{\"type\":\"myTestStruct\",\"attributes\":[{\"value\":{\"type\":\"bool\"}}]}");
      value = true;

      ret = __server->Publish(value);
    }

  if (ret)
    {
      (void)ccs::HelperTools::SleepFor(1000000000ul);
    }

  if (ret)
    { // Test received value is invalid
      ret = (false == static_cast<bool>(__handler.GetValue().GetType()));
    }

  ASSERT_EQ(true, ret);
}

