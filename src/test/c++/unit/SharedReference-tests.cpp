/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "tools.h" // Misc. helper functions

#include "log-api.h" // Syslog wrapper routines

#include "SharedReference.h"

// Constants

// Type definition

class MyRootClass
{

  public:

  MyRootClass (void);
  virtual ~MyRootClass (void);

};

class MyDerivedClass : public MyRootClass
{

  public:

  MyDerivedClass (void);
  MyDerivedClass (const MyDerivedClass& org);
  virtual ~MyDerivedClass (void);

};

class MyOtherClass
{

  public:

  MyOtherClass (void);
  virtual ~MyOtherClass (void);

};

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();

static ccs::types::uint32 __root = 0u;
static ccs::types::uint32 __derived = 0u;
static ccs::types::uint32 __other = 0u;

// Function declaration

// Function definition

MyRootClass::MyRootClass (void) { __root++; }
MyRootClass::~MyRootClass (void) { __root--; }

MyDerivedClass::MyDerivedClass (void) { __derived++; }
MyDerivedClass::MyDerivedClass (const MyDerivedClass& org) { __derived++; };
MyDerivedClass::~MyDerivedClass (void) { __derived--; }

MyOtherClass::MyOtherClass (void) { __other++; }
MyOtherClass::~MyOtherClass (void) { __other--; }

TEST(SharedReference_Test, Constructor_default)
{
  ccs::base::SharedReference<ccs::types::uint32> ref;

  bool ret = (!static_cast<bool>(ref) && !ref.IsValid() && (NULL_PTR_CAST(ccs::types::uint32*) == ref.GetReference()) && (NULL_PTR_CAST(ccs::base::Counter*) == ref.GetCounter()));

  ASSERT_EQ(ret, true);
}

TEST(SharedReference_Test, Constructor_reference)
{
  ccs::base::SharedReference<ccs::types::uint32> ref (new (std::nothrow) ccs::types::uint32 (1u));

  bool ret = (static_cast<bool>(ref) && ref.IsValid() && (NULL_PTR_CAST(ccs::types::uint32*) != ref.GetReference()) && (NULL_PTR_CAST(ccs::base::Counter*) != ref.GetCounter()));

  if (ret)
    {
      ret = (1u == (ref.GetCounter())->GetCount());
    }

  if (ret)
    {
      ret = (1u == *(ref.GetReference()));
    }

  ASSERT_EQ(ret, true);
}

TEST(SharedReference_Test, Constructor_copy)
{
  ccs::base::SharedReference<ccs::types::uint32> ref1 (new (std::nothrow) ccs::types::uint32 (1u));
  ccs::base::SharedReference<ccs::types::uint32> ref (ref1);

  bool ret = (static_cast<bool>(ref) && ref.IsValid() && (NULL_PTR_CAST(ccs::types::uint32*) != ref.GetReference()) && (NULL_PTR_CAST(ccs::base::Counter*) != ref.GetCounter()));

  if (ret)
    {
      ret = (2u == (ref.GetCounter())->GetCount());
    }

  if (ret)
    {
      ret = (1u == *(ref.GetReference()));
    }

  ASSERT_EQ(ret, true);
}

TEST(SharedReference_Test, Constructor_const)
{
  ccs::base::SharedReference<ccs::types::uint32> ref1 (new (std::nothrow) ccs::types::uint32 (1u));
  ccs::base::SharedReference<const ccs::types::uint32> ref (const_cast<const ccs::types::uint32*>(ref1.GetReference()), ref1.GetCounter());

  bool ret = (static_cast<bool>(ref) && ref.IsValid() && (NULL_PTR_CAST(ccs::types::uint32*) != ref.GetReference()) && (NULL_PTR_CAST(ccs::base::Counter*) != ref.GetCounter()));

  if (ret)
    {
      ret = (2u == (ref.GetCounter())->GetCount());
    }

  if (ret)
    {
      ret = (1u == *(ref.GetReference()));
    }

  ASSERT_EQ(ret, true);
}

TEST(SharedReference_Test, Constructor_RTTI)
{
  MyRootClass* root = new (std::nothrow) MyRootClass;

  bool ret = (1u == __root);

  if (ret)
    {
      ccs::base::SharedReference<MyRootClass> ref (root);
      // Going out of scope
    }

  if (ret)
    {
      ret = (0u == __root); // Silently deleted

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Constructor_RTTI) - Expected instance count '%u'", __root);
	}
    }

  ccs::base::SharedReference<MyDerivedClass> derived (new (std::nothrow) MyDerivedClass);

  if (ret)
    {
      ret = (1u == __derived);

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Constructor_RTTI) - Expected instance count '%u'", __derived);
	}
    }

  if (ret)
    {
      ret = (1u == (derived.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Constructor_RTTI) - Expected reference count '%u'", (derived.GetCounter())->GetCount());
	}
    }

  if (ret)
    {
      ccs::base::SharedReference<MyRootClass> ref (derived);
      ret = (2u == (ref.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Constructor_RTTI) - Expected reference count '%u'", (ref.GetCounter())->GetCount());
	}
    }

  if (ret)
    {
      ret = (1u == (derived.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Constructor_RTTI) - Expected reference count '%u'", (derived.GetCounter())->GetCount());
	}
    }

  ccs::base::SharedReference<MyOtherClass> other (new (std::nothrow) MyOtherClass);

  if (ret)
    {
      ret = (1u == __other);

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Constructor_RTTI) - Expected instance count '%u'", __other);
	}
    }

  if (ret)
    {
      ret = (1u == (other.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Constructor_RTTI) - Expected reference count '%u'", (other.GetCounter())->GetCount());
	}
    }

  if (ret)
    {
      ccs::base::SharedReference<MyRootClass> ref (other);
      ret = (!static_cast<bool>(ref) && (1u == (other.GetCounter())->GetCount()));

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Constructor_RTTI) - Expected invalid reference");
	}
    }

  if (ret)
    {
      ret = (1u == (other.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Constructor_RTTI) - Expected reference count '%u'", (other.GetCounter())->GetCount());
	}
    }

  ASSERT_EQ(ret, true);
}

TEST(SharedReference_Test, Constructor_instance)
{
  ccs::base::SharedReference<MyRootClass> ref;

  bool ret = (0u == __root);

  if (ret)
    {
      MyDerivedClass instance;
      ref = ccs::base::SharedReference<MyRootClass>(ccs::base::SharedReference<MyDerivedClass>(new (std::nothrow) MyDerivedClass (instance)));
      ret = ((1u == ref.GetCount()) && (2u == __derived));
    }

  if (ret)
    {
      ret = ((1u == ref.GetCount()) && (1u == __derived));
    }

  ASSERT_EQ(ret, true);
}

TEST(SharedReference_Test, Assignment_Default)
{
  ccs::base::SharedReference<ccs::types::uint32> ref (new (std::nothrow) ccs::types::uint32 (0u));
  ccs::base::SharedReference<ccs::types::uint32> ref1 (new (std::nothrow) ccs::types::uint32 (1u));

  bool ret = (static_cast<bool>(ref) && ref.IsValid() && (NULL_PTR_CAST(ccs::types::uint32*) != ref.GetReference()) && (NULL_PTR_CAST(ccs::base::Counter*) != ref.GetCounter()));

  if (ret)
    {
      ref = ref1;
    }

  if (ret)
    {
      ret = (2u == (ref.GetCounter())->GetCount());
    }

  if (ret)
    {
      ret = (1u == *(ref.GetReference()));
    }

  ASSERT_EQ(ret, true);
}

TEST(SharedReference_Test, Assignment_RTTI)
{
  ccs::base::SharedReference<MyRootClass> ref;

  bool ret = !static_cast<bool>(ref);

  ccs::base::SharedReference<MyDerivedClass> derived (new (std::nothrow) MyDerivedClass);

  if (ret)
    {
      ret = (1u == __derived);

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Assignment_RTTI) - Expected instance count '%u'", __derived);
	}
    }

  if (ret)
    {
      ret = (1u == (derived.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Assignment_RTTI) - Expected reference count '%u'", (derived.GetCounter())->GetCount());
	}
    }

  if (ret)
    {
      ref = derived;
      ret = (2u == (ref.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Assignment_RTTI) - Expected 'derived' reference count '%u'", (ref.GetCounter())->GetCount());
	}
    }

  ccs::base::SharedReference<MyOtherClass> other (new (std::nothrow) MyOtherClass);

  if (ret)
    {
      ret = (1u == __other);

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Assignment_RTTI) - Expected instance count '%u'", __other);
	}
    }

  if (ret)
    {
      ret = (1u == (other.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Assignment_RTTI) - Expected 'other' reference count '%u'", (other.GetCounter())->GetCount());
	}
    }

  if (ret)
    {
      ref = other;
      ret = (!static_cast<bool>(ref) && (1u == (other.GetCounter())->GetCount()));

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Assignment_RTTI) - Expected invalid reference");
	}
    }

  if (ret)
    {
      ret = (1u == (derived.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Assignment_RTTI) - Expected 'derived' reference count '%u'", (derived.GetCounter())->GetCount());
	}
    }

  if (ret)
    {
      ret = (1u == (other.GetCounter())->GetCount());

      if (!ret)
	{
	  log_error("TEST(SharedReference_Test, Assignment_RTTI) - Expected 'other' reference count '%u'", (other.GetCounter())->GetCount());
	}
    }

  ASSERT_EQ(ret, true);
}

TEST(SharedReference_Test, ZZZZ)
{
  bool ret = ((0u == __root) && (0u == __derived) && (0u == __other));

  ASSERT_EQ(ret, true);
}
 
