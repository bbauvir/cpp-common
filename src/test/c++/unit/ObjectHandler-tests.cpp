/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

// Local header files

#include "BasicTypes.h" // Misc. type definition
#include "SysTools.h" // Misc. helper functions

#include "log-api.h" // Syslog wrapper routines

#include "AnyObject.h"
#include "ObjectDatabase.h"
#include "ObjectFactory.h"

#include "ObjectHandler.h"

// Constants

// Type definition

class MyAnyObject : public ccs::base::AnyObject {
  
  private:

  public:
  
    MyAnyObject (void) {};
    virtual ~MyAnyObject (void) {};
  
};
  
class MyCfgableObject : public ccs::base::AnyObject, public ccs::base::CfgableObject {
  
  private:

    ccs::types::uint32 __value = 0u;

  public:
  
    MyCfgableObject (void) {};
    virtual ~MyCfgableObject (void) {};

    ccs::types::uint32 GetValue (void) { return __value; };

    virtual bool SetParameter (const char* name, const char* value) {

      log_info("MyCfgableObject::SetParameter('%s, %s')", name, value);

      bool status = true;

      std::string __name (name);
 
      if (__name == "incr") __value += 1u; 
      if (__name == "reset") __value = 0u; 
      if (__name == "false") status = false; 
      if (__name == "value")
	{
	  status = (0 < sscanf(value, "%u", &__value));
	} 

      return status;

    };
  
};
  
class MyMsgableObject : public ccs::base::AnyObject, public  ccs::base::MsgableObject {
  
  private:

    ccs::types::uint32 __count = 0u;

  public:
  
    MyMsgableObject (void) {};
    virtual ~MyMsgableObject (void) {};

    ccs::types::uint32 GetCount (void) { return __count; };

    virtual bool ProcessMessage (const char* msg) {

      log_info("MyMsgableObject::ProcessMessage('%s')", msg);

      bool status = true;
 
      if (std::string(msg) == "incr") __count += 1u; 
      if (std::string(msg) == "reset") __count = 0u; 
      if (std::string(msg) == "false") status = false; 

      return status;

    };
  
};
  
class MyAnyableObject : public ccs::base::AnyObject, public  ccs::base::CfgableObject, public  ccs::base::MsgableObject {
  
  private:

    ccs::types::uint32 __count = 0u;
    ccs::types::uint32 __value = 0u;

  public:
  
    MyAnyableObject (void) {};
    virtual ~MyAnyableObject (void) {};

    ccs::types::uint32 GetCount (void) { return __count; };
    ccs::types::uint32 GetValue (void) { return __value; };

    virtual bool SetParameter (const char* name, const char* value) {

      log_info("MyAnyableObject::SetParameter('%s, %s')", name, value);

      bool status = true;

      std::string __name (name);
 
      if (__name == "incr") __value += 1u; 
      if (__name == "reset") __value = 0u; 
      if (__name == "false") status = false; 
      if (__name == "value")
	{
	  status = (0 < sscanf(value, "%u", &__value));
	} 

      return status;

    };
  
    virtual bool ProcessMessage (const char* msg) {

      log_info("MyAnyableObject::ProcessMessage('%s')", msg);

      bool status = true;
 
      std::string __msg (msg);
 
      if (__msg == "incr") __count += 1u; 
      if (__msg == "reset") __count = 0u; 
      if (__msg == "false") status = false; 

      return status;

    };
  
};
  
// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();

REGISTER_CLASS_WITH_OBJECT_FACTORY(MyAnyObject) // Auto-register constructor with the ObjectFactory
REGISTER_CLASS_WITH_OBJECT_FACTORY(MyCfgableObject) // Auto-register constructor with the ObjectFactory
REGISTER_CLASS_WITH_OBJECT_FACTORY(MyMsgableObject) // Auto-register constructor with the ObjectFactory
REGISTER_CLASS_WITH_OBJECT_FACTORY(MyAnyableObject) // Auto-register constructor with the ObjectFactory

// Function definition
  
TEST(ObjectHandler_Test, Constructor)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}

TEST(ObjectHandler_Test, LoadLibrary_success)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = handler->LoadLibrary("librt.so");
    }

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}

TEST(ObjectHandler_Test, LoadLibrary_error)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = !handler->LoadLibrary("undefined");
    }

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}


TEST(ObjectHandler_Test, IsTypeValid)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = (handler->IsTypeValid("MyAnyObject") &&
	     handler->IsTypeValid("MyCfgableObject") &&
	     handler->IsTypeValid("MyMsgableObject") &&
	     handler->IsTypeValid("MyAnyableObject"));
    }

  if (ret)
    {
      ret = (!handler->IsTypeValid(NULL) &&
	     !handler->IsTypeValid("") &&
	     !handler->IsTypeValid("UndefinedType"));
    }

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}

TEST(ObjectHandler_Test, Instantiate)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = (handler->IsTypeValid("MyAnyObject") &&
	     handler->IsTypeValid("MyCfgableObject") &&
	     handler->IsTypeValid("MyMsgableObject") &&
	     handler->IsTypeValid("MyAnyableObject"));
    }

  if (ret)
    {
      ret = (handler->Instantiate("MyAnyObject","MyAnyObject") &&
	     handler->Instantiate("MyCfgableObject","MyCfgableObject") &&
	     handler->Instantiate("MyMsgableObject","MyMsgableObject") &&
	     handler->Instantiate("MyAnyableObject","MyAnyableObject"));
    }

  if (ret)
    {
      ret = (!handler->Instantiate(NULL,NULL) &&
	     !handler->Instantiate("","UndefinedObject") &&
	     !handler->Instantiate("MyAnyObject",""));
    }

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}

TEST(ObjectHandler_Test, IsValid)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = (handler->Instantiate("MyAnyObject","MyAnyObject") &&
	     handler->Instantiate("MyCfgableObject","MyCfgableObject") &&
	     handler->Instantiate("MyMsgableObject","MyMsgableObject") &&
	     handler->Instantiate("MyAnyableObject","MyAnyableObject"));
    }

  if (ret)
    {
      ret = (handler->IsValid("MyAnyObject") &&
	     handler->IsValid("MyCfgableObject") &&
	     handler->IsValid("MyMsgableObject") &&
	     handler->IsValid("MyAnyableObject"));
    }

  if (ret)
    {
      ret = (!handler->IsValid(static_cast<const char*>(NULL)) &&
	     !handler->IsValid("") &&
	     !handler->IsValid("UndefinedObject"));
    }

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}

TEST(ObjectHandler_Test, IsInstanceOf)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = (handler->Instantiate("MyAnyObject","MyAnyObject") &&
	     handler->Instantiate("MyCfgableObject","MyCfgableObject") &&
	     handler->Instantiate("MyMsgableObject","MyMsgableObject") &&
	     handler->Instantiate("MyAnyableObject","MyAnyableObject"));
    }

  if (ret)
    {
      ret = (handler->IsInstanceOf<ccs::base::AnyObject>("MyAnyObject") &&
	     handler->IsInstanceOf<ccs::base::AnyObject>("MyCfgableObject") &&
	     handler->IsInstanceOf<ccs::base::AnyObject>("MyMsgableObject") &&
	     handler->IsInstanceOf<ccs::base::AnyObject>("MyAnyableObject"));
    }

  if (ret)
    {
      ret = (!handler->IsInstanceOf<ccs::base::CfgableObject>("MyAnyObject") &&
	     handler->IsInstanceOf<ccs::base::CfgableObject>("MyCfgableObject") &&
	     !handler->IsInstanceOf<ccs::base::CfgableObject>("MyMsgableObject") &&
	     handler->IsInstanceOf<ccs::base::CfgableObject>("MyAnyableObject"));
    }

  if (ret)
    {
      ret = (!handler->IsInstanceOf<ccs::base::MsgableObject>("MyAnyObject") &&
	     !handler->IsInstanceOf<ccs::base::MsgableObject>("MyCfgableObject") &&
	     handler->IsInstanceOf<ccs::base::MsgableObject>("MyMsgableObject") &&
	     handler->IsInstanceOf<ccs::base::MsgableObject>("MyAnyableObject"));
    }

  if (ret)
    {
      ret = (handler->IsInstanceOf<MyAnyObject>("MyAnyObject") &&
	     handler->IsInstanceOf<MyCfgableObject>("MyCfgableObject") &&
	     handler->IsInstanceOf<MyMsgableObject>("MyMsgableObject") &&
	     handler->IsInstanceOf<MyAnyableObject>("MyAnyableObject"));
    }

  if (ret)
    {
      ret = (!handler->IsInstanceOf<ccs::base::AnyObject>(NULL) &&
	     !handler->IsInstanceOf<ccs::base::AnyObject>("") &&
	     !handler->IsInstanceOf<ccs::base::AnyObject>("UndefinedObject"));
    }

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}

TEST(ObjectHandler_Test, GetInstance)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = (handler->Instantiate("MyAnyObject","MyAnyObject") &&
	     handler->Instantiate("MyCfgableObject","MyCfgableObject") &&
	     handler->Instantiate("MyMsgableObject","MyMsgableObject") &&
	     handler->Instantiate("MyAnyableObject","MyAnyableObject"));
    }

  if (ret)
    {
      ret = ((static_cast<ccs::base::AnyObject*>(NULL) != handler->GetInstance<ccs::base::AnyObject>("MyAnyObject")) &&
	     (static_cast<ccs::base::AnyObject*>(NULL) != handler->GetInstance<ccs::base::AnyObject>("MyCfgableObject")) &&
	     (static_cast<ccs::base::AnyObject*>(NULL) != handler->GetInstance<ccs::base::AnyObject>("MyMsgableObject")) &&
	     (static_cast<ccs::base::AnyObject*>(NULL) != handler->GetInstance<ccs::base::AnyObject>("MyAnyableObject")));
    }

  if (ret)
    {
      ret = ((static_cast<ccs::base::CfgableObject*>(NULL) == handler->GetInstance<ccs::base::CfgableObject>("MyAnyObject")) &&
	     (static_cast<ccs::base::CfgableObject*>(NULL) != handler->GetInstance<ccs::base::CfgableObject>("MyCfgableObject")) &&
	     (static_cast<ccs::base::CfgableObject*>(NULL) == handler->GetInstance<ccs::base::CfgableObject>("MyMsgableObject")) &&
	     (static_cast<ccs::base::CfgableObject*>(NULL) != handler->GetInstance<ccs::base::CfgableObject>("MyAnyableObject")));
    }

  // Etc.

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}

TEST(ObjectHandler_Test, Terminate)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = (handler->Instantiate("MyAnyObject","MyAnyObject") &&
	     handler->Instantiate("MyCfgableObject","MyCfgableObject") &&
	     handler->Instantiate("MyMsgableObject","MyMsgableObject") &&
	     handler->Instantiate("MyAnyableObject","MyAnyableObject"));
    }

  if (ret)
    {
      ret = (handler->IsValid("MyAnyObject") &&
	     handler->IsValid("MyCfgableObject") &&
	     handler->IsValid("MyMsgableObject") &&
	     handler->IsValid("MyAnyableObject"));
    }

  // Should have been all regsitered to the GODB

  if (ret)
    {
      ret = ccs::base::GlobalObjectDatabase::IsValid("MyAnyObject"); // Etc.
    }

  if (ret)
    {
      ret = handler->Terminate("MyAnyObject");
    }

  if (ret)
    {
      ret = (!handler->IsValid("MyAnyObject") &&
	     handler->IsValid("MyCfgableObject") &&
	     handler->IsValid("MyMsgableObject") &&
	     handler->IsValid("MyAnyableObject"));
    }

  if (ret)
    {
      delete handler;
    }

  // Should have been all deleted from the GODB

  if (ret)
    {
      ret = !ccs::base::GlobalObjectDatabase::IsValid("MyAnyObject"); // Etc.
    }

  ASSERT_EQ(true, ret);
}

TEST(ObjectHandler_Test, SetParameter)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  log_info("TEST(ObjectHandler_Test, SetParameter) - ccs::base::ObjectHandler instance '%p'", handler);

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = (handler->Instantiate("MyAnyObject","MyAnyObject") &&
	     handler->Instantiate("MyCfgableObject","MyCfgableObject") &&
	     handler->Instantiate("MyMsgableObject","MyMsgableObject") &&
	     handler->Instantiate("MyAnyableObject","MyAnyableObject"));
    }

  if (ret)
    {
      ret = (!handler->SetParameter("MyAnyObject","name","value") &&
	     handler->SetParameter("MyCfgableObject","name","value") &&
	     !handler->SetParameter("MyMsgableObject","name","value") &&
	     handler->SetParameter("MyAnyableObject","name","value"));
    }

  if (ret)
    {
      ret = ((handler->SetParameter("MyCfgableObject","value","1234") &&
	      (1234u == handler->GetInstance<MyCfgableObject>("MyCfgableObject")->GetValue())) &&
	     (handler->SetParameter("MyAnyableObject","value","1234") &&
	      (1234u == handler->GetInstance<MyAnyableObject>("MyAnyableObject")->GetValue())));
    }

  if (ret)
    {
      ret = ((handler->SetParameter("MyCfgableObject","reset","") &&
	      (0u == handler->GetInstance<MyCfgableObject>("MyCfgableObject")->GetValue())) &&
	     (handler->SetParameter("MyAnyableObject","reset","") &&
	      (0u == handler->GetInstance<MyAnyableObject>("MyAnyableObject")->GetValue())));
    }

  if (ret)
    {
      ret = ((handler->SetParameter("MyCfgableObject","incr","") &&
	      (1u == handler->GetInstance<MyCfgableObject>("MyCfgableObject")->GetValue())) &&
	     (handler->SetParameter("MyAnyableObject","incr","") &&
	      (1u == handler->GetInstance<MyAnyableObject>("MyAnyableObject")->GetValue())));
    }

  if (ret)
    {
      ret = (!handler->SetParameter("MyCfgableObject","false","") &&
	     !handler->SetParameter("MyAnyableObject","false",""));
    }

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}

TEST(ObjectHandler_Test, ProcessMessage)
{
  ccs::base::ObjectHandler* handler = new (std::nothrow) ccs::base::ObjectHandler ();

  bool ret = (static_cast<ccs::base::ObjectHandler*>(NULL) != handler);

  if (ret)
    {
      ret = (handler->Instantiate("MyAnyObject","MyAnyObject") &&
	     handler->Instantiate("MyCfgableObject","MyCfgableObject") &&
	     handler->Instantiate("MyMsgableObject","MyMsgableObject") &&
	     handler->Instantiate("MyAnyableObject","MyAnyableObject"));
    }

  if (ret)
    {
      ret = (!handler->ProcessMessage("MyAnyObject","msg") &&
	     !handler->ProcessMessage("MyCfgableObject","msg") &&
	     handler->ProcessMessage("MyMsgableObject","msg") &&
	     handler->ProcessMessage("MyAnyableObject","msg"));
    }

  if (ret)
    {
      ret = ((handler->ProcessMessage("MyMsgableObject","reset") &&
	      (0u == handler->GetInstance<MyMsgableObject>("MyMsgableObject")->GetCount())) &&
	     (handler->ProcessMessage("MyAnyableObject","reset") &&
	      (0u == handler->GetInstance<MyAnyableObject>("MyAnyableObject")->GetCount())));
    }

  if (ret)
    {
      ret = ((handler->ProcessMessage("MyMsgableObject","incr") &&
	      (1u == handler->GetInstance<MyMsgableObject>("MyMsgableObject")->GetCount())) &&
	     (handler->ProcessMessage("MyAnyableObject","incr") &&
	      (1u == handler->GetInstance<MyAnyableObject>("MyAnyableObject")->GetCount())));
    }

  if (ret)
    {
      ret = ((handler->ProcessMessage("MyMsgableObject","reset") &&
	      (0u == handler->GetInstance<MyMsgableObject>("MyMsgableObject")->GetCount())) &&
	     (handler->ProcessMessage("MyAnyableObject","reset") &&
	      (0u == handler->GetInstance<MyAnyableObject>("MyAnyableObject")->GetCount())));
    }

  if (ret)
    {
      ret = (!handler->ProcessMessage("MyMsgableObject","false") &&
	     !handler->ProcessMessage("MyAnyableObject","false"));
    }

  if (ret)
    {
      delete handler;
    }

  ASSERT_EQ(true, ret);
}

