/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVMonitorUDP.h
 * @brief Header file for PVMonitorUDP class.
 * @date 01/07/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @detail This header file contains the definition of the PVMonitorUDP class.
 */

#ifndef _PVMonitorUDP_h_
#define _PVMonitorUDP_h_

// Global header files

// Local header files

#include "BasicTypes.h"

#include "AnyThread.h"

#include "PVMonitorImpl.h"

// Constants

namespace ccs {

namespace test {

// Type definition

class ParticipantImpl
{

  private:

    ccs::types::char8 __if_addr [ccs::types::MaxIPv4AddrLength];
    ccs::types::char8 __if_name [ccs::types::MaxIPv4AddrLength];

    ccs::types::char8 __mcast_group [ccs::types::MaxIPv4AddrLength];
    ccs::types::uint32 __mcast_port;

    int __socket; // Socket handle

    void Initialise (void);

  public:

    const ccs::types::char8* GetAddress (void) const { return __if_addr; };
    const ccs::types::char8* GetInterface (void) const { return __if_name; };
    const ccs::types::char8* GetMCastAddr (void) const { return __mcast_group; };
    ccs::types::uint32 GetMCastPort (void) const { return __mcast_port; };
    int GetSocket (void) const { return __socket; };

    void SetAddress (const ccs::types::char8 * const addr);
    void SetInterface (const ccs::types::char8 * const iface);
    void SetMCastAddr (const ccs::types::char8 * const addr);
    void SetMCastPort (const ccs::types::uint32 port);

    bool Open (void);
    bool Close (void);

    ParticipantImpl (void);
    virtual ~ParticipantImpl (void);

};

class PVMonitorUDP : public ccs::base::PVMonitorImpl, public ParticipantImpl
{

  private:

    ccs::base::SynchronisedThreadWithCallback* m_thread; // __thread causes compilation issues

    bool __initialised;
    bool __connected;

  protected:

  public:

    // Initialiser methods
    virtual bool Initialise (void);
    virtual bool Terminate (void);

    // Accessor methods
    virtual bool IsConnected (void) const;

    // PV monitor parameters
    virtual bool SetParameter (const ccs::types::char8 * const name, const ccs::types::char8 * const value); // Specialises virtual method
    virtual bool SetParameter (const ccs::types::char8 * const name, const ccs::types::AnyValue& value); // Specialises virtual method

    // Miscellaneous methods
    bool Open (void);
    bool Close (void);

    bool Receive (void* buffer, ccs::types::uint32& size, const ccs::types::uint64 timeout) const; // Blocking wait with configurable timeout - Size updated with actual number of bytes received
    bool Receive (void* buffer, ccs::types::uint32& size) const;

    bool Do (void);

    // Constructor methods
    PVMonitorUDP (void);

    // Destructor method
    virtual ~PVMonitorUDP (void); 

};

class PVServerUDP : public ParticipantImpl
{

  private:

    bool __initialised;
               
  protected:

  public:

    // Initialiser methods
    bool Initialise (void);
    bool Terminate (void);

    // Miscellaneous methods
    bool Open (void);
    bool Close (void);

    bool Publish (const void* buffer, const ccs::types::uint32 size) const;
    bool Publish (const ccs::types::AnyValue& value) const;

    // Constructor methods
    PVServerUDP (void);

    // Destructor method
    virtual ~PVServerUDP (void); 

};

// Global variables

const ccs::types::char8 DefaultIfaceName [] = "lo";
const ccs::types::char8 DefaultMCastAddr [] = "239.0.0.1";
const ccs::types::uint32 DefaultMCastPort = 60000u;

// Function declaration

ccs::base::PVMonitorImpl* PVMonitorUDPConstructor (void);

// Function definition

} // namespace test

} // namespace ccs

#endif // _PVMonitorUDP_h_

