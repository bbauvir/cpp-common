/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <memory> // std::shared_ptr
#include <new> // std::nothrow

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <errno.h>

// Local header files

#include "BasicTypes.h" // Global type definition
#include "SysTools.h" // Misc. helper functions
#include "NetTools.h" // Misc. helper functions
#include "CyclicRedundancyCheck.h"

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include "log-api.h" // Syslog wrapper routines

#include "AnyThread.h"

#include "AnyType.h" // Introspectable data type ..
#include "AnyTypeHelper.h" // .. associated helper routines

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

#include "PVMonitorImpl.h"
#include "PVMonitorImplFactory.h"

#include "PVMonitorUDP.h" // This class definition

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "udp-if"

// Type definition

namespace ccs {

namespace test {

// Global variables

static bool __is_registered = (ccs::base::PVMonitorImplFactory::GetInstance()->Register("ccs::base::PVMonitorUDP", &PVMonitorUDPConstructor) &&
			 ccs::base::PVMonitorImplFactory::GetInstance()->SetDefault("ccs::base::PVMonitorUDP"));

// Function declaration

void PVMonitorUDP_CB (PVMonitorUDP* self) { self->Do(); }

// Function definition

void ParticipantImpl::Initialise (void)
{

  log_trace("ParticipantImpl::Initialise - Entering method"); 

  // Initialise attributes
  this->SetAddress(STRING_UNDEFINED);
  this->SetInterface(STRING_UNDEFINED);
  this->SetMCastAddr(STRING_UNDEFINED);
  this->SetMCastPort(0u);

  __socket = -1;

  log_trace("ParticipantImpl::Initialise - Leaving method"); 

  return;

}

void ParticipantImpl::SetAddress (const ccs::types::char8 * const addr) 
{ 

  log_trace("ParticipantImpl::SetAddress - Entering method"); 

  if ((ccs::HelperTools::IsUndefinedString(addr) != true) && (ccs::HelperTools::IsAddressValid(addr) != true)) 
    {
      log_warning("ParticipantImpl::SetAddress - Address '%s' is invalid", addr); 
    }

  ccs::HelperTools::SafeStringCopy(__if_addr, addr, ccs::types::MaxIPv4AddrLength);

  log_trace("ParticipantImpl::SetAddress - Leaving method"); 

  return; 

}

void ParticipantImpl::SetInterface (const ccs::types::char8 * const iface) 
{ 

  log_trace("ParticipantImpl::SetInterface - Entering method"); 

  if ((ccs::HelperTools::IsUndefinedString(iface) != true) && (ccs::HelperTools::IsInterfaceValid(iface) != true)) 
    {
      log_warning("ParticipantImpl::SetInterface - Interface '%s' is invalid", iface); 
    }

  ccs::HelperTools::SafeStringCopy(__if_name, iface, ccs::types::MaxIPv4AddrLength); 

  log_trace("ParticipantImpl::SetInterface - Leaving method"); 

  return; 

}

void ParticipantImpl::SetMCastAddr (const ccs::types::char8 * const addr) 
{ 

  log_trace("ParticipantImpl::SetMCastAddr - Entering method"); 

  if ((ccs::HelperTools::IsUndefinedString(addr) != true) && (ccs::HelperTools::IsAddressValid(addr) != true)) 
    {
      log_warning("ParticipantImpl::SetMCastAddr - Address '%s' is invalid", addr); 
    }

  ccs::HelperTools::SafeStringCopy((ccs::types::char8*) __mcast_group, addr, ccs::types::MaxIPv4AddrLength); 

  log_trace("ParticipantImpl::SetMCastAddr - Leaving method"); 

  return; 

}

void ParticipantImpl::SetMCastPort (const ccs::types::uint32 port) { __mcast_port = port; return; }

bool ParticipantImpl::Open (void)
{

  log_trace("ParticipantImpl::Open - Entering method"); 

  bool status = ((-1 == __socket) && !ccs::HelperTools::IsUndefinedString(__if_name));

  if (status)
    {
      __socket = socket(AF_INET, SOCK_DGRAM, 0);
      status = (-1 != __socket);
    }

  if (status)    
    { // Get address of selected local interface
      struct ifreq if_req;
      strcpy(if_req.ifr_name, __if_name);
      status = (-1 != ioctl(__socket, SIOCGIFADDR, &if_req));

      if (status)
	{
	  log_debug("ParticipantImpl::Open - IP address is '%s'", inet_ntoa(((struct sockaddr_in*)&if_req.ifr_addr)->sin_addr));
	  ccs::HelperTools::SafeStringCopy(__if_addr, inet_ntoa(((struct sockaddr_in*)&if_req.ifr_addr)->sin_addr), ccs::types::MaxIPv4AddrLength);
	}
      else
	{
	  log_error("ParticipantImpl::Open - ioctl(...) failed with '%d - %s'", errno, strerror(errno));
	}

  }

  if (status)    
    { // Set socket options - Attach socket to selected local interface
      struct in_addr if_addr;
      if_addr.s_addr = inet_addr(__if_addr);
      status = (-1 != setsockopt(__socket, IPPROTO_IP, IP_MULTICAST_IF, (ccs::types::char8*) &if_addr, sizeof(if_addr)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  if (status)    
    { // Set socket buffer length
      int length = ccs::types::MaxIPv4PacketSize;
      status = (-1 != setsockopt(__socket, SOL_SOCKET, SO_SNDBUF, (ccs::types::char8*) &length, sizeof(length)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  if (status)    
    { // Set socket buffer length
      int length = ccs::types::MaxIPv4PacketSize;
      status = (-1 != setsockopt(__socket, SOL_SOCKET, SO_RCVBUF, (ccs::types::char8*) &length, sizeof(length)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }
#if 0 // CCP-731 - IP fragmentation error on VM NIC
  if (status)    
    { // Disable UDP checksum
      int disable = 1;
      status = (-1 != setsockopt(__socket, SOL_SOCKET, SO_NO_CHECK, (ccs::types::char8*) &disable, sizeof(disable)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }
#endif
  if (status)    
    { // Set reuse flag to allow for multiple instances running on the same machine/
      int reuse = 1;
      status = (-1 != setsockopt(__socket, SOL_SOCKET, SO_REUSEADDR, (ccs::types::char8*) &reuse, sizeof(reuse)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }
  
  log_trace("ParticipantImpl::Open - Leaving method"); 

  return status;

}

bool ParticipantImpl::Close (void)
{

  log_trace("ParticipantImpl::Close - Entering method"); 

  bool status = (-1 != __socket);

  if (status)
    {
      close(__socket);
    }

  __socket = -1;

  log_trace("ParticipantImpl::Close - Leaving method"); 

  return status;

}

ParticipantImpl::ParticipantImpl (void) 
{ 

  log_trace("ParticipantImpl::ParticipantImpl - Entering method"); 

  // Initialize attributes 
  this->Initialise(); 

  log_trace("ParticipantImpl::ParticipantImpl - Leaving method"); 

  return; 

}

ParticipantImpl::~ParticipantImpl (void)
{

  log_trace("ParticipantImpl::~ParticipantImpl - Entering method"); 

  // Close socket 
  this->Close(); 

  log_trace("ParticipantImpl::~ParticipantImpl - Leaving method"); 

  return; 

}

ccs::base::PVMonitorImpl* PVMonitorUDPConstructor (void) { return dynamic_cast<ccs::base::PVMonitorImpl*>(new (std::nothrow) PVMonitorUDP ()); }

bool PVMonitorUDP::Open (void)
{

  log_trace("PVMonitorUDP::Open - Entering method"); 

  bool status = ((-1 == ParticipantImpl::GetSocket()) && !ccs::HelperTools::IsUndefinedString(ParticipantImpl::GetInterface()));

  if (status)
    {
      status = ParticipantImpl::Open();
    }

  if (status)    
    { // Bind socket to multicast address
      struct sockaddr_in mcast_addr;

      memset(&mcast_addr, 0, sizeof(mcast_addr));
      mcast_addr.sin_family = AF_INET;
      mcast_addr.sin_addr.s_addr = inet_addr(ParticipantImpl::GetMCastAddr());
      mcast_addr.sin_port = htons(ParticipantImpl::GetMCastPort());

      status = (-1 != bind(ParticipantImpl::GetSocket(), (struct sockaddr*) &mcast_addr, sizeof(mcast_addr)));

      if (status)
	{
	  log_debug("PVMonitorUDP::Open - MCAST group is '%s %d'", _mcast_group, __mcast_port);
	}
      else
	{
	  log_error("PVMonitorUDP::Open - bind(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  if (status)    
    { // Set socket options - Add IGMP membership
      struct ip_mreq ip_req;
      
      ip_req.imr_multiaddr.s_addr = inet_addr(ParticipantImpl::GetMCastAddr());
      ip_req.imr_interface.s_addr = inet_addr(ParticipantImpl::GetAddress());
 
      status = (-1 != setsockopt(ParticipantImpl::GetSocket(), IPPROTO_IP, IP_ADD_MEMBERSHIP, (ccs::types::char8*) &ip_req, sizeof(ip_req)));

      if (!status)
	{
	  log_error("PVMonitorUDP::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  log_trace("PVMonitorUDP::Open - Leaving method"); 

  return status;

}

bool PVMonitorUDP::Close (void)
{

  log_trace("PVMonitorUDP::Close - Entering method"); 

  bool status = (-1 != ParticipantImpl::GetSocket());

  if (status)
    { // Set socket options - Drop IGMP membership
      struct ip_mreq ip_req;
      
      ip_req.imr_multiaddr.s_addr = inet_addr(ParticipantImpl::GetMCastAddr());
      ip_req.imr_interface.s_addr = inet_addr(ParticipantImpl::GetAddress());

      status = (-1 != setsockopt(ParticipantImpl::GetSocket(), IPPROTO_IP, IP_DROP_MEMBERSHIP, (ccs::types::char8*) &ip_req, sizeof(ip_req)));

      if (!status)
	{
	  log_error("PVMonitorUDP::Close - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  if (status)
    {
      status = ParticipantImpl::Close();
    }

  log_trace("PVMonitorUDP::Close - Leaving method"); 

  return status;

}

bool PVMonitorUDP::Receive (void* buffer, ccs::types::uint32& size, const ccs::types::uint64 timeout) const // ToDo - Update timeout
{

  log_trace("PVMonitorUDP::Receive - Entering method"); 

  bool status = ((-1 != ParticipantImpl::GetSocket()) && (NULL != buffer)); // Bug 10103 - Test for NULL buffer

  if (status)
    {
      fd_set sel_fd;
      struct timespec sel_timeout = ccs::HelperTools::ToTimespec(timeout);
      
      FD_ZERO(&sel_fd); FD_SET(ParticipantImpl::GetSocket(), &sel_fd);

      status = (1 == pselect(ParticipantImpl::GetSocket() + 1, &sel_fd, NULL, NULL, &sel_timeout, NULL));
    }

  if (status)
    {
      status = Receive(buffer, size);
    }
  
  log_trace("PVMonitorUDP::Receive - Leaving method"); 

  return status;

}

bool PVMonitorUDP::Receive (void* buffer, ccs::types::uint32& size) const
{

  log_trace("PVMonitorUDP::Receive - Entering method"); 

  bool status = ((-1 != ParticipantImpl::GetSocket()) && (NULL != buffer)); // Bug 10103 - Test for NULL buffer

  if (status)
    {
      struct sockaddr_in ucast_addr; // The remote address to which to reply
      int addr_size = sizeof(ucast_addr);

      memset(&ucast_addr, 0, addr_size);

      int recvsize = recvfrom(ParticipantImpl::GetSocket(), buffer, size, 0, (struct sockaddr*) &ucast_addr, (socklen_t*) &addr_size);

      status = (-1 != recvsize);

      if (status)
	{
	  log_debug("PVMonitorUDP::Receive - recvfrom(...) successful with size '%d'", recvsize);

	  // WARNING - recvfrom return 0 in case of success on MCAST socket
	  if (recvsize == 0) recvsize = strlen(static_cast<const ccs::types::char8*>(buffer)) + 1;
	}

      if (status)
	{
	  size = recvsize;
	}
    }
  
  return status;

}

bool PVMonitorUDP::IsConnected (void) const
{

  bool status = __connected;

  if (!status)
    {
      // ToDo - ping server is not connected
    }

  return status;

}

bool PVMonitorUDP::SetParameter (const ccs::types::char8 * const name, const ccs::types::char8 * const value) 
{ 
  // Call base class method
  return ccs::base::PVMonitorImpl::SetParameter(name, value);

}

bool PVMonitorUDP::SetParameter (const ccs::types::char8 * const name, const ccs::types::AnyValue& value)
{ 
  // Call base class method
  return ccs::base::PVMonitorImpl::SetParameter(name, value);

}

bool PVMonitorUDP::Initialise (void)
{

  // Default implementation .. coverage
  (void)ccs::base::PVMonitorImpl::Initialise();  

  // Initalise attributes
  __initialised = true;
  __connected = false;

  // Set interface
  ParticipantImpl::SetInterface(DefaultIfaceName);

  // ToDo - Compute MCAST address from service name
  ParticipantImpl::SetMCastAddr(DefaultMCastAddr);
  ParticipantImpl::SetMCastPort(DefaultMCastPort);

  bool status = Open();

  if (status)
    {
      m_thread = new (std::nothrow) ccs::base::SynchronisedThreadWithCallback ("UDP RPC Server");
      m_thread->SetPeriod(100000000ul);
      m_thread->SetAccuracy(100000000ul);
      m_thread->SetCallback(reinterpret_cast<void(*)(void*)>(&PVMonitorUDP_CB), reinterpret_cast<void*>(this));
      
      status = m_thread->Launch();

      if (status)
        {
          log_info("PVMonitorUDP::Initialise - .. ccs::base::SynchronisedThreadWithCallback::Launch successful");
        }
      else
        {
          log_error("PVMonitorUDP::Initialise - .. ccs::base::SynchronisedThreadWithCallback::Launch unsuccessful");
        }
    }

  if (!status)
    {
      log_error("PVMonitorUDP::Initialise - .. unsuccessful");
    }

  return status;

}

bool PVMonitorUDP::Terminate (void)
{

  bool status = (NULL_PTR_CAST(ccs::base::SynchronisedThreadWithCallback*) != m_thread);

  if (status)
    {
      // Delete thread
      log_info("Terminate");
      (void)m_thread->Terminate();
      log_info("Delete");
      delete m_thread;
    }

  m_thread = NULL_PTR_CAST(ccs::base::SynchronisedThreadWithCallback*);

  return Close();

}

bool PVMonitorUDP::Do (void)
{

  ccs::types::char8 buffer [ccs::types::MaxMCastPacketSize] = STRING_UNDEFINED;
  ccs::types::uint32 size = ccs::types::MaxMCastPacketSize;

  bool status = Receive(buffer, size, 10000000ul);

  ccs::types::AnyValue value;

  if (status && (false == __connected))
    {
      CallEventHandler(ccs::base::PVMonitor::Connect);
      __connected = true;
    }

  if (status)
    {
      log_info("PVMonitorUDP::Do - Received '%s' ..", buffer);
    }

  if (status)
    {
      status = ccs::HelperTools::ParseFromJSONStream(&value, buffer);
    }

  if (status)
    {
      CallDataHandler(value);
    }

  return status;

}

PVMonitorUDP::PVMonitorUDP (void) : ccs::base::PVMonitorImpl(), ParticipantImpl() { Initialise(); }

PVMonitorUDP::~PVMonitorUDP (void) {} // Terminate() of the transport implicit when destroying the monitor client

bool PVServerUDP::Open (void)
{

  log_trace("PVServerUDP::Open - Entering method"); 

  bool status = ((-1 == ParticipantImpl::GetSocket()) && !ccs::HelperTools::IsUndefinedString(ParticipantImpl::GetInterface()));

  if (status)
    {
      status = ParticipantImpl::Open();
    }

  log_trace("PVServerUDP::Open - Leaving method"); 

  return status;

}

bool PVServerUDP::Close (void)
{

  log_trace("PVServerUDP::Close - Entering method"); 

  bool status = (-1 != ParticipantImpl::GetSocket());

  if (status)
    {
      status = ParticipantImpl::Close();
    }

  log_trace("PVServerUDP::Close - Leaving method"); 

  return status;

}

bool PVServerUDP::Publish (const void* buffer, const ccs::types::uint32 size) const
{

  log_trace("PVServerUDP::Publish - Entering method"); 

  bool status = ((-1 != ParticipantImpl::GetSocket()) && (NULL != buffer)); // Bug 10103 - Test for NULL buffer

  if (status)
    {
      struct sockaddr_in mcast_addr;

      memset(&mcast_addr, 0, sizeof(mcast_addr));
      mcast_addr.sin_family = AF_INET;
      mcast_addr.sin_addr.s_addr = inet_addr(ParticipantImpl::GetMCastAddr());
      mcast_addr.sin_port = htons(ParticipantImpl::GetMCastPort());
    
      status = (-1 != sendto(ParticipantImpl::GetSocket(), buffer, size, MSG_DONTWAIT, (struct sockaddr*) &mcast_addr, sizeof(mcast_addr)));
    }

  log_trace("PVServerUDP::Publish - Leaving method"); 

  return status;

}

bool PVServerUDP::Publish (const ccs::types::AnyValue& value) const
{

  ccs::types::char8 buffer [4096] = STRING_UNDEFINED;

  bool status = ccs::HelperTools::SerialiseToJSONStream(&value, buffer, 4096u);

  if (status)
    {
      status = Publish(buffer, 4096u);
    }

  if (status)
    {
      log_info("PVServerUDP::Publish - Published '%s'", buffer);
    }

  return status;

}

bool PVServerUDP::Initialise (void)
{

  // Set interface
  ParticipantImpl::SetInterface(DefaultIfaceName);

  // ToDo - Compute MCAST address from service name
  ParticipantImpl::SetMCastAddr(DefaultMCastAddr);
  ParticipantImpl::SetMCastPort(DefaultMCastPort);

  bool status = Open();

  return status;

}

bool PVServerUDP::Terminate (void)
{

  bool status = Close();

  return status;

}

PVServerUDP::PVServerUDP (void) : ParticipantImpl() { Initialise(); }
PVServerUDP::~PVServerUDP (void) { Terminate(); }

} // namespace test

} // namespace ccs

#undef LOG_ALTERN_SRC
