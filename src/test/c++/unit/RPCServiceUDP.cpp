/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <memory> // std::shared_ptr
#include <new> // std::nothrow

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <errno.h>

// Local header files

#include "BasicTypes.h" // Global type definition
#include "SysTools.h" // Misc. helper functions
#include "NetTools.h" // Misc. helper functions
#include "CyclicRedundancyCheck.h"

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include "log-api.h" // Syslog wrapper routines

#include "AnyThread.h"

#include "AnyType.h" // Introspectable data type ..
#include "AnyTypeHelper.h" // .. associated helper routines

#include "AnyValue.h" // Variable with introspectable data type ..
#include "AnyValueHelper.h" // .. associated helper routines

#include "RPCClientImpl.h"
#include "RPCClientImplFactory.h"

#include "RPCServerImpl.h"
#include "RPCServerImplFactory.h"

#include "RPCTypes.h"

#include "RPCServiceUDP.h" // This class definition

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "udp-if"

// Type definition

namespace ccs {

namespace mcast {

class ParticipantImpl
{

  private:

    char __if_addr [ccs::types::MaxIPv4AddrLength];
    char __if_name [ccs::types::MaxIPv4AddrLength];

    char __mcast_group [ccs::types::MaxIPv4AddrLength];
    ccs::types::uint32 __mcast_port;

    int __socket; // Socket handle

    void Initialise (void);

  public:

    const char* GetAddress (void) const { return __if_addr; };
    const char* GetInterface (void) const { return __if_name; };
    const char* GetMCastAddr (void) const { return __mcast_group; };
    ccs::types::uint32 GetMCastPort (void) const { return __mcast_port; };
    int GetSocket (void) const { return __socket; };

    void SetAddress (const char* addr);
    void SetInterface (const char* iface);
    void SetMCastAddr (const char* addr);
    void SetMCastPort (const ccs::types::uint32 port);

    bool Open (void);
    bool Close (void);

    ParticipantImpl (void);
    virtual ~ParticipantImpl (void);

};

class RPCClientUDP : public ccs::base::RPCClientImpl, public ParticipantImpl
{

  private:

    bool __initialised;
    bool __connected;

    // Initialiser methods
    bool Initialise (void);

  protected:

  public:

    // Initialiser methods
    virtual bool Launch (void);
    virtual bool Terminate (void);

    // Accessor methods
    virtual bool IsConnected (void) const;

    // Miscellaneous methods
    bool Open (void);
    bool Close (void);

    bool Publish (const void* buffer, const ccs::types::uint32 size) const;
    bool Receive (void* buffer, ccs::types::uint32& size, const ccs::types::uint64 timeout) const; // Blocking wait with configurable timeout - Size updated with actual number of bytes received
    bool Receive (void* buffer, ccs::types::uint32& size) const; // Blocking call - Size updated with actual number of bytes received

    virtual ccs::types::AnyValue SendRequest (const ccs::types::AnyValue& request) const;

    // Constructor methods
    RPCClientUDP (void);

    // Destructor method
    virtual ~RPCClientUDP (void); 

};
#if 0 // Launch and Terminate methods are duplicated through hierarchy
class RPCServerUDP : public ccs::base::RPCServerImpl, public ParticipantImpl, public ccs::base::SynchronisedThread
#else
class RPCServerUDP : public ccs::base::RPCServerImpl, public ParticipantImpl
#endif
{

  private:

    ccs::base::SynchronisedThreadWithCallback* m_thread; // __thread causes compilation issues

    char   __reply_addr [ccs::types::MaxIPv4AddrLength];
    ccs::types::uint32 __reply_port;

    bool __initialised;
               
    // Initialiser methods
    bool Initialise (void);

  protected:

  public:

    // Initialiser methods
    virtual bool Launch (void);
    virtual bool Terminate (void);

    // Miscellaneous methods
    bool Open (void);
    bool Close (void);

    bool Receive (void* buffer, ccs::types::uint32& size, const ccs::types::uint64 timeout) const; // Blocking wait with configurable timeout - Size updated with actual number of bytes received
    bool Receive (void* buffer, ccs::types::uint32& size) const; // Blocking call - Size updated with actual number of bytes received
    bool Reply (const void* buffer, const ccs::types::uint32 size) const;

    bool Do (void);

    // Constructor methods
    RPCServerUDP (void);

    // Destructor method
    virtual ~RPCServerUDP (void); 

};

// Global variables

static bool __is_registered = (ccs::base::RPCClientImplFactory::GetInstance()->Register("ccs::base::RPCClientUDP", &RPCClientUDPConstructor) &&
			       ccs::base::RPCServerImplFactory::GetInstance()->Register("ccs::base::RPCServerUDP", &RPCServerUDPConstructor));

// Function declaration

// Function definition

void ParticipantImpl::Initialise (void)
{

  log_trace("ParticipantImpl::Initialise - Entering method"); 

  // Initialise attributes
  this->SetAddress(STRING_UNDEFINED);
  this->SetInterface(STRING_UNDEFINED);
  this->SetMCastAddr(STRING_UNDEFINED);
  this->SetMCastPort(0u);

  __socket = -1;

  log_trace("ParticipantImpl::Initialise - Leaving method"); 

  return;

}

void ParticipantImpl::SetAddress (const char* addr) 
{ 

  log_trace("ParticipantImpl::SetAddress - Entering method"); 

  if ((ccs::HelperTools::IsUndefinedString(addr) != true) && (ccs::HelperTools::IsAddressValid(addr) != true)) 
    {
      log_warning("ParticipantImpl::SetAddress - Address '%s' is invalid", addr); 
    }

  ccs::HelperTools::SafeStringCopy(__if_addr, addr, ccs::types::MaxIPv4AddrLength);

  log_trace("ParticipantImpl::SetAddress - Leaving method"); 

  return; 

}

void ParticipantImpl::SetInterface (const char* iface) 
{ 

  log_trace("ParticipantImpl::SetInterface - Entering method"); 

  if ((ccs::HelperTools::IsUndefinedString(iface) != true) && (ccs::HelperTools::IsInterfaceValid(iface) != true)) 
    {
      log_warning("ParticipantImpl::SetInterface - Interface '%s' is invalid", iface); 
    }

  ccs::HelperTools::SafeStringCopy(__if_name, iface, ccs::types::MaxIPv4AddrLength); 

  log_trace("ParticipantImpl::SetInterface - Leaving method"); 

  return; 

}

void ParticipantImpl::SetMCastAddr (const char* addr) 
{ 

  log_trace("ParticipantImpl::SetMCastAddr - Entering method"); 

  if ((ccs::HelperTools::IsUndefinedString(addr) != true) && (ccs::HelperTools::IsAddressValid(addr) != true)) 
    {
      log_warning("ParticipantImpl::SetMCastAddr - Address '%s' is invalid", addr); 
    }

  ccs::HelperTools::SafeStringCopy((char*) __mcast_group, addr, ccs::types::MaxIPv4AddrLength); 

  log_trace("ParticipantImpl::SetMCastAddr - Leaving method"); 

  return; 

}

void ParticipantImpl::SetMCastPort (const ccs::types::uint32 port) { __mcast_port = port; return; }

bool ParticipantImpl::Open (void)
{

  log_trace("ParticipantImpl::Open - Entering method"); 

  bool status = ((-1 == __socket) && !ccs::HelperTools::IsUndefinedString(__if_name));

  if (status)
    {
      __socket = socket(AF_INET, SOCK_DGRAM, 0);
      status = (-1 != __socket);
    }

  if (status)    
    { // Get address of selected local interface
      struct ifreq if_req;
      strcpy(if_req.ifr_name, __if_name);
      status = (-1 != ioctl(__socket, SIOCGIFADDR, &if_req));

      if (status)
	{
	  log_debug("ParticipantImpl::Open - IP address is '%s'", inet_ntoa(((struct sockaddr_in*)&if_req.ifr_addr)->sin_addr));
	  ccs::HelperTools::SafeStringCopy(__if_addr, inet_ntoa(((struct sockaddr_in*)&if_req.ifr_addr)->sin_addr), ccs::types::MaxIPv4AddrLength);
	}
      else
	{
	  log_error("ParticipantImpl::Open - ioctl(...) failed with '%d - %s'", errno, strerror(errno));
	}

  }

  if (status)    
    { // Set socket options - Attach socket to selected local interface
      struct in_addr if_addr;
      if_addr.s_addr = inet_addr(__if_addr);
      status = (-1 != setsockopt(__socket, IPPROTO_IP, IP_MULTICAST_IF, (char*) &if_addr, sizeof(if_addr)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  if (status)    
    { // Set socket buffer length
      int length = ccs::types::MaxIPv4PacketSize;
      status = (-1 != setsockopt(__socket, SOL_SOCKET, SO_SNDBUF, (char*) &length, sizeof(length)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  if (status)    
    { // Set socket buffer length
      int length = ccs::types::MaxIPv4PacketSize;
      status = (-1 != setsockopt(__socket, SOL_SOCKET, SO_RCVBUF, (char*) &length, sizeof(length)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }
#if 0 // CCP-731 - IP fragmentation error on VM NIC
  if (status)    
    { // Disable UDP checksum
      int disable = 1;
      status = (-1 != setsockopt(__socket, SOL_SOCKET, SO_NO_CHECK, (char*) &disable, sizeof(disable)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }
#endif
  if (status)    
    { // Set reuse flag to allow for multiple instances running on the same machine/
      int reuse = 1;
      status = (-1 != setsockopt(__socket, SOL_SOCKET, SO_REUSEADDR, (char*) &reuse, sizeof(reuse)));

      if (!status)
	{
	  log_error("ParticipantImpl::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }
  
  log_trace("ParticipantImpl::Open - Leaving method"); 

  return status;

}

bool ParticipantImpl::Close (void)
{

  log_trace("ParticipantImpl::Close - Entering method"); 

  bool status = (-1 != __socket);

  if (status)
    {
      close(__socket);
    }

  __socket = -1;

  log_trace("ParticipantImpl::Close - Leaving method"); 

  return status;

}

ParticipantImpl::ParticipantImpl (void) 
{ 

  log_trace("ParticipantImpl::ParticipantImpl - Entering method"); 

  // Initialize attributes 
  this->Initialise(); 

  log_trace("ParticipantImpl::ParticipantImpl - Leaving method"); 

  return; 

}

ParticipantImpl::~ParticipantImpl (void)
{

  log_trace("ParticipantImpl::~ParticipantImpl - Entering method"); 

  // Close socket 
  this->Close(); 

  log_trace("ParticipantImpl::~ParticipantImpl - Leaving method"); 

  return; 

}

ccs::base::RPCClientImpl* RPCClientUDPConstructor (void) { return dynamic_cast<ccs::base::RPCClientImpl*>(new (std::nothrow) RPCClientUDP ()); }

bool RPCClientUDP::Initialise (void)
{

  // Initialise attributes
  __connected = false;
  __initialised = true;

  return true;

}

bool RPCClientUDP::Open (void)
{

  log_trace("RPCClientUDP::Open - Entering method"); 

  bool status = ((-1 == ParticipantImpl::GetSocket()) && !ccs::HelperTools::IsUndefinedString(ParticipantImpl::GetInterface()));

  if (status)
    {
      status = ParticipantImpl::Open();
    }

  log_trace("RPCClientUDP::Open - Leaving method"); 

  return status;

}

bool RPCClientUDP::Close (void)
{

  log_trace("RPCClientUDP::Close - Entering method"); 

  bool status = (-1 != ParticipantImpl::GetSocket());

  if (status)
    {
      status = ParticipantImpl::Close();
    }

  log_trace("RPCClientUDP::Close - Leaving method"); 

  return status;

}

bool RPCClientUDP::Publish (const void* buffer, const ccs::types::uint32 size) const
{

  log_trace("RPCClientUDP::Publish - Entering method"); 

  bool status = ((-1 != ParticipantImpl::GetSocket()) && (NULL != buffer)); // Bug 10103 - Test for NULL buffer

  if (status)
    {
      struct sockaddr_in mcast_addr;

      memset(&mcast_addr, 0, sizeof(mcast_addr));
      mcast_addr.sin_family = AF_INET;
      mcast_addr.sin_addr.s_addr = inet_addr(ParticipantImpl::GetMCastAddr());
      mcast_addr.sin_port = htons(ParticipantImpl::GetMCastPort());
    
      status = (-1 != sendto(ParticipantImpl::GetSocket(), buffer, size, MSG_DONTWAIT, (struct sockaddr*) &mcast_addr, sizeof(mcast_addr)));
    }

  log_trace("RPCClientUDP::Publish - Leaving method"); 

  return status;

}

bool RPCClientUDP::Receive (void* buffer, ccs::types::uint32& size, const ccs::types::uint64 timeout) const // ToDo - Update timeout
{

  log_trace("RPCClientUDP::Receive - Entering method"); 

  bool status = ((-1 != ParticipantImpl::GetSocket()) && (NULL != buffer)); // Bug 10103 - Test for NULL buffer

  if (status)
    {
      fd_set sel_fd;
      struct timespec sel_timeout = ccs::HelperTools::ToTimespec(timeout);
      
      FD_ZERO(&sel_fd); FD_SET(ParticipantImpl::GetSocket(), &sel_fd);

      status = (1 == pselect(ParticipantImpl::GetSocket() + 1, &sel_fd, NULL, NULL, &sel_timeout, NULL));
    }

  if (status)
    {
      status = Receive(buffer, size);
    }
  
  log_trace("RPCClientUDP::Receive - Leaving method"); 

  return status;

}

bool RPCClientUDP::Receive (void* buffer, ccs::types::uint32& size) const
{

  log_trace("RPCClientUDP::Receive - Entering method"); 

  bool status = ((-1 != ParticipantImpl::GetSocket()) && (NULL != buffer)); // Bug 10103 - Test for NULL buffer

  if (status)
    {
      struct sockaddr_in ucast_addr; // The remote address to which to reply
      int addr_size = sizeof(ucast_addr);

      memset(&ucast_addr, 0, addr_size);

      int recvsize = recvfrom(ParticipantImpl::GetSocket(), buffer, size, 0, (struct sockaddr*) &ucast_addr, (socklen_t*) &addr_size);

      status = (-1 != recvsize);

      if (status)
	{
	  log_debug("RPCClientUDP::Receive - recvfrom(...) successful with size '%d'", recvsize);

	  // WARNING - recvfrom return 0 in case of success on MCAST socket
	  if (recvsize == 0) recvsize = strlen(static_cast<const char*>(buffer)) + 1;
	}

      if (status)
	{
	  size = recvsize;
	}
    }
  
  return status;

}

ccs::types::AnyValue RPCClientUDP::SendRequest (const ccs::types::AnyValue& request) const
{

  log_info("RPCClientUDP::SendRequest - Entering method");

  bool status = IsConnected();

  // Serialise type and value as JSON stream
  char buffer [ccs::types::MaxMCastPacketSize] = STRING_UNDEFINED;
  ccs::types::uint32 size;

  if (status)
    {
      status = ccs::HelperTools::Serialise(request.GetType(), buffer, ccs::types::MaxMCastPacketSize);
    }

  if (status)
    {
      log_info("RPCClientUDP::SendRequest - Publish '%s' ..", buffer);
      size = strlen(buffer) + 1u;
      status = Publish(reinterpret_cast<void*>(buffer), size);
    }

  if (status)
    {
      status = request.SerialiseInstance(buffer, ccs::types::MaxMCastPacketSize);
    }

  if (status)
    {
      log_info("RPCClientUDP::SendRequest - .. and '%s'", buffer);
      size = strlen(buffer) + 1u;
      status = Publish(reinterpret_cast<void*>(buffer), size);
    }

  // Wait for reply
  size = ccs::types::MaxMCastPacketSize;
  while (status && (false == Receive(buffer, size, 1000000000ul))) {}

  log_info("RPCClientUDP::SendRequest - Received '%s' ..", buffer);

  // Compose reply from received JSON stream
  ccs::types::AnyValue reply (buffer);

  ccs::HelperTools::LogSerialisedType(&reply);

  size = ccs::types::MaxMCastPacketSize;
  while (status && (false == Receive(buffer, size, 1000000000ul))) {}

  log_info("RPCClientUDP::SendRequest - .. and '%s'", buffer);

  reply.ParseInstance(buffer);

  ccs::HelperTools::LogSerialisedInstance(&reply);

  log_info("RPCClientUDP::SendRequest - Reply copied and getting out of scope");

  return reply;

}

bool RPCClientUDP::IsConnected (void) const
{

  bool status = __connected;

  if (!status)
    {
      // ToDo - ping server is not connected
      status = true;
    }

  return status;

}

bool RPCClientUDP::Launch (void)
{

  // Set interface
  ParticipantImpl::SetInterface(DefaultIfaceName);
#if 0
  // Compute MCAST address from service name
  RPCClientImpl::GetService();
#else
  ParticipantImpl::SetMCastAddr(DefaultMCastAddr);
  ParticipantImpl::SetMCastPort(DefaultMCastPort);
#endif
  return Open();

}

bool RPCClientUDP::Terminate (void)
{

  return Close();

}

RPCClientUDP::RPCClientUDP (void) : ccs::base::RPCClientImpl(), ParticipantImpl() { Initialise(); }

RPCClientUDP::~RPCClientUDP (void) 
{

  Terminate();

  return;

}

void RPCServerUDP_CB (RPCServerUDP* self) { self->Do(); }

ccs::base::RPCServerImpl* RPCServerUDPConstructor (void) { return dynamic_cast<ccs::base::RPCServerImpl*>(new (std::nothrow) RPCServerUDP ()); }

bool RPCServerUDP::Initialise (void)
{

  // Initialise attributes
  m_thread = new (std::nothrow) ccs::base::SynchronisedThreadWithCallback ("UDP RPC Server");
  __initialised = true;

  m_thread->SetPeriod(100000000ul); m_thread->SetAccuracy(100000000ul);
  m_thread->SetCallback(reinterpret_cast<void(*)(void*)>(&RPCServerUDP_CB), reinterpret_cast<void*>(this));

  return true;

}

bool RPCServerUDP::Open (void)
{

  log_trace("RPCServerUDP::Open - Entering method"); 

  bool status = ((-1 == ParticipantImpl::GetSocket()) && !ccs::HelperTools::IsUndefinedString(ParticipantImpl::GetInterface()));

  if (status)
    {
      status = ParticipantImpl::Open();
    }

  if (status)    
    { // Bind socket to multicast address
      struct sockaddr_in mcast_addr;

      memset(&mcast_addr, 0, sizeof(mcast_addr));
      mcast_addr.sin_family = AF_INET;
      mcast_addr.sin_addr.s_addr = inet_addr(ParticipantImpl::GetMCastAddr());
      mcast_addr.sin_port = htons(ParticipantImpl::GetMCastPort());

      status = (-1 != bind(ParticipantImpl::GetSocket(), (struct sockaddr*) &mcast_addr, sizeof(mcast_addr)));

      if (status)
	{
	  log_debug("RPCServerUDP::Open - MCAST group is '%s %d'", _mcast_group, __mcast_port);
	}
      else
	{
	  log_error("RPCServerUDP::Open - bind(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  if (status)    
    { // Set socket options - Add IGMP membership
      struct ip_mreq ip_req;
      
      ip_req.imr_multiaddr.s_addr = inet_addr(ParticipantImpl::GetMCastAddr());
      ip_req.imr_interface.s_addr = inet_addr(ParticipantImpl::GetAddress());
 
      status = (-1 != setsockopt(ParticipantImpl::GetSocket(), IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*) &ip_req, sizeof(ip_req)));

      if (!status)
	{
	  log_error("RPCServerUDP::Open - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  log_trace("RPCServerUDP::Open - Leaving method"); 

  return status;

}

bool RPCServerUDP::Close (void)
{

  log_trace("RPCServerUDP::Close - Entering method"); 

  bool status = (-1 != ParticipantImpl::GetSocket());

  if (status)
    { // Set socket options - Drop IGMP membership
      struct ip_mreq ip_req;
      
      ip_req.imr_multiaddr.s_addr = inet_addr(ParticipantImpl::GetMCastAddr());
      ip_req.imr_interface.s_addr = inet_addr(ParticipantImpl::GetAddress());

      status = (-1 != setsockopt(ParticipantImpl::GetSocket(), IPPROTO_IP, IP_DROP_MEMBERSHIP, (char*) &ip_req, sizeof(ip_req)));

      if (!status)
	{
	  log_error("RPCServerUDP::Close - setsockopt(...) failed with '%d - %s'", errno, strerror(errno));
	}
    }

  if (status)
    {
      status = ParticipantImpl::Close();
    }

  log_trace("RPCServerUDP::Close - Leaving method"); 

  return status;

}

bool RPCServerUDP::Receive (void* buffer, ccs::types::uint32& size, const ccs::types::uint64 timeout) const // ToDo - Update timeout
{

  log_trace("RPCServerUDP::Receive - Entering method"); 

  bool status = ((-1 != ParticipantImpl::GetSocket()) && (NULL != buffer)); // Bug 10103 - Test for NULL buffer

  if (status)
    {
      fd_set sel_fd;
      struct timespec sel_timeout = ccs::HelperTools::ToTimespec(timeout);
      
      FD_ZERO(&sel_fd); FD_SET(ParticipantImpl::GetSocket(), &sel_fd);

      status = (1 == pselect(ParticipantImpl::GetSocket() + 1, &sel_fd, NULL, NULL, &sel_timeout, NULL));
    }

  if (status)
    {
      status = Receive(buffer, size);
    }
  
  log_trace("RPCServerUDP::Receive - Leaving method"); 

  return status;

}

bool RPCServerUDP::Receive (void* buffer, ccs::types::uint32& size) const
{

  log_info("RPCServerUDP::Receive - Entering method"); 

  bool status = ((-1 != ParticipantImpl::GetSocket()) && (NULL != buffer)); // Bug 10103 - Test for NULL buffer

  if (status)
    {
      struct sockaddr_in ucast_addr; // The remote address to which to reply
      int addr_size = sizeof(ucast_addr);

      memset(&ucast_addr, 0, addr_size);

      int recvsize = recvfrom(ParticipantImpl::GetSocket(), buffer, size, 0, (struct sockaddr*) &ucast_addr, (socklen_t*) &addr_size);

      status = (-1 != recvsize);

      if (status)
	{
	  log_info("RPCServerUDP::Receive - recvfrom(...) successful with size '%d'", recvsize);

	  // WARNING - recvfrom return 0 in case of success on MCAST socket
	  if (recvsize == 0) recvsize = strlen(static_cast<const char*>(buffer)) + 1;
	}

      if (status)
	{
	  log_info("RPCServerUDP::Receive - Received UID '%u (%u)'", ccs::HelperTools::CyclicRedundancyCheck<uint32_t>(static_cast<uint8_t*>(buffer), recvsize), recvsize);

	  ccs::HelperTools::SafeStringCopy(const_cast<char*>(__reply_addr), inet_ntoa(ucast_addr.sin_addr) , ccs::types::MaxIPv4AddrLength);
	  *(const_cast<ccs::types::uint32*>(&__reply_port)) = ntohs(ucast_addr.sin_port);
	  
	  log_info("RPCServerUDP::Receive - Received message from '%s:%d'", __reply_addr, __reply_port);

	  size = recvsize;
	}
    }
  
  return status;

}

bool RPCServerUDP::Reply (const void* buffer, const ccs::types::uint32 size) const
{

  log_trace("RPCServerUDP::Reply - Entering method"); 

  bool status = ((-1 != ParticipantImpl::GetSocket()) && (NULL != buffer)); // Bug 10103 - Test for NULL buffer

  if (status)
    {
      struct sockaddr_in ucast_addr; // The remote address to which to send
      int addr_size = sizeof(ucast_addr);

      memset(&ucast_addr, 0, sizeof(ucast_addr));
      ucast_addr.sin_family = AF_INET;
      ucast_addr.sin_addr.s_addr = inet_addr(__reply_addr);
      ucast_addr.sin_port = htons(__reply_port);

      status = (-1 != sendto(ParticipantImpl::GetSocket(), buffer, size, MSG_DONTWAIT, (struct sockaddr*) &ucast_addr, addr_size));
    }

  // Todo - Clear reply address

  log_trace("RPCServerUDP::Reply - Leaving method"); 

  return status;

}

bool RPCServerUDP::Do (void)
{

  char buffer [ccs::types::MaxMCastPacketSize] = STRING_UNDEFINED;
  ccs::types::uint32 size = ccs::types::MaxMCastPacketSize;

  bool status = Receive(buffer, size, 10000000ul);

  if (status)
    {
      log_info("RPCServerUDP::Do - Received '%s' ..", buffer);
      //status = ccs::HelperTools::Is<ccs::types::CompoundType>(buffer);

      ccs::types::AnyValue reply;

      ccs::types::AnyValue request (buffer);

      size = ccs::types::MaxMCastPacketSize;
      while (status && (false == Receive(buffer, size, 1000000000ul))) {}

      log_info("RPCServerUDP::Do - .. and '%s'", buffer);
      status = request.ParseInstance(buffer);

      if (status)
	{
	  log_info("RPCServerUDP::Do - Size is '%u'", request.GetSize());
	  reply = CallHandler(request);
	  status = static_cast<bool>(reply.GetType());
	}
      else
	{
	  log_error("Unable to parse value '%s'", buffer);
	}

      if (!status)
	{
	  reply = ccs::HelperTools::InstantiateRPCReply(status, "error", "Issue with callback");
	  status = true;
	}

      if (status)
	{
	  status = ccs::HelperTools::Serialise(reply.GetType(), buffer, ccs::types::MaxMCastPacketSize);
	}
      
      if (status)
	{
	  log_info("RPCServerUDP::Do - Reply '%s' ..", buffer);
	  size = strlen(buffer) + 1u;
	  status = Reply(reinterpret_cast<void*>(buffer), size);
	}
      
      if (status)
	{
	  status = reply.SerialiseInstance(buffer, ccs::types::MaxMCastPacketSize);
	}

      if (status)
	{
	  log_info("RPCServerUDP::Do - .. and '%s'", buffer);
	  size = strlen(buffer) + 1u;
	  status = Reply(reinterpret_cast<void*>(buffer), size);
	}
    }

  return status;

}

bool RPCServerUDP::Launch (void)
{

  // Set interface
  ParticipantImpl::SetInterface(DefaultIfaceName);
  ParticipantImpl::SetMCastAddr(DefaultMCastAddr);
  ParticipantImpl::SetMCastPort(DefaultMCastPort);

  bool status = Open();

  if (status)
    {
      status = m_thread->Launch();
    }

  return status;

}

bool RPCServerUDP::Terminate (void)
{

  bool status = (NULL_PTR_CAST(ccs::base::SynchronisedThreadWithCallback*) != m_thread);

  if (status)
    {
      status = m_thread->Terminate();
    }

  if (status)
    {
      delete m_thread;
      m_thread = NULL_PTR_CAST(ccs::base::SynchronisedThreadWithCallback*);
    }

  if (status)
    {
      status = Close();
    }

  return status;

}

RPCServerUDP::RPCServerUDP (void) : ccs::base::RPCServerImpl(), ParticipantImpl() { Initialise(); }

RPCServerUDP::~RPCServerUDP (void) 
{

  Terminate();

  return;

}

} // namespace mcast

} // namespace ccs

#undef LOG_ALTERN_SRC
