/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

// Local header files

#include "types.h" // Misc. type definition
#include "tools.h" // Misc. helper functions

#include "log-api.h" // Syslog wrapper routines

#include "AnyValueDatabase.h"

// Constants

// Type definition

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();

// Function declaration

// Function definition
  
TEST(AnyValueDatabase_Test, Constructor_default)
{
  // Variable cache implicitly instantiated when calling GTDB methods
  bool ret = !ccs::base::GlobalVariableCache::IsValid("");

  ASSERT_EQ(ret, true);
}

TEST(AnyValueDatabase_Test, IsValid_scalar)
{
  bool ret = !ccs::base::GlobalVariableCache::IsValid("test::bool");

  if (ret)
    {
      ret = ccs::base::GlobalVariableCache::Register("test::bool", ::ccs::types::Boolean);
    }

  if (ret)
    {
      ret = ccs::base::GlobalVariableCache::IsValid("test::bool");
    }

  if (ret)
    {
      ret = ccs::base::GlobalVariableCache::Remove("test::bool");
    }

  if (ret)
    {
      ret = !ccs::base::GlobalVariableCache::IsValid("test::bool");
    }

  ASSERT_EQ(ret, true);
}

TEST(AnyValueDatabase_Test, Register_error)
{
  bool ret = !ccs::base::GlobalVariableCache::IsValid("test::undefined");

  if (ret)
    {
      // Register with invalid type
      ccs::base::SharedReference<const ccs::types::AnyType> type;
      ret = !ccs::base::GlobalVariableCache::Register("test::undefined", type);
    }

   if (ret)
    {
      ret = !ccs::base::GlobalVariableCache::IsValid("test::undefined");
    }

   ASSERT_EQ(ret, true);
}

TEST(AnyValueDatabase_Test, SetValue_bool)
{
  bool ret = !ccs::base::GlobalVariableCache::IsValid("test::bool");

  if (ret)
    {
      ret = ccs::base::GlobalVariableCache::Register("test::bool", ::ccs::types::Boolean);
    }

  if (ret)
    {
      ret = ccs::base::GlobalVariableCache::IsValid("test::bool");
    }

  bool value;

  if (ret)
    {
      value = true;
      ret = ccs::base::GlobalVariableCache::SetValue("test::bool", value);
    }

  if (ret)
    {
      value = false;
      ret = ccs::base::GlobalVariableCache::GetValue("test::bool", value);
    }

  if (ret)
    {
      ret = (true == value);
    }

  if (ret)
    {
      value = false;
      ret = ccs::base::GlobalVariableCache::SetValue("test::bool", value);
    }

  if (ret)
    {
      value = true;
      ret = ccs::base::GlobalVariableCache::GetValue("test::bool", value);
    }

  if (ret)
    {
      ret = (false == value);
    }

  if (ret)
    {
      ret = ccs::base::GlobalVariableCache::Remove("test::bool");
    }

  if (ret)
    {
      ret = !ccs::base::GlobalVariableCache::IsValid("test::bool");
    }

  ASSERT_EQ(ret, true);
}

TEST(AnyValueDatabase_Test, SetValue_uint64)
{
  bool ret = !ccs::base::GlobalVariableCache::IsValid("test::uint64");

  if (ret)
    {
      ret = ccs::base::GlobalVariableCache::Register("test::uint64", ::ccs::types::UnsignedInteger64);
    }

  if (ret)
    {
      ret = ccs::base::GlobalVariableCache::IsValid("test::uint64");
    }

  ccs::types::uint64 value;

  if (ret)
    {
      value = 123456789ul;
      ret = ccs::base::GlobalVariableCache::SetValue("test::uint64", value);
    }

  if (ret)
    {
      value = 0ul;
      ret = ccs::base::GlobalVariableCache::GetValue("test::uint64", value);
    }

  if (ret)
    {
      ret = (123456789ul == value);
    }

  if (ret)
    {
      value = 0ul;
      ret = ccs::base::GlobalVariableCache::SetValue("test::uint64", value);
    }

  if (ret)
    {
      value = 123456789ul;
      ret = ccs::base::GlobalVariableCache::GetValue("test::uint64", value);
    }

  if (ret)
    {
      ret = (0ul == value);
    }

  if (ret)
    {
      ret = ccs::base::GlobalVariableCache::Remove("test::uint64");
    }

  if (ret)
    {
      ret = !ccs::base::GlobalVariableCache::IsValid("test::uint64");
    }

  ASSERT_EQ(ret, true);
}

