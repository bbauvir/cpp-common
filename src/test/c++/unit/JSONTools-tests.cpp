/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

//#include <memory> // std::shared_ptr, etc.
#include <new> // std::nothrow, etc.

#include <gtest/gtest.h> // Google test framework

// Local header files

#define LOG_DEBUG_ENABLE
#undef LOG_DEBUG_ENABLE

#include "BasicTypes.h" // Misc. type definition
#include "JSONTools.h" // Misc. helper functions

#include "log-api.h" // Syslog wrapper routines

// Constants

// Type definition

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();

// Function declaration

// Function definition
  
TEST(JSONTools_Test, FindMatchingBrace)
{
  bool ret = true;

  if (ret)
    { //                0 12345 67 890123456789 01 23456789 012 34567 890123 4567890123456 7890
      char buffer [] = "{\"type\":\"MyArrayType\",\"element\":{\"type\":..},\"multiplicity\":4},{},{}]";
      log_info("TEST(SysTools_Test, FindMatchingBrace) - '%u' for '%s'", ccs::HelperTools::FindMatchingBrace(buffer), buffer);
      ret = (60 == ccs::HelperTools::FindMatchingBrace(buffer));
    }

  if (ret)
    { //                0 12345 678 90123 456789 01234 567890123
      char buffer [] = "[\"att1\":{\"type\":..},\"att2\":{..},,]";
      log_info("TEST(SysTools_Test, FindMatchingBrace) - '%u' for '%s'", ccs::HelperTools::FindMatchingBrace(buffer), buffer);
      ret = (33 == ccs::HelperTools::FindMatchingBrace(buffer));
    }
#if 0
  if (ret)
    { //                0123456789012345678901
      char buffer [] = "(((a==b)&&()||())&&())";
      log_info("TEST(SysTools_Test, FindMatchingBrace) - '%u' for '%s'", ccs::HelperTools::FindMatchingBrace(buffer), buffer);
      ret = (21 == ccs::HelperTools::FindMatchingBrace(buffer));
    }
#endif
  if (ret)
    {
      char buffer [STRING_MAX_LENGTH] = STRING_UNDEFINED;
      ret = ((-1 == ccs::HelperTools::FindMatchingBrace(NULL)) &&
	     (-1 == ccs::HelperTools::FindMatchingBrace(buffer)));
    }
#if 0 // Probability that the uninitialised buffer contains valid sequence is not necessarilly low
  if (ret)
    { // Random content
      char* buffer = static_cast<char*>(malloc(1024u));
      ret = (-1 == ccs::HelperTools::FindMatchingBrace(buffer));
      free(buffer);
    }
#endif
  ASSERT_EQ(true, ret);
}

TEST(JSONTools_Test, Array)
{
  using namespace ccs::types;
  using namespace ccs::HelperTools;

  // JSON stream     01 23456 78 901234 5678901234567
  char8 buffer [] = "[{\"attr\":\"value\"},{..},{..}]";
      
  bool ret = IsJSONArray(buffer);

  uint32 start = 0u;
  uint32 close = 0u;

  if (ret)
    { 
      ret = FindJSONArray(buffer, start, close);
    }

  if (ret)
    { 
      ret = ((0u == start) && (27u == close));
    }

  if (ret)
    { 
      ret = (28u == JSONArrayLength(buffer));
    }

  char8 copy [64] = STRING_UNDEFINED;

  if (ret)
    { 
      ret = CopyJSONArray(buffer, copy, 64u);
    }

  if (ret)
    { 
      ret = StringCompare(buffer, copy);
    }

  if (ret)
    { 
      log_info("TEST(JSONTools_Test, Array) - Element start at '%d' for '%s' ..", FindJSONArrayElementStart(buffer, 0u), buffer);
      log_info("TEST(JSONTools_Test, Array) - .. and '%d'", FindJSONArrayElementStart(buffer, 17u));
      log_info("TEST(JSONTools_Test, Array) - .. and '%d'", FindJSONArrayElementStart(buffer, 22u));
      log_info("TEST(JSONTools_Test, Array) - .. and '%d'", FindJSONArrayElementStart(buffer, 27u));
      ret = ((1 == FindJSONArrayElementStart(buffer, 0u)) &&
	     (18 == FindJSONArrayElementStart(buffer, 17u)) &&
	     (23 == FindJSONArrayElementStart(buffer, 22u)) &&
	     (-1 == FindJSONArrayElementStart(buffer, 27u)));
    }

  if (ret)
    { 
      log_info("TEST(JSONTools_Test, Array) - Element close at '%d' for '%s' ..", FindJSONArrayElementEnd(buffer, 0u), buffer);
      log_info("TEST(JSONTools_Test, Array) - .. and '%d'", FindJSONArrayElementEnd(buffer, 17u));
      log_info("TEST(JSONTools_Test, Array) - .. and '%d'", FindJSONArrayElementEnd(buffer, 22u));
      log_info("TEST(JSONTools_Test, Array) - .. and '%d'", FindJSONArrayElementEnd(buffer, 27u));
      ret = ((16 == FindJSONArrayElementEnd(buffer, 0u)) &&
	     (21 == FindJSONArrayElementEnd(buffer, 17u)) &&
	     (26 == FindJSONArrayElementEnd(buffer, 22u)) &&
	     (-1 == FindJSONArrayElementEnd(buffer, 27u)));
    }

  // Plausble pattern

  if (ret)
    {
      ccs::types::uint32 start = 0u;
      ccs::types::uint32 close = 0u;
      ccs::types::uint32 elems = 0u;

      for (; FindJSONArrayElement(buffer, start, close, close); )
	{
	  log_info("TEST(JSONTools_Test, Array) - Element between '%d' and '%d' for '%s'", start, close, buffer);
	  elems++;
	  close++;
	}

      ret = (3u == elems);
    }

  // CompoundType definition

  if (ret)
    {
      char8 buffer [] = "{\"type\":\"test::loose::Scalars_t\","
                         "\"attributes\":["
                                         "{\"boolean\":{\"type\":\"bool\",\"size\":1}},"
                                         "{\"uint32\":{\"type\":\"uint32\",\"size\":4}},"
                                         "{\"float64\":{\"type\":\"float64\",\"size\":8}}"
                                        "]"
                        "}";

      ccs::types::uint32 start = 0u;
      ccs::types::uint32 close = 0u;

      (void)FindJSONArray(buffer, start, close);

      ccs::types::uint32 elems = 0u;
      close = start; // The offset

      for (; FindJSONArrayElement(buffer, start, close, close); )
        {
          char8 attr [64] = STRING_UNDEFINED;
	  SafeStringCopy(attr, buffer+start, close-start+2);
          log_info("TEST(JSONTools_Test, Array) - Element between '%d' and '%d' for '%s' ..", start, close, buffer);
          log_info("TEST(JSONTools_Test, Array) - .. '%s'", attr);
          elems++;
          close++;
        }

      ret = (3u == elems);

    }

  ASSERT_EQ(true, ret);
}
  
TEST(JSONTools_Test, Object)
{
  using namespace ccs::types;
  using namespace ccs::HelperTools;

  // JSON stream     0 12345 67 890123 456 78 9012345678 90 12345678 90 12345678 90 1234567
  char8 buffer [] = "{\"attr\":\"value\", \".\" : {..}, \".\":[..], \".\":true, \".\":0.0 }";
      
  bool ret = IsJSONObject(buffer);

  uint32 start = 0u;
  uint32 close = 0u;

  if (ret)
    { 
      ret = FindJSONObject(buffer, start, close);
    }

  if (ret)
    { 
      ret = ((0u == start) && (57u == close));
    }

  if (ret)
    { 
      ret = (58u == JSONObjectLength(buffer));
    }

  char8 copy [64] = STRING_UNDEFINED;

  if (ret)
    { 
      ret = CopyJSONObject(buffer, copy, 64u);
    }

  if (ret)
    { 
      ret = StringCompare(buffer, copy);
    }

  if (ret)
    { 
      log_info("TEST(JSONTools_Test, Object) - Element start at '%d' for '%s' ..", FindJSONObjectElementStart(buffer, 0u), buffer);
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementStart(buffer, 15u));
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementStart(buffer, 27u));
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementStart(buffer, 37u));
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementStart(buffer, 47u));
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementStart(buffer, 56u));
      ret = ((1 == FindJSONObjectElementStart(buffer, 0u)) &&
	     (17 == FindJSONObjectElementStart(buffer, 15u)) &&
	     (29 == FindJSONObjectElementStart(buffer, 27u)) &&
	     (39 == FindJSONObjectElementStart(buffer, 37u)) &&
	     (49 == FindJSONObjectElementStart(buffer, 47u)) &&
	     (-1 == FindJSONObjectElementStart(buffer, 56u)));
    }

  if (ret)
    { 
      log_info("TEST(JSONTools_Test, Object) - Element close at '%d' for '%s' ..", FindJSONObjectElementEnd(buffer, 0u), buffer);
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementEnd(buffer, 15u));
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementEnd(buffer, 27u));
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementEnd(buffer, 37u));
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementEnd(buffer, 47u));
      log_info("TEST(JSONTools_Test, Object) - .. and '%d'", FindJSONObjectElementEnd(buffer, 56u));
      ret = ((14 == FindJSONObjectElementEnd(buffer, 0u)) &&
	     (26 == FindJSONObjectElementEnd(buffer, 15u)) &&
	     (36 == FindJSONObjectElementEnd(buffer, 27u)) &&
	     (46 == FindJSONObjectElementEnd(buffer, 37u)) &&
	     (55 == FindJSONObjectElementEnd(buffer, 47u)) &&
	     (-1 == FindJSONObjectElementEnd(buffer, 56u)));
    }

  if (ret)
    {
      (void)CopyJSONObjectElementName(buffer, copy, 64u, 0u);
      log_info("TEST(JSONTools_Test, Object) - Element name at '%d' is '%s' ..", 0u, copy);
      ret = StringCompare(copy, "attr");
    }

  if (ret)
    {
      (void)CopyJSONObjectElementName(buffer, copy, 64u, FindJSONObjectElementEnd(buffer, 0u) + 1u);
      log_info("TEST(JSONTools_Test, Object) - .. at '%d' is '%s'", FindJSONObjectElementEnd(buffer, 0u) + 1u, copy);
      ret = StringCompare(copy, ".");
    }

  // Plausble pattern

  if (ret)
    {
      ccs::types::uint32 start = 0u;
      ccs::types::uint32 close = 0u;
      ccs::types::uint32 elems = 0u;

      for (; FindJSONObjectElement(buffer, start, close, close); )
	{
	  log_info("TEST(JSONTools_Test, Object) - Element between '%d' and '%d' for '%s'", start, close, buffer);
	  elems++;
	  close++;
	}

      ret = (5u == elems);
    }

  ASSERT_EQ(true, ret);
}
  
