/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file RPCServiceUDP.h
 * @brief Header file for RPCServiceUDP class.
 * @date 18/09/2018
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @detail This header file contains the definition of the RPCServiceUDP class.
 */

#ifndef _RPCServiceUDP_h_
#define _RPCServiceUDP_h_

// Global header files

// Local header files

#include "BasicTypes.h"

// Constants

// Type definition

namespace ccs {

namespace mcast {

class RPCClientImpl; // Forward class declaration
class RPCServerImpl; // Forward class declaration

// Global variables

const char DefaultIfaceName [] = "lo";
const char DefaultMCastAddr [] = "239.0.0.1";
const ccs::types::uint32 DefaultMCastPort = 60000u;

// Function declaration

ccs::base::RPCClientImpl* RPCClientUDPConstructor (void);
ccs::base::RPCServerImpl* RPCServerUDPConstructor (void);

// Function definition

} // namespace mcast

} // namespace ccs

#endif // _RPCServiceUDP_h_

