/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

// Local header files

#include "AnyTypeHelper.h"
#include "AnyValue.h"
#include "AnyValueHelper.h"

// Constants

// Type definition

// Global variables

// Function declaration

// Function definition

TEST(Bug12532_Test, Default)
{
  ccs::types::char8 type [] = "{\"type\":\"RPCMessage_t\",\"attributes\":[{\"qualifier\":{\"type\":\"string\"}},{\"value\":{\"type\":\"Message_t\",\"attributes\":[{\"name\":{\"type\":\"string\"}},{\"message\":{\"type\":\"string\"}}]}}]}";
  ccs::types::char8 inst [] = "{\"qualifier\":\"message\",\"value\":{\"name\":\"gtdb\",\"message\":\"Dump\"}}";

  ccs::types::AnyValue value (type);

  bool ret = value.ParseInstance(inst);

  if (ret)
    {
      ret = (ccs::HelperTools::HasAttribute(&value, "value.name") &&
	     ccs::HelperTools::IsString(ccs::HelperTools::GetAttributeType(&value, "value.name")) &&
	     ccs::HelperTools::HasAttribute(&value, "value.message") &&
	     ccs::HelperTools::IsString(ccs::HelperTools::GetAttributeType(&value, "value.message")));
    }

  ASSERT_EQ(ret, true);
}

