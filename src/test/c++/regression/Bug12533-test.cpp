/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

// Local header files

#include "BasicTypes.h"

// Constants

// Type definition

// Global variables

// Function declaration

// Function definition

TEST(Bug12533_Test, Default)
{
  bool ret = true;

  if (ret)
    {
      ret = ((0 == strcmp(ccs::types::GetName(ccs::types::GetIdentifier("char")), "char8")) &&
             (0 == strcmp(ccs::types::GetName(ccs::types::GetIdentifier("float")), "float32")) &&
             (0 == strcmp(ccs::types::GetName(ccs::types::GetIdentifier("double")), "float64")) &&
             (0 == strcmp(ccs::types::GetName(ccs::types::GetIdentifier("uint32_t")), "uint32")) &&
             (0 == strcmp(ccs::types::GetName(ccs::types::GetIdentifier("uint_t")), "uint32")));
    }

  ASSERT_EQ(ret, true);
}

