/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

// Local header files

#include "JSONTools.h"

// Constants

// Type definition

// Global variables

// Function declaration

// Function definition

TEST(Bug12831_Test, GetAttributeFromJSONContent_default)
{
  ccs::types::char8 buffer [] = "Associate('{\"name\":\"adcDevice.dataSource.channel0\",\"chan\":\"D1-I9-BCA0:DQADADDSC0-NWOOE\"}')";

  const ccs::types::char8* p_buf = buffer;
      
  while ((*p_buf != 0) && (*p_buf != '{')) { p_buf += 1; }
  bool ret = ((*p_buf != 0) && (-1 != ccs::HelperTools::FindMatchingBrace(p_buf)));

  ccs::types::string chan = STRING_UNDEFINED;

  if (ret)
    {
      ret = ccs::HelperTools::GetAttributeFromJSONContent(p_buf, "chan", chan, ccs::types::MaxStringLength);
    }

  if (ret)
    {
      log_info("TEST(Bug12831_Test, GetAttributeFromJSONContent_default) - Retrieved '%s'", chan);
      ret = ccs::HelperTools::StringCompare(chan, "\"D1-I9-BCA0:DQADADDSC0-NWOOE\"");
    }

  ASSERT_EQ(true, ret);
}

