/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

// Local header files

#include "AnyTypeHelper.h"

// Constants

// Type definition

// Global variables

// Function declaration

// Function definition

TEST(Bug13335_Test, HasAttribute_nullptr)
{
  ccs::types::char8 _type [] = "{\"type\":\"Bug13335_t\",\"attributes\":[{\"qualifier\":{\"type\":\"string\"}},{\"value\":{\"type\":\"Message_t\",\"attributes\":[{\"name\":{\"type\":\"string\"}},{\"message\":{\"type\":\"string\"}}]}}]}";

  ccs::base::SharedReference<ccs::types::AnyType> type;

  bool ret = (0u < ccs::HelperTools::Parse(type, _type));

  if (ret)
    {
      ret = (true == ccs::HelperTools::HasAttribute(type, "qualifier"));
    }

  if (ret)
    {
      ret = (true == ccs::HelperTools::HasAttribute(type, ""));
    }

  if (ret)
    {
      ret = (false == ccs::HelperTools::HasAttribute(type, NULL_PTR_CAST(const ccs::types::char8*)));
    }

  ASSERT_EQ(true, ret);
}

