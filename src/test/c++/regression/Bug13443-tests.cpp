/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

// Local header files

#include "AnyTypeHelper.h"
#include "AnyValueHelper.h"

// Constants

// Type definition

// Global variables

// Function declaration

// Function definition

TEST(Bug13443_Test, ReadFromFile_float32)
{
  ccs::types::char8 _type [] = "{\"type\":\"float32\"}";

  ccs::types::AnyValue _value (_type);

  bool ret = static_cast<bool>(_value.GetType());

  if (ret)
    {
      _value = static_cast<ccs::types::float32>(-0.1);
      ret = (true == ccs::HelperTools::DumpToFile(&_value, "/tmp/bug-13442.float32.var"));
    }

  ccs::types::AnyValue value; // Placeholder

  if (ret)
    {
      ret = (true == ccs::HelperTools::ReadFromFile(&value, "/tmp/bug-13442.float32.var"));
    }

  if (ret)
    {
      ret = (static_cast<bool>(value.GetType()) && 
	     (ccs::types::Float32 == value.GetType()) &&
	     (value == static_cast<ccs::types::float32>(-0.1)));
    }

  ASSERT_EQ(true, ret);
}

TEST(Bug13443_Test, ReadFromFile_string)
{
  ccs::types::char8 _type [] = "{\"type\":\"string\"}";

  ccs::types::AnyValue _value (_type);

  bool ret = static_cast<bool>(_value.GetType());

  if (ret)
    {
      (void)ccs::HelperTools::SafeStringCopy(static_cast<ccs::types::char8*>(_value.GetInstance()), "This is a string", ccs::types::MaxStringLength);
      ret = (true == ccs::HelperTools::DumpToFile(&_value, "/tmp/bug-13442.string.var"));
    }

  ccs::types::AnyValue value; // Placeholder

  if (ret)
    {
      ret = (true == ccs::HelperTools::ReadFromFile(&value, "/tmp/bug-13442.string.var"));
    }

  if (ret)
    {
      ret = (static_cast<bool>(value.GetType()) && 
	     (ccs::types::String == value.GetType()) &&
	     ccs::HelperTools::StringCompare(static_cast<const ccs::types::char8*>(value.GetInstance()), "This is a string"));
    }

  ASSERT_EQ(true, ret);
}

