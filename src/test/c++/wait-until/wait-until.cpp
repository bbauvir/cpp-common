/******************************************************************************
* $HeadURL: $
* $Id: $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Simple test program
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/* Global header files */

#include <stdio.h> /* sscanf, printf, etc. */
#include <string.h> /* strncpy, etc. */
#include <stdarg.h> /* va_start, etc. */

#include <signal.h> /* sigset, etc. */

/* Local header files */

#include "types.h" /* Misc. type definition */
#include "tools.h" /* Misc. helper functions */

#include "FIFOBuffer.h"
#include "Statistics.h"

#include "log-wrap.h" /* Syslog wrapper routines */

/* Constants */

#define DEFAULT_AFFINITY       0
#define DEFAULT_ITERATIONS 10000
#define DEFAULT_PERIOD   1000000 /* 1kHz */

/* Type definition */

/* Global variables */

bool _terminate = false;

/* Internal function definition */

void print_usage (void)
{

  char prog_name [STRING_MAX_LENGTH] = STRING_UNDEFINED;

  get_program_name((char*) prog_name);

  fprintf(stdout, "Usage: %s <options>\n", prog_name);
  fprintf(stdout, "Options: -h|--help: Print usage.\n");
  fprintf(stdout, "         -a|--affinity <core_id>: Run thread on <core_id> CPU core, defaults to 0.\n");
  fprintf(stdout, "         -c|--count <cycle_nb>: Stop after <cycle_nb> cycles, defaults to 10000.\n");
  fprintf(stdout, "         -p|--period <period_in_ns>: Execute cycles every <period_in_ns>, defaults to 1000000, i.e. 1kHz loop.\n");
  fprintf(stdout, "         -v|--verbose: Verbose mode, statistics and measurmeent data are printed on stdout.\n");
  fprintf(stdout, "\n");

  return;

};

void signal_handler (int signal)
{

  log_info("Received signal '%d' to terminate", signal);
  _terminate = true;

};

/* Public function definition */

int main(int argc, char **argv)
{

  /* Install signal handler to support graceful termination */
  sigset(SIGTERM, signal_handler);
  sigset(SIGINT,  signal_handler);
  sigset(SIGHUP,  signal_handler);

  uint_t core = 0;
  uint_t count = DEFAULT_ITERATIONS;
  uint64_t period = DEFAULT_PERIOD;
  bool verbose = false; /* Set to true to get measurements on stdout so as to load/plot into e.g. Matlab */

  if (argc > 1)
    {
      for (uint_t index = 1; index < (uint_t) argc; index++)
	{
          if ((strcmp(argv[index], "-h") == 0) || (strcmp(argv[index], "--help") == 0))
	    {
	      /* Display usage */
	      print_usage();
	      return (0);
	    }
	  else if ((strcmp(argv[index], "-a") == 0) || (strcmp(argv[index], "--affinity") == 0))
	    {
	      // Get core identifier
	      if ((index + 1) < (uint_t) argc)
		{
		  sscanf(argv[index + 1], "%u", &core);

		  if (set_thread_affinity_to_core(core) != STATUS_SUCCESS) log_warning("set_thread_affinity_to_core() failed");
		}
	      else { print_usage(); return (0); }
	      index += 1;
            
	    }
          else if ((strcmp(argv[index], "-c") == 0) || (strcmp(argv[index], "--count") == 0))
	    {
	      /* Get sample numer */
	      sscanf(argv[index + 1], "%u", &count);
	      index += 1;
            
	    }
	  else if ((strcmp(argv[index], "-p") == 0) || (strcmp(argv[index], "--period") == 0))
	    {
	      /* Get period in ns */
	      sscanf(argv[index + 1], "%lu", &period);
	      index += 1;
            
	    }
	  else if ((strcmp(argv[index], "-v") == 0) || (strcmp(argv[index], "--verbose") == 0))
	    {
	      /* Set verbose mode */
	      verbose = true;
            
	    }
	}
    }
  else
    {
    }

  char version   [STRING_MAX_LENGTH] = STRING_UNDEFINED;
  char host_name [STRING_MAX_LENGTH] = STRING_UNDEFINED;
  char prog_name [STRING_MAX_LENGTH] = STRING_UNDEFINED;

  if (get_ccs_version(version) != STATUS_SUCCESS)
    {
      log_warning("get_ccs_version() failed");
    }
  else
    {
      if (verbose) fprintf(stdout, "%% CCS version is '%s'\n", version);
      log_info("CCS version is '%s'", version);
    }

  if (get_host_name(host_name) != STATUS_SUCCESS)
    {
      log_warning("get_host_name() failed");
    }
  else
    {
      if (verbose) fprintf(stdout, "%% Host name is '%s'\n", host_name);
      log_info("Host name is '%s'", host_name);
    }

  if (get_program_name(prog_name) != STATUS_SUCCESS)
    {
      log_warning("get_program_name() failed");
    }
  else
    {
      if (verbose) fprintf(stdout, "%% Program name is '%s'\n", prog_name);
      log_info("Program name is '%s'", prog_name);
    }

  if (set_thread_priority(SCHED_FIFO, 80) != STATUS_SUCCESS)
    {
      log_warning("set_thread_priority() failed with '%d - %s'", errno, strerror(errno));
    }
  else
    {
      if (verbose) fprintf(stdout, "%% Setting thread scheduling priority to '%d'\n", 80);
      log_info("Setting thread scheduling priority to '%d'", 80);
    }

  if (verbose) fprintf(stdout, "%% Staring test with '%d' iterations and '%lu' period\n", count, period);
  log_info("Staring test with '%d' iterations and '%lu' period", count, period);

  using namespace ccs::base;

  FIFOBuffer<uint64_t>* p_fifo  = new FIFOBuffer<uint64_t> (count); 
  Statistics<uint64_t>* p_stats = new Statistics<uint64_t> (count);

  uint64_t curr_time = get_time();
  uint64_t till_time = ceil_time(curr_time);
  uint64_t latency = 0;

  while ((_terminate == false) && (count > 0))
    {

      curr_time = wait_until(till_time);

      latency = curr_time - till_time;

      p_fifo->PushData(latency);

      till_time += period;
      count -= 1;

    }

  log_info("Computing statistics");

  /* Compute statistics */
  while (p_fifo->PullData(latency) != STATUS_ERROR)
    {
      /* Just for a quick feedback on the quality of the measurement */
      if (verbose) fprintf(stdout, "%lu\n", latency);
      log_debug("Latency is '%lu [ns]'\n", latency);

      p_stats->PushSample(latency);
    }

  /* Just for a quick feedback on the quality of the measurement */
  if (verbose) fprintf(stdout, "%% Statistics are '%lu %lu %lu %lu [ns]'\n", p_stats->GetAvg(), p_stats->GetStd(), p_stats->GetMin(), p_stats->GetMax());
  log_info("Statistics are '%lu %lu %lu %lu [ns]'\n", p_stats->GetAvg(), p_stats->GetStd(), p_stats->GetMin(), p_stats->GetMax());

  if (NULL_PTR_CAST(FIFOBuffer<uint64_t>*) != p_fifo)
    {
      delete p_fifo;
    }

  if (NULL_PTR_CAST(Statistics<uint64_t>*) != p_stats)
    {
      delete p_stats;
    }

  return 0;

};
